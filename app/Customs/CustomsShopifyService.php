<?php namespace App\Customs;

use Config;
use Carbon\Carbon;
use Exception;
use Mail;
use Log;
use Osiset\BasicShopifyAPI;

class CustomsShopifyService
{
    protected $api;

    public function __construct()
    {
        $this->api = new BasicShopifyAPI(true);
        $this->api->setVersion(config('services.shopify.version')); // "YYYY-MM" or "unstable"
        $this->api->setShop(config('services.shopify.store'));
        $this->api->setApiKey(config('services.shopify.key'));
        $this->api->setApiPassword(config('services.shopify.password'));
        $this->api->setApiSecret(config('services.shopify.secret'));
    }

    public function checkForExistingDraftOrder()
    {

    }

    public function getProductDetailsFromStore($product_id)
    {
        $type = "GET";
        $path = "/admin/products/".$product_id.".json";

        $product = $this->api->rest($type, $path)->body;

        return $product;

    }

    public function findProductFromStore($name, $spots, $min, $max, $size, $sleeve)
    {
        $type = "GET";
        $path = "/admin/products.json";
        $product_id = 0;
        $variant_id = 0;
        
        $variant = $spots ." | ". $min . " | " . $max; 
        $name = $name . " - " . $variant;

        $products = $this->api->rest($type, $path, ['title' => $name])->body;
        
        foreach($products->products as $p){
            if($p->title === $name){
                
                switch($size){
                    case "xxlarge":
                        $size = "2xlarge";
                    break;

                    case "xxxlarge":
                        $size = "3xlarge";
                    break;

                    case "xxxxlarge":
                        $size = "4xlarge";
                    break;
                }

                $product_id = $p->id;
                foreach($p->variants as $variant){
                    if($variant->option1 === $variant && strtolower($variant->option2) === $size && $variant->option3 === $sleeve->options){
                        $variant_id = $variant->id;
                    }
                }
            }
        }

        $product_array = [
            "product_id" => $product_id,
            "variant_id" => $variant_id
        ];
        
        return $product_array;
    }

    public function findCustomerFromStore($order)
    {
        $type = "GET";
        $path = "/admin/customers/search.json";
        $params = [
            "email" => $order->email
        ];
        $customer_id = 0;
        $customers = $this->api->rest($type, $path, $params)->body;
        
        return $customers;
    }

    public function submitNewCustomer($customer)
    {
        $customer = $this->api->rest("POST", "/admin/customers.json", $customer)->body;

        return $customer;
    }

    public function findDraftOrder($id)
    {
        $type = "GET";
        $path = "/admin/draft_orders/".$id.".json";
        
        $drafts = $this->api->rest($type, $path)->body;

        return $drafts;

    }

    public function submitDraftOrder($draft)
    {
        $draft = $this->api->rest("POST", "/admin/draft_orders.json", $draft)->body;

        return $draft;
    }
}
