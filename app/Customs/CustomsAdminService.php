<?php namespace App\Customs;

use Config;
use App\Helpers\Curl;
use Carbon\Carbon;
use Exception;
use Mail;
use Log;
use Validator;
use CustomsShopifyService;
use App\Http\Models\Employees;
use App\Http\Models\Preorders;
use App\Http\Models\Order_sizes;
use App\Http\Models\Products;
use App\Http\Models\Contacts;
use App\Http\Models\Quotes;
use App\Http\Models\Shirts;
use App\Http\Models\Garments;
use App\Http\Models\Colors;
use App\Http\Models\InkColors;
use App\Http\Models\Shirt_types;
use App\Http\Models\Sleeves;
use App\Http\Models\LineItems;
use App\Http\Models\Form_types;
use App\Http\Models\Payment_types;
use App\Http\Models\Designers;
use App\Http\Models\Projects;
use App\Http\Models\ProjectStatus;
use App\Http\Models\ProductionStatus;
use App\Http\Models\DocTypes;
use App\Http\Models\ProjectFiles;
use App\Http\Models\RevTypes;
use App\Http\Models\NoteLocations;
use App\Http\Models\ProjectRevisions;
use App\Http\Models\Statuses;
use App\Http\Models\QuoteBuilder;
use Avalara\AvaTaxClient;
use Avalara\TransactionBuilder;
use Avalara\DocumentType;


class CustomsAdminService
{
    public function generateRandomString($length) {
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function addNewProduct($request)
    {
        $messages = [
            'style.required' => "Style is required",
            'style.regex' => "Style can only contain valid alpha numeric characters",
            'price.required' => "Price is required",
            'price.email' => "Price is not a valid price format",

        ];

        $rules = [
            'style' => 'required|regex:/^[a-zA-Z0-9,.!?\- ]*$/u',
            'price' => 'required|regex:/^\d+(,\d{3})*(\.\d{1,2})?$/u',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->passes()){
            $product = Products::where('style', '=', $request->style)->exists();
            if($product === false){
                $product = new Products;
                $product->style = $request->style;
                $product->price = $request->price;
                $product->save();

                return response()->json([
                    'success' => true,
                    'status' => 'Product Created'
                ], 200);
            } else {
                return response()->json([
                    'success' => false,
                    'status' => 'Product Exists'
                ], 422);
            }
        } else {
            $errors = $validator->errors();
            $errors =  json_decode($errors);

            return response()->json([
                'success' => false,
                'statuserror' => $errors
            ], 422);
        }
    }

    public function addNewGarment($request)
    {
        $messages = [
            'garment_garment.required' => "Garment is required",
            'garment_garment.regex' => "Garment can only contain valid alpha numeric characters",
            'garment_upcharge.required' => "Upcharge is required",
            'garment_upcharge.regex' => "Upcharge is not a valid price format",
        ];

        $rules = [
            'garment_garment' => 'required|regex:/^[a-zA-Z0-9,.!?\- ]*$/u',
            'garment_upcharge' => 'required|regex:/^\d+(,\d{3})*(\.\d{1,2})?$/u',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->passes()){
            $garment = Garments::where('garment', '=', $request->garment)->exists();
            if($garment === false){
                $garment = new Garments;
                $garment->garment = $request->garment_garment;
                $garment->upcharge = $request->garment_upcharge;
                $garment->xsmall = (isset($request->garment_xsmall) ? 1:0);
                $garment->xxxlarge = (isset($request->garment_xxxlarge) ? 1:0);
                $garment->xxxxlarge = (isset($request->garment_xxxxlarge) ? 1:0);
                $garment->save();

                return response()->json([
                    'success' => true,
                    'status' => 'Garment Created'
                ], 200);
            } else {
                return response()->json([
                    'success' => false,
                    'status' => 'Garment Exists'
                ], 200);
            }
        } else {
            $errors = $validator->errors();
            $errors =  json_decode($errors);

            return response()->json([
                'success' => false,
                'statuserror' => $errors
            ], 422);
        }
    }

    public function addNewProject($request)
    {
            $project = Projects::where('name', '=', $request->project_name)->where('company_id','=',$request->company_id)->exists();
            if($project === false){
                $project = new Projects;
                $project->name = $request->project_name;
                $project->company_id = $request->company_id;
                $project->head_designer_id = $request->head_graphic_designer;
                $project->designer_id = 13;
                $project->estimated_start_date = $request->estimated_start_date;
                $project->estimated_end_date = $request->estimated_end_date;
                $project->salesperson_id = $request->salesperson_id;
                $project->request_id = $request->order_request;
                $project->salesperson_id = $request->salesperson;
                $project->description = $request->description;
                $project->production_status()->associate(1);
                $project->save();

                $order = Preorders::where('id', $request->order_request)->first();
                $tax_rate = $this->getSampleTaxRate($order);
                // $project->tax_rate = $tax_rate->totalRate * 100;
                // $project->save();

                $projectstatus = new ProjectStatus;
                $projectstatus->user_id = \Auth::user()->id;
                $projectstatus->project_id = $project->id;
                $projectstatus->statuses()->associate(14);
                $projectstatus->save();

                return response()->json([
                    'success' => true,
                    'status' => 'Project Created'
                ], 200);
            } else {
                return response()->json([
                    'success' => false,
                    'status' => 'Project Exists'
                ], 422);
            }
        
    }

    public function addNewEmployee($request){
        $messages = [
            'name.regex' => "Name can only contain valid alpha characters",
            'phone.regex' => "Phone number improperly formatted (format: XXX-XXX-XXXX)",
            'email.required' => "Email is required",
            'email.email' => "Email address should be in an email address format",
            'role_id.required' => "You must select a role for the user",
        ];

        $rules = [
            'name' => 'required|regex:/^[(a-zA-Z\s)]+$/u',
            'email' => 'required|email',
            'phone' => 'regex:/^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/',
            'url' => 'alpha',
            'role_id' => 'required',
            'password' => 'min:6|confirmed|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[.!@#\$%\^&\*])(?=.{6,})/',
            'password_confirm' => 'same:password'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->passes()){
            $email = Employees::where('email', '=', $request->email)->exists();
            if($email === false){
                $employee = new Employees;
                $employee->name = $request->name;
                $employee->email = $request->email;
                $employee->phone = $request->phone_number;
                $employee->role_id = $request->role_id;
                $employee->salesperson_url = $request->salesperson_url;
                $employee->password = bcrypt($request->password);
                $employee->save();

                return response()->json([
                    'success' => true,
                    'status' => 'Employee Created'
                ], 200);
            } else {
                return response()->json([
                    'success' => false,
                    'status' => 'Employee Exists'
                ], 422);
            }
        } else {
            $errors = $validator->errors();
            $errors =  json_decode($errors);

            return response()->json([
                'success' => false,
                'statuserror' => $errors
            ], 422);
        }
    }

    public function addNewContact($request)
    {
        $messages = [
            'name.required' => "Name is required",
            'name.regex' => "Name can only contain valid alpha characters",
            'phone.required' => "Phone is required",
            'phone.regex' => "Phone number is not in a valid format",
            'mobile.regex' => "Mobile number is not in a valid format",
            'company_name.regex' => "Company name can only contain valid alpha characters",
        ];

        $rules = [
            'name' => 'required|regex:/^[(a-zA-Z\s)]+$/u',
            'email' => 'required|email',
            'phone' => 'required|regex:/^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/',
            'company_name' => 'regex:/^[(a-zA-Z\s)]+$/u',
            'mobile' => 'regex:/^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->passes()){
            $contact = Contacts::where('email', '=', $request->email)->exists();
            if($contact === false){
                $contact = new Contacts;
                $contact->name = $request->name;
                $contact->email = $request->email;
                $contact->phone = $request->phone;
                $contact->mobile = $request->mobile;
                $contact->company_name = $request->company_name;
                $contact->save();

                return response()->json([
                    'success' => true,
                    'status' => 'Contact Created'
                ], 200);
            } else {
                return response()->json([
                    'success' => false,
                    'status' => 'Contact Exists'
                ], 422);
            }
        } else {
            $errors = $validator->errors();
            $errors =  json_decode($errors);

            return response()->json([
                'success' => false,
                'statuserror' => $errors
            ], 422);
        }
    }

    public function updateEmployee($request, $id)
    {
        $messages = [
            'name.regex' => "Name can only contain valid alpha characters",
            'phone.regex' => "Phone number improperly formatted (format: XXX-XXX-XXXX)",
            'email.required' => "Email is required",
            'email.email' => "Email address should be in an email address format",
            'role_id.required' => "You must select a role for the user",
        ];

        if (!isset($request->password) && !isset($request->password_confirm)){
            $rules = [
                'name' => 'required|regex:/^[(a-zA-Z\s)]+$/u',
                'email' => 'required|email',
                'phone' => 'regex:/^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/',
                'url' => 'alpha',
                'role_id' => 'required',
            ];
        } else {
            $rules = [
                'name' => 'required|regex:/^[(a-zA-Z\s)]+$/u',
                'email' => 'required|email',
                'phone' => 'regex:/^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/',
                'url' => 'alpha',
                'password' => 'min:6|confirmed|regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\X])(?=.*[!$#%]).*$/',
                'password_confirm' => 'same:password',
                'role_id' => 'required',
            ];
        }

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->passes()){
            $employee = Employees::find($id);

            if($employee !== false){
                $employee->name = $request->name;
                $employee->email = $request->email;
                $employee->role_id = $request->role_id;
                $employee->phone = $request->phone_number;
                $employee->salesperson_url = $request->salesperson_url;
                if (isset($request->password)){
                    $employee->password = bcrypt($request->password);
                }

                $employee->save();
                return response()->json([
                    'success' => true,
                    'status' => 'Employee Updated'
                ], 200);
            } else {
                return response()->json([
                    'success' => false,
                    'status' => 'Employee Not Found'
                ], 422);
            }
        } else {
            $errors = $validator->errors();
            $errors =  json_decode($errors);

            return response()->json([
                'success' => false,
                'statuserror' => $errors
            ], 422);
        }
    }

    public function updateProducts($request, $id)
    {

        $messages = [
            'product.required' => "Product is required",
            'product.regex' => "Product can only contain valid alpha numeric characters",
            'price.required' => "Price is required",
            'price.regex' => "Price is not a valid price format",
            'spots.required' => "Spots are required",
            'spots.numeric' => "Spots can only contain valid numeric characters",
            'min_pieces.required' => "Min. Pieces is required",
            'min_pieces.numeric' => "Min. Pieces can only contain valid numeric characters",
            'max_pieces.required' => "Max. Pieces is required",
            'max_pieces.numeric' => "Max. Pieces can only contain valid numeric characters",

        ];

        $rules = [
            'product' => 'required|regex:/^[a-zA-Z0-9,.!?\- ]*$/u',
            'price' => 'required|regex:/^\d+(,\d{3})*(\.\d{1,2})?$/u',
            'spots' => 'required|numeric',
            'min_pieces' => 'required|numeric',
            'max_pieces' => 'required|numeric',
            
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->passes()){
            $product = Products::find($id);

            if($product !== false){
                $product->product = $request->product;
                $product->price = $request->price;
                $product->spots = $request->spots;
                $product->min_pieces = $request->min_pieces;
                $product->max_pieces = $request->max_pieces;
                $product->save();
                return response()->json([
                    'success' => true,
                    'status' => 'Product Updated'
                ], 200);
            } else {
                return response()->json([
                    'success' => false,
                    'status' => 'Product Not Found'
                ], 422);
            }
        } else {
            $errors = $validator->errors();
            $errors =  json_decode($errors);

            return response()->json([
                'success' => false,
                'statuserror' => $errors
            ], 422);
        }
    }

    public function updateGarments($request, $id)
    {

        $messages = [
            'garment_garment.required' => "Garment is required",
            'garment_garment.regex' => "Garment can only contain valid alpha numeric characters",
            'garment_upcharge.required' => "Upcharge is required",
            'garment_upcharge.regex' => "Upcharge is not a valid price format",
        ];

        $rules = [
            'garment_garment' => 'required|regex:/^[a-zA-Z0-9,.!?\- ]*$/u',
            'garment_upcharge' => 'required|regex:/^\d+(,\d{3})*(\.\d{1,2})?$/u',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->passes()){
            $garment = Garments::find($id);

            if($garment !== false){
                $garment->garment = $request->garment_garment;
                $garment->upcharge = $request->garment_upcharge;
                $garment->xsmall = (isset($request->garment_xsmall) ? 1:0);
                $garment->xxxlarge = (isset($request->garment_xxxlarge) ? 1:0);
                $garment->xxxxlarge = (isset($request->garment_xxxxlarge) ? 1:0);
                $garment->save();
                return response()->json([
                    'success' => true,
                    'status' => 'Garment Updated'
                ], 200);
            } else {
                return response()->json([
                    'success' => false,
                    'status' => 'Garment Not Found'
                ], 200);
            }
        } else {
            $errors = $validator->errors();
            $errors =  json_decode($errors);

            return response()->json([
                'success' => false,
                'statuserror' => $errors
            ], 422);
        }
    }

    public function updateContacts($request, $id)
    {

        $messages = [
            'name.required' => "Name is required",
            'name.regex' => "Name can only contain valid alpha characters",
            'phone.required' => "Phone is required",
            'phone.regex' => "Phone number is not in a valid format",
            'mobile.regex' => "Mobile number is not in a valid format",
            'company_name.regex' => "Company name can only contain valid alpha characters",
        ];

        $rules = [
            'name' => 'required|regex:/^[(a-zA-Z\s)]+$/u',
            'email' => 'required|email',
            'phone' => 'required|regex:/^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/',
            'company_name' => 'regex:/^[(a-zA-Z\s)]+$/u',
            'mobile' => 'regex:/^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->passes()){
            $contact = Contacts::find($id);

            if($contact !== false){
                $contact->name = $request->name;
                $contact->email = $request->email;
                $contact->phone = $request->phone;
                $contact->mobile = $request->mobile;
                $contact->company_name = $request->company_name;
                $contact->save();
                return response()->json([
                    'success' => true,
                    'status' => 'Contact Updated'
                ], 200);
            } else {
                return response()->json([
                    'success' => false,
                    'status' => 'Contact Not Found'
                ], 422);
            }
        } else {
            $errors = $validator->errors();
            $errors =  json_decode($errors);

            return response()->json([
                'success' => false,
                'statuserror' => $errors
            ], 422);
        }
    }

    public function updateOrderDatabase($request, $id){

        $order = Preorders::find($id);


        if($order !== false){

	        $file_names = $this->generateIdsAndMoveImages($request);

            if(isset($file_names['customer']['image1'])){
            	$order->customerimage1 = $file_names['customer']['image1'];
			}

            if(isset($file_names['customer']['image2'])){
            	$order->customerimage2 = $file_names['customer']['image2'];
			}

			if(isset($file_names['customer']['image3'])){
            	$order->customerimage3 = $file_names['customer']['image3'];
			}

			if(isset($file_names['customer']['image4'])){
            	$order->customerimage4 = $file_names['customer']['image4'];
			}

			if(isset($file_names['customer']['image5'])){
            	$order->customerimage5= $file_names['customer']['image5'];
			}
            $order->generated_url = $request->url;
            $order->save();
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function getSizesFromRequestedQuote($id, $post)
    {
        $size_array = array();

        foreach($post as $k => $v){
            if($k == "_token"){
                continue;
            }

            $split_name = explode("_", $k);
            if(isset($split_name[5])){
                $type = $split_name[0] ." ".$split_name[1]." ".$split_name[2];
                $color = $split_name[3] ." ". $split_name[4];
                $size = $split_name[5];  
            } else if(isset($split_name[4])){
                $is_color = Colors::where('name', 'like', '%'.$split_name[2].'%')->get();
                if($is_color->isNotEmpty()){
                    $type = $split_name[0] ." ".$split_name[1];
                    $color = $split_name[2] ." ". $split_name[3];
                    $size = $split_name[4];
                } else {
                    $type = $split_name[0] ." ".$split_name[1]." ".$split_name[2];
                    $color = $split_name[3];
                    $size = $split_name[4];
                }
                
            
            } else if(isset($split_name[3])){
                $is_color = Colors::where('name', 'like', '%'.$split_name[1].'%')->get();
                if($is_color->isNotEmpty()){
                    $type = $split_name[0];
                    $color = $split_name[1] ." ".$split_name[2];
                    $size = $split_name[3];
                } else {
                    $type = $split_name[0] ." ".$split_name[1];
                    $color = $split_name[2];
                    $size = $split_name[3];
                }
                
            
            } else {
                $type = $split_name[0];
                $color = $split_name[1];
                $size = $split_name[2];
            }
        
            $size_array[$type][$color][$size] = $v;

        }

        $order_info = Preorders::where('generated_url', 'like', "%".$id)->get();
        $order_id = "";
        $salesperson = "";
        foreach($order_info as $oi){
            $order_id = $oi->id;
            $salesperson = $oi->salesperson;
        }


        foreach($size_array as $type=>$colors){

            foreach($colors as $color => $sizes){


                if( !isset($sizes['xsmall']) ){
                    $sizes['xsmall'] = 0;
                }

                if( !isset($sizes['xxxlarge']) ){
                    $sizes['xxxlarge'] = 0;
                }

                if( !isset($sizes['xxxxlarge']) ){
                    $sizes['xxxxlarge'] = 0;
                }


                $order = Order_sizes::where('shirt_type', $type)
                                    ->where('shirt_color', $color)->first();
                if(!$order) {
                    $order = new Order_sizes;
                    $order->order_id = $order_id;
                    $order->shirt_type = $type;
                    $order->shirt_color = $color;
                    $order->xsmall = (isset($sizes['xsmall'])?$sizes['xsmall']:0);
                    $order->small = (isset($sizes['small'])?$sizes['small']:0);
                    $order->medium = (isset($sizes['medium'])?$sizes['medium']:0);
                    $order->large = (isset($sizes['large'])?$sizes['large']:0);
                    $order->xlarge = (isset($sizes['xlarge'])?$sizes['xlarge']:0);
                    $order->xxlarge = (isset($sizes['xxlarge'])?$sizes['xxlarge']:0);
                    $order->xxxlarge = (isset($sizes['xxxlarge'])?$sizes['xxxlarge']:0);
                    $order->xxxxlarge = (isset($sizes['xxxxlarge'])?$sizes['xxxxlarge']:0);
                    $order->save();
                } else {
                    $order->xsmall = (isset($sizes['xsmall'])?$sizes['xsmall']:0);
                    $order->small = (isset($sizes['small'])?$sizes['small']:0);
                    $order->medium = (isset($sizes['medium'])?$sizes['medium']:0);
                    $order->large = (isset($sizes['large'])?$sizes['large']:0);
                    $order->xlarge = (isset($sizes['xlarge'])?$sizes['xlarge']:0);
                    $order->xxlarge = (isset($sizes['xxlarge'])?$sizes['xxlarge']:0);
                    $order->xxxlarge = (isset($sizes['xxxlarge'])?$sizes['xxxlarge']:0);
                    $order->xxxxlarge = (isset($sizes['xxxxlarge'])?$sizes['xxxxlarge']:0);
                    $order->save();
                }

            }

        }

        return $salesperson;
    }

    public function determineUseablePieces($data, $type)
    {
        $useable = [];

        foreach($data as $d){
            if($d['quote'] === 1){
                $useable[] = $d[$type];
            }
        }

        return $useable;
    }

    public function buildOrderSizesNow($id)
    {
        $order_info = Preorders::where('id', $id)->first();
        $shirt_types = unserialize($order_info->shirt_types);
        $garments = unserialize($order_info->garments);

        $useable_colors = $this->determineUseablePieces($shirt_types['colors'],'color');
        $useable_types = $this->determineUseablePieces($shirt_types['types'], 'type');
        $useable_garments = $this->determineUseablePieces($garments['garments'], 'name');

        $build_array = [];

        foreach($useable_garments as $ug){
            foreach($useable_colors as $uc){
                //foreach($useable_types as $ut){
                    $build_array[$ug][] = [
                        //"type" => $ut,
                        "color" => $uc
                    ];
               // }
            }
        }

        $order_data = Order_sizes::where('order_id', $id)->get()->toArray();
        foreach($build_array as $key => $value){                
            foreach($value as $k => $v){
                $is_in_array = $this->isInArrayRecursive($v['color'], $order_data, 'shirt_color','shirt_type', $key);
                if($is_in_array === false){
                    $order = Order_sizes::where('order_id', $id)->where('shirt_type', $key)->where('shirt_color', $v['color'])->first();
                    if(!$order) {
                        $order = new Order_sizes;
                        $order->order_id = $id;
                        $order->shirt_type = $key;
                        $order->shirt_color = $v['color'];
                        $order->garment_number = "";
                        $order->xsmall = 0;
                        $order->small = 0;
                        $order->medium = 0;
                        $order->large = 0;
                        $order->xlarge = 0;
                        $order->xxlarge = 0;
                        $order->xxxlarge = 0;
                        $order->xxxxlarge = 0;
                        $order->save();
                    }
                } else {
                    $order_data = Order_sizes::where('order_id', $id)->get();
                    foreach($order_data as $od){
                        $is_in_array = $this->isInArrayRecursiveReverse($od->shirt_color, $build_array, 'color');
                        if($is_in_array === false){
                            $od->delete();
                        } else {
                            $is_in_array = $this->isInArrayGarment($od->shirt_type, $build_array);
                            if($is_in_array === false){
                                $od->delete();
                            }
                        }
                    }
                }
            }
        }
        

        
    }

    private function isInArrayRecursive($needle, $haystack, $array_key1, $array_key2, $search_key = null ) {
        foreach ($haystack as $item) {
            if($item[$array_key1] === $needle && $item[$array_key2] === $search_key){
                return true;
            }
        }
    
        return false;
    }

    private function isInArrayGarment($needle, $haystack) {
        foreach ($haystack as $key=>$item) {
            if($key === $needle){
                return true;
            }
        }
    
        return false;
    }

    private function isInArrayRecursiveReverse($needle, $haystack, $array_key1) {
        foreach ($haystack as $item) {
            foreach($item as $k=>$v){
                if($v[$array_key1] === $needle){
                    return true;
                }
            }
        }
    
        return false;
    }

    public function generateIdsAndMoveImages($request)
    {
        if( $request->file('customerimage1') ||
            $request->file('customerimage2') ||
            $request->file('customerimage3') ||
            $request->file('customerimage4') ||
            $request->file('customerimage5') ){

            $files = array();
            $rules = array();

            if( $request->file('customerimage1') ){
                $files['customerimage1'] = $request->file('customerimage1');
                $rules['customerimage1'] = 'required|image';
                $customer_image_name_1 = \Uuid::generate(4).'.'.$request->file('customerimage1')->getClientOriginalExtension();
            }

            if( $request->file('customerimage2') ){
                $files['customerimage2'] = $request->file('customerimage2');
                $rules['customerimage2'] = 'required|image';
                $customer_image_name_2 = \Uuid::generate(4).'.'.$request->file('customerimage2')->getClientOriginalExtension();
            }

            if( $request->file('customerimage3') ){
                $files['customerimage3'] = $request->file('customerimage3');
                $rules['customerimage3'] = 'required|image';
                $customer_image_name_3 = \Uuid::generate(4).'.'.$request->file('customerimage3')->getClientOriginalExtension();
            }

            if( $request->file('customerimage4') ){
                $files['customerimage4'] = $request->file('customerimage4');
                $rules['customerimage4'] = 'required|image';
                $customer_image_name_4 = \Uuid::generate(4).'.'.$request->file('customerimage4')->getClientOriginalExtension();
            }

            if( $request->file('customerimage5') ){
                $files['customerimage5'] = $request->file('customerimage5');
                $rules['customerimage5'] = 'required|image';
                $customer_image_name_5 = \Uuid::generate(4).'.'.$request->file('customerimage5')->getClientOriginalExtension();
            }

            $validator = Validator::make($files, $rules);

            if ($validator->passes()) {

                $path = storage_path() . "/app/designer/";
                $file_names = array();

                if(isset($customer_image_name_1)){
                    $files['customerimage1']->move($path, $customer_image_name_1); // uploading file to given path
                    $file_names['customer']['image1'] = $customer_image_name_1;
                }

                if(isset($customer_image_name_2)){
                    $files['customerimage2']->move($path, $customer_image_name_2); // uploading file to given path
                    $file_names['customer']['image2'] = $customer_image_name_2;
                }

                if(isset($customer_image_name_3)){
                    $files['customerimage3']->move($path, $customer_image_name_3); // uploading file to given path
                    $file_names['customer']['image3'] = $customer_image_name_3;
                }

                if(isset($customer_image_name_4)){
                    $files['customerimage4']->move($path, $customer_image_name_4); // uploading file to given path
                    $file_names['customer']['image4'] = $customer_image_name_4;
                }

                if(isset($customer_image_name_5)){
                    $files['customerimage5']->move($path, $customer_image_name_5); // uploading file to given path
                    $file_names['customer']['image5'] = $customer_image_name_5;
                }

                return $file_names;


            } else {
                return Redirect::route('admin.orderrequests')
                    ->withInput()
                    ->withErrors($validator);
            }
        }
    }

    public function processAjaxUpload($request, $id){
        if ($request->hasFile('files')){
            foreach($request->files as $file){
                foreach($file as $f){
                    $rules = [
                        'fileupload'=>'image',
                    ];
                    $validator = Validator::make($file, $rules);
                    $path = storage_path() . "/app/designer/";

                    if ($validator->passes()) {
                        $new_file_name = \Uuid::generate(4).'.'.$f->getClientOriginalExtension();
                        $f->move($path, $new_file_name);

                        $project_files = new ProjectFiles;
                        $project_files->project_id = $id;
                        $project_files->file_url = $new_file_name;
                        $project_files->final_art = 0;
                        $project_files->save();

                        return response()->json([
                            'success' => true,
                            'status' => 'File Uploaded',
                            'old_file_name' => $f->getClientOriginalName(),
                            'file_name' => $new_file_name,
                            'document_id' => $project_files->id,
                        ], 200);
                    } else {
                        $errors = $validator->errors();
                        $errors =  json_decode($errors);

                        return response()->json([
                            'success' => false,
                            'statuserror' => $errors
                        ], 422);
                    }

                }
            }
        } else {
            return response()->json([
                'success' => false,
                'status' => 'Files Not Uploaded'
            ], 422);
        }
    }

    public function processRequestAjaxUpload($request, $id){
        if ($request->hasFile('files')){
            foreach($request->files as $file){
                foreach($file as $f){
                    $rules = [
                        'fileupload'=>'image',
                    ];
                    $validator = Validator::make($file, $rules);
                    $path = storage_path() . "/app/designer/";

                    if ($validator->passes()) {
                        $new_file_name = \Uuid::generate(4).'.'.$f->getClientOriginalExtension();
                        $f->move($path, $new_file_name);

                        return response()->json([
                            'success' => true,
                            'status' => 'File Uploaded',
                            'old_file_name' => $f->getClientOriginalName(),
                            'file_name' => $new_file_name
                        ], 200);
                    } else {
                        $errors = $validator->errors();
                        $errors =  json_decode($errors);

                        return response()->json([
                            'success' => false,
                            'statuserror' => $errors
                        ], 422);
                    }

                }
            }
        } else {
            return response()->json([
                'success' => false,
                'status' => 'Files Not Uploaded'
            ], 422);
        }
    }

    public function buildContactsArray()
    {
        $contact_list = array();

        $contacts = Contacts::all()->toArray();
        foreach ($contacts as $contact){
            if($contact['company_name'] == ""){

                $contact_list[$contact['id']] =  trim($contact['name']);

            } else {
                $contact_list[$contact['id']] =  trim($contact['company_name']);
            }
        }
        asort($contact_list);

        $contact_list = array("0"=>"Select Company") + $contact_list;
        return $contact_list;

    }

    public function buildInksArray()
    {
        $ink_list = array();

        $inks = InkColors::all();
        foreach ($inks as $ink){
            if($ink->name){

                $ink_list[$ink->name] =  trim($ink->name);

            }
        }

        $ink_list = array("0"=>"Select Ink") + $ink_list;
        return $ink_list;

    }

    public function buildGarmentsArray()
    {
        $garments_list = array();

        $garments = Garments::all();
        foreach ($garments as $garment){
            if($garment->garment){

                $garments_list[$garment->garment] =  trim($garment->garment);

            }
        }

        $garments_list = array("0"=>"Select Garment") + $garments_list;
        return $garments_list;

    }

    public function buildShirtColorsArray()
    {
        $colors_list = array();
        $colors = Colors::all();
        foreach ($colors as $color){
            if($color->name){

                $colors_list[$color->name] =  trim($color->name);

            }
        }

        $colors_list = array("0"=>"Select A Color") + $colors_list;
        return $colors_list;

    }

    public function editOrderSizes($data, $row_id)
    {
        $sizes = Order_sizes::where('id',$row_id)->first();
        $sizes->shirt_type = $data['garment'];
        $sizes->shirt_color = $data['color'];
        $sizes->garment_number = $data['garment_number'];
        $sizes->xsmall = $data['xsmall'];
        $sizes->xsmall_override = (isset($data['xsmall_override'])?$data['xsmall_override']:0);
        $sizes->small = $data['small'];
        $sizes->small_override = (isset($data['small_override'])?$data['small_override']:0);
        $sizes->medium = $data['medium'];
        $sizes->medium_override = (isset($data['medium_override'])?$data['medium_override']:0);
        $sizes->large = $data['large'];
        $sizes->large_override = (isset($data['large_override'])?$data['large_override']:0);
        $sizes->xlarge = $data['xlarge'];
        $sizes->xlarge_override = (isset($data['xlarge_override'])?$data['xlarge_override']:0);
        $sizes->xxlarge = $data['xxlarge'];
        $sizes->xxlarge_override = (isset($data['xxlarge_override'])?$data['xxlarge_override']:0);
        $sizes->xxxlarge = $data['xxxlarge'];
        $sizes->xxxlarge_override = (isset($data['xxxlarge_override'])?$data['xxxlarge_override']:0);
        $sizes->xxxxlarge = $data['xxxxlarge'];
        $sizes->xxxxlarge_override = (isset($data['xxxxlarge_override'])?$data['xxxxlarge_override']:0);
        $sizes->save();

        if($sizes) {
            $items = [
                "status" => true,
                "reason" => "success"
            ];
        } else {
            $items = [
                "status" => false,
                "reason" => "could not save updated order sizes"
            ];
        }

        return $items;
    }

    public function buildProjectGarmentsArray($garments)
    {
        $garments = unserialize($garments);

        $garments_list = array();
        foreach ($garments['garments'] as $garment){
            if($garment['quote'] === 1){
                $garments_list[$garment['name']] =  trim($garment['name']);
            }
        }

        $garments_list = array("0"=>"Select Garment") + $garments_list;
        return $garments_list;

    }

    public function buildProjectColorsArray($shirt_type)
    {
        $shirt_type = unserialize($shirt_type);
        $colors = $shirt_type['colors'];

        $colors_list = array();
        foreach ($colors as $color){
            if($color['quote'] === 1){
                $colors_list[$color['color']] =  trim($color['color']);
            }
        }

        $colors_list = array("0"=>"Select Shirt Color") + $colors_list;
        return $colors_list;

    }

    public function buildProjectSleevesArray($id)
    {
        $sleeves = $this->determineSleeveType($id);

        $sleeve_list = array();

        $sleeve_list[$sleeves->options] =  trim($sleeves->options);

        $sleeve_list = array("0"=>"Select Sleeve") + $sleeve_list;
        return $sleeve_list;

    }

    public function buildProjectInksArray($front, $back)
    {
        $front = unserialize($front);
        $back = unserialize($back);
        $ink_array = [];

        foreach($front['colors'] as $k=>$v){
            if($v !== ""){
                $ink_array[$v] = $v;
            }
        }

        foreach($back['colors'] as $k=>$v){
            if($v !== ""){
                $ink_array[$v] = $v;
            }
        }
        $ink_array = array("0"=>"Select Ink") + $ink_array;
        return $ink_array;
    }

    public function buildGrungeArray()
    {
        $grunge_list = array();

        $grunge_list = [
            "none" => "None",
            "low" => "Low",
            "medium" => "Medium",
            "high" => "High"
        ];

        $grunge_list = array("0"=>"Select Grunge Level") + $grunge_list;
        return $grunge_list;
    }

    public function buildProductsArray()
    {
        $products_list = array();

        $products = Products::orderBy('id','asc')->get();
        foreach ($products as $product){
            $products_list[$product['id']] =  $product['product'] . " " . $product['spots'] . " " . $product['min_pieces'] . " " . $product['max_pieces'];
        }
        //asort($products_list);

        $products_list = array("0"=>"Select A Product") + $products_list;
        return $products_list;

    }

    public function buildDesignersArray()
    {
        $contact_list = array();

        $contacts = Employees::where('role_id','=', 3)->get()->toArray();
        foreach ($contacts as $contact){
            $contact_list[$contact['id']] =  trim($contact['name']);
        }
        asort($contact_list);

        $contact_list = array("0"=>"Select A Designer") + $contact_list;
        return $contact_list;
    }

    public function buildEmpoyeesArray()
    {
        $contact_list = array();  
        $contacts = Employees::where("role_id",4)->orWhere("role_id",2)->get()->toArray();

        foreach ($contacts as $contact){
            $contact_list[$contact['id']] =  trim($contact['name']);
        }
        asort($contact_list);

        return $contact_list;

    }

    public function buildSalesPersonArray()
    {
        $contact_list = array();
        return $this->buildEmpoyeesArray();

    }

    public function buildOrderRequestArray()
    {
        $contact_list = array();
        $salesperson_url = \Auth::user()->salesperson_url;

        $contacts = Preorders::where('salesperson','=',$salesperson_url)->get()->toArray();
        foreach ($contacts as $contact){
            $contact_list[$contact['id']] =  trim($contact['name']);
        }
        asort($contact_list);

        $contact_list = array("0"=>"Select A Request") + $contact_list;
        return $contact_list;

    }

    public function buildDocTypeArray()
    {
        $contact_list = array();

        $contacts = DocTypes::all()->toArray();
        foreach ($contacts as $contact){
            $contact_list[$contact['id']] =  trim($contact['type']);
        }
        asort($contact_list);
        $contact_list = array("0"=>"Select A Doc Type") + $contact_list;
        return $contact_list;

    }

    public function buildRevTypeArray()
    {
        $contact_list = array();

        $contacts = RevTypes::all()->toArray();
        foreach ($contacts as $contact){
            $contact_list[$contact['id']] =  trim($contact['type']);
        }
        asort($contact_list);
        $contact_list = array("0"=>"Select A Revision Type") + $contact_list;
        return $contact_list;

    }

    public function buildLocationsTypeArray()
    {
        $contact_list = array();

        $contacts = NoteLocations::all()->toArray();
        foreach ($contacts as $contact){
            $contact_list[$contact['id']] =  trim($contact['type']);
        }
        asort($contact_list);
        //$contact_list = array("0"=>"What part of the shirt is this revision/note for?") + $contact_list;
        return $contact_list;

    }

    public function buildProjectStatusTypeArray()
    {
        $contact_list = array();

        $contacts = Statuses::all()->toArray();
        foreach ($contacts as $contact){
            $contact_list[$contact['id']] =  trim($contact['status']);
        }
        asort($contact_list);
        return $contact_list;

    }

    public function buildProductionStatusTypeArray()
    {
        $contact_list = array();

        $contacts = ProductionStatus::all()->toArray();
        foreach ($contacts as $contact){
            $contact_list[$contact['id']] =  trim($contact['status']);
        }
        asort($contact_list);
        return $contact_list;

    }

    public function loggedInUserData($email, $class)
    {
        $user_data = $class::where('email', '=', $email)->get();

        $user_id = FALSE;

        foreach($user_data as $ud){
           $user_id = $ud->id;
        }

        return $user_id;
    }

    public function findEmployeesQuotations($id)
    {
        $quotes = Quotes::where('salesperson_id', '=', $id)->paginate(10);

        if (!$quotes->isEmpty()){
            return $quotes;
        } else {
            return FALSE;
        }
    }

    public function startNewQuote($id)
    {
        $project = Projects::where('id',$id)->first();
        $order = Preorders::where('id', $project->request_id)->first();
        $quote = Quotes::where('project_id', $id)->first();
        
        if(!$quote){
            $quote = new Quotes;
            $quote->project_id = $id;
            $quote->customer_id = $project->company_id;
            $quote->order_id = $project->request_id;
            $quote->salesperson_id = $project->salesperson_id;
            $quote->form_type_id = 1;
            $quote->payment_type_id = 5;
            $quote->order_shipped = 0;
            $quote->discount = 0.00;
            $quote->tax = 0.00;
            $quote->project_name = $project->name;
            $quote->shipping_address = $order->address;
            $quote->shipping_city = $order->city;
            $quote->shipping_state = $order->state;
            $quote->shipping_zip = $order->zip;
            $quote->created_at = Carbon::now()->format('Y-m-d H:i:s');
            $quote->updated_at = Carbon::now()->format('Y-m-d H:i:s');
            $quote->save();
        }

        //$quote = $this->getQuoteNumericValues($quote);

        return $quote;
    }

    public function calcTax($quote, $lineitems, $calc_subtotal, $calc_shipping)
    {
        $tax_subtotal = 0;
        $order = Preorders::where('id',$quote->order_id)->first();
        $tax_rates = $this->getTaxRate($order);

        foreach($lineitems as $line){
            $tax_subtotal += round($line->line_total * $tax_rates->totalRate,2);
        }

        
        $project = Projects::where('request_id',$quote->order_id)->first();
        
        $project->tax_data = serialize($tax_rates->rates);
        $project->save();
        $tax_shipping = + round($calc_shipping * $tax_rates->totalRate,2);

        $tax = $tax_subtotal + $tax_shipping;

        $quote->tax = $tax;
        $quote->save();

        return $tax;
        
    }

    public function updateLineItemTotal($lineitems, $id)
    {
        foreach($lineitems as $line){
            $product = Products::where('id',$line->product_id)->first();
            $garment = Garments::where('id',$line->garment_id)->first();
            $sleeve_cost = $this->determineExtraSleeveCosts($line->sleeve_id);
            $extra_cost = $this->determineExtraSizingCosts($line->size);
            $line_sub_total = ( $product->price + $garment->upcharge + $sleeve_cost + $extra_cost);
            $line_total = $line_sub_total * $line->quantity;

            $line->item_price = $line_sub_total;
            $line->line_total = $line_total;
            $line->save();
        }

        return $lineitems;
    }

    public function calcShipping($quote, $lineitems)
    {
        $shipping = 0.00;
        foreach($lineitems as $li){
            $shipping += ($li->quantity * 1.00);
        }

        $quote->shipping_charged = $shipping;
        $quote->save();
        
        return $shipping;
    }

    public function getTotalItems($items)
    {
        $qty = 0;
        foreach($items as $item){
            $qty += $item->quantity;
        }

        return $qty;
    }

    public function calcSubTotal($quote, $lineitems)
    {

        $subtotal = 0.00;
        foreach($lineitems as $li){

            $subtotal += $li->line_total;
            
        }

        $quote->subtotal = $subtotal;
        $quote->save();

        return $subtotal;
    }

    public function calcTotal($quote,$lineitems,$calc_shipping,$calc_tax,$calc_subtotal)
    {
        $total = $calc_shipping + $calc_subtotal + $calc_tax;
        $quote->order_total = round($total,2);
        $quote->save();

        return $total;
    }

    public function getLineItemsByProjectId($id)
    {
        $line_items = QuoteBuilder::where('project_id', $id)->get();
        return $line_items;
    }

    public function addLineItem($data,$project)
    {


            $product = Products::where('id',$data['quote_product'])->first();
            $garment = Garments::where('garment',$data['quote_garments'])->first();
            $color = Colors::where('name',$data['quote_colors'])->first();
            $size = $data['quote_sizes'];
            $location = $data['quote_locations'];
            $sleeves = Sleeves::where('options', $data['quote_sleeves'])->first();
            $ink1 = InkColors::where('name', $data['quote_front_ink1'])->first();
            $ink2 = InkColors::where('name', $data['quote_front_ink2'])->first();
            $ink3 = InkColors::where('name', $data['quote_front_ink3'])->first();
            $ink4 = InkColors::where('name', $data['quote_front_ink4'])->first();
            $bink1 = InkColors::where('name', $data['quote_back_ink1'])->first();
            $bink2 = InkColors::where('name', $data['quote_back_ink2'])->first();
            $bink3 = InkColors::where('name', $data['quote_back_ink3'])->first();
            $bink4 = InkColors::where('name', $data['quote_back_ink4'])->first();
            $quantity = $data['quote_quantity'];



            $line_sub_total = ( $product->price + 
                            $garment->upcharge + 
                            $this->determineExtraSleeveCosts($sleeves->id) + 
                            $this->determineExtraSizingCosts($size) );
            $line_total = $line_sub_total * $quantity;

            if($product->spots === 0){
                $items = [
                    "status" => false,
                    "reason" => "You cannot have a back location with 0 spots"
                ];
                return $items;
            }

            $lineItems = new QuoteBuilder;
            $lineItems->project_id = $project->id;
            $lineItems->request_id = $project->request_id;
            $lineItems->product_id = $product->id;
            $lineItems->garment_id = $garment->id;
            $lineItems->color_id = $color->id;
            $lineItems->size = $size;
            $lineItems->location = $location;
            $lineItems->sleeve_id = $sleeves->id;
            $lineItems->ink_1_front_id = (isset($ink1->id)?$ink1->id:0);
            $lineItems->ink_2_front_id = (isset($ink2->id)?$ink2->id:0);
            $lineItems->ink_3_front_id = (isset($ink3->id)?$ink3->id:0);
            $lineItems->ink_4_front_id = (isset($ink4->id)?$ink4->id:0);
            $lineItems->ink_1_back_id = (isset($bink1->id)?$bink1->id:0);
            $lineItems->ink_2_back_id = (isset($bink2->id)?$bink2->id:0);
            $lineItems->ink_3_back_id = (isset($bink3->id)?$bink3->id:0);
            $lineItems->ink_4_back_id = (isset($bink4->id)?$bink4->id:0);
            $lineItems->item_price = $line_sub_total;
            $lineItems->quantity = $quantity;
            $lineItems->line_total = $line_total;
            $lineItems->save();

            if($lineItems){
                $items = [
                    "status" => true,
                    "reason" => "success"
                ];
            }
            return $items;
        
    }

    public function determineProductToUse($total_items, $num_of_front_colors, $num_of_back_colors)
    {
        
        if($total_items < 50){
            $total_items = 50;
        }

        $product_name = $num_of_front_colors . " COLOR FRONT";
        $prod = Products::where('product', $product_name)
                            ->where('spots', $num_of_back_colors)
                            ->where('min_pieces', '<=', $total_items)
                            ->where('max_pieces', '>=', $total_items)->first();

        return $prod;
    }

    public function addLineItems($order_sizes, $project)
    {
        $order = Preorders::where('id', $project->request_id)->first();
        $front = unserialize($order->front);
        $num_of_front_colors = 0;
        $front_colors = [];
        foreach($front['colors'] as $c){
            if($c !== "" && $c !== "0"){
                $front_colors[] = $c;
                $num_of_front_colors ++;
            }
        }

        $back = unserialize($order->back);
        $num_of_back_colors = 0;
        $back_colors = [];
        foreach($back['colors'] as $c){
            if($c !== "" && $c !== "0"){
                $back_colors[] = $c;
                $num_of_back_colors ++;
            }
        }

        foreach($order_sizes as $os){
            
            $garment = Garments::where('garment', $os['shirt_type'])->first();
            $color = Colors::where('name', $os['shirt_color'])->first();
            $sleeves = $this->determineSleeveType($os['order_id']);
            $total_items = $os['xsmall'] + $os['small'] + $os['medium'] + $os['large'] + $os['xlarge'] + $os['xxlarge'] + $os['xxxlarge'] + $os['xxxxlarge'];

            $product = $this->determineProductToUse($total_items, $num_of_front_colors, $num_of_back_colors);

            if($os['xsmall'] > 0){
                $this->buildLineItem($project->id, $project->request_id, $garment->id, $color->id, 'xsmall', $sleeves->id, $os['xsmall'], $product->id, $product, $garment, $front_colors, $back_colors);
            }

            if($os['small'] > 0) {
                $this->buildLineItem($project->id, $project->request_id, $garment->id, $color->id, 'small', $sleeves->id, $os['small'], $product->id, $product, $garment, $front_colors, $back_colors);
            }

            if($os['medium'] > 0) {
                $this->buildLineItem($project->id, $project->request_id, $garment->id, $color->id, 'medium', $sleeves->id, $os['medium'], $product->id, $product, $garment, $front_colors, $back_colors);
            }

            if($os['large'] > 0) {
                $this->buildLineItem($project->id, $project->request_id, $garment->id, $color->id, 'large', $sleeves->id, $os['large'], $product->id, $product, $garment, $front_colors, $back_colors);
            }

            if($os['xlarge'] > 0) {
                $this->buildLineItem($project->id, $project->request_id, $garment->id, $color->id, 'xlarge', $sleeves->id, $os['xlarge'], $product->id, $product, $garment, $front_colors, $back_colors);
            }

            if($os['xxlarge'] > 0) {
                $this->buildLineItem($project->id, $project->request_id, $garment->id, $color->id, 'xxlarge', $sleeves->id, $os['xxlarge'], $product->id, $product, $garment, $front_colors, $back_colors);
            }

            if($os['xxxlarge'] > 0) {
                $this->buildLineItem($project->id, $project->request_id, $garment->id, $color->id, 'xxxlarge', $sleeves->id, $os['xxxlarge'], $product->id, $product, $garment, $front_colors, $back_colors);
            }

            if($os['xxxxlarge'] > 0) {
                $this->buildLineItem($project->id, $project->request_id, $garment->id, $color->id, 'xxxxlarge', $sleeves->id, $os['xxxxlarge'], $product->id, $product, $garment, $front_colors, $back_colors);
            }

        }

        return;
    }

    public function buildLineItem($project_id, $request_id, $garment_id, $color_id, $size, $sleeve_id, $quantity, $product_id, $product, $garment, $front_colors, $back_colors)
    {
        $line_sub_total = ( $product->price + 
                            $garment->upcharge + 
                            $this->determineExtraSleeveCosts($sleeve_id) + 
                            $this->determineExtraSizingCosts($size) );
        $line_total = $line_sub_total * $quantity;

        
        $lineItems = new QuoteBuilder;
        $lineItems->product_id = $product_id;
        $lineItems->project_id = $project_id;
        $lineItems->request_id = $request_id;
        $lineItems->garment_id = $garment_id;
        $lineItems->color_id = $color_id;
        $lineItems->size = $size;
        $lineItems->sleeve_id = $sleeve_id;
        $lineItems->quantity = $quantity;
        $lineItems->item_price = $line_sub_total;
        $lineItems->line_total = $line_total;

        foreach($front_colors as $k=>$c){
            if($c == "0"){
                continue;
            }
            $ink = InkColors::where('name',$c)->first();
            $ink_name = "ink_".($k+1)."_front_id";
            if($c == "Designers Choice"){
                $ink_id = 41;
            } else {
                $ink_id = $ink->id;
            }
            $lineItems->{$ink_name} = $ink_id;
        }

        foreach($back_colors as $k=>$c){
            if($c == "0"){
                continue;
            }
            $ink = InkColors::where('name',$c)->first();
            $ink_name = "ink_".($k+1)."_back_id";
            if($c == "Designers Choice"){
                $ink_id = 41;
            } else {
                $ink_id = $ink->id;
            }
            $lineItems->$ink_name = $ink_id;
        }
        $lineItems->save();
    }

    public function editLineItem($data,$project,$row_id)
    {
        $product = Products::where('id',$data['quote_product'])->first();
        $garment = Garments::where('garment',$data['quote_garments'])->first();
        $color = Colors::where('name',$data['quote_colors'])->first();
        $size = $data['quote_sizes'];
        //$location = $data['quote_locations'];
        $sleeves = Sleeves::where('options', $data['quote_sleeves'])->first();
        $front_ink1 = InkColors::where('name', $data['quote_front_ink1'])->first();
        $front_ink2 = InkColors::where('name', $data['quote_front_ink2'])->first();
        $front_ink3 = InkColors::where('name', $data['quote_front_ink3'])->first();
        $front_ink4 = InkColors::where('name', $data['quote_front_ink4'])->first();

        $back_ink1 = InkColors::where('name', $data['quote_back_ink1'])->first();
        $back_ink2 = InkColors::where('name', $data['quote_back_ink2'])->first();
        $back_ink3 = InkColors::where('name', $data['quote_back_ink3'])->first();
        $back_ink4 = InkColors::where('name', $data['quote_back_ink4'])->first();
        $quantity = $data['quote_quantity'];



        $line_sub_total = ( $product->price + 
                        $garment->upcharge + 
                        $this->determineExtraSleeveCosts($sleeves->id) + 
                        $this->determineExtraSizingCosts($size) );
        $line_total = $line_sub_total * $quantity;

        if(isset($back_ink1->id) || isset($back_ink2->id) || isset($back_ink3->id) || isset($back_ink4->id)){
            if($product->spots === 0){
                $items = [
                    "status" => false,
                    "reason" => "You cannot have back inks with a 0 spot product"
                ];
                return $items;
            }
        }

        $lineItem = QuoteBuilder::where('id', $row_id)->first();
        if($lineItem){
            $lineItem->project_id = $project->id;
            $lineItem->request_id = $project->request_id;
            $lineItem->product_id = $product->id;
            $lineItem->garment_id = $garment->id;
            $lineItem->color_id = $color->id;
            $lineItem->size = $size;
            $lineItem->sleeve_id = $sleeves->id;
            $lineItem->ink_1_front_id = (isset($front_ink1->id)?$front_ink1->id:0);
            $lineItem->ink_2_front_id = (isset($front_ink2->id)?$front_ink2->id:0);
            $lineItem->ink_3_front_id = (isset($front_ink3->id)?$front_ink3->id:0);
            $lineItem->ink_4_front_id = (isset($front_ink4->id)?$front_ink4->id:0);
            $lineItem->ink_1_back_id = (isset($back_ink1->id)?$back_ink1->id:0);
            $lineItem->ink_2_back_id = (isset($back_ink2->id)?$back_ink2->id:0);
            $lineItem->ink_3_back_id = (isset($back_ink3->id)?$back_ink3->id:0);
            $lineItem->ink_4_back_id = (isset($back_ink4->id)?$back_ink4->id:0);
            $lineItem->item_price = $line_sub_total;
            $lineItem->quantity = $quantity;
            $lineItem->line_total = $line_total;
            $lineItem->save();

            
            $lineItems = QuoteBuilder::where('garment_id', $lineItem->garment_id)->where('color_id', $lineItem->color_id)->get();
            $total_items = 0;
            $front_inks = 0;
            $back_inks = 0;
            foreach($lineItems as $item){
                $total_items += $item->quantity;
                $f1 = (isset($item->ink_1_front_id)?1:0);
                $f2 = (isset($item->ink_2_front_id)?1:0);
                $f3 = (isset($item->ink_3_front_id)?1:0);
                $f4 = (isset($item->ink_4_front_id)?1:0);
                $front_inks = $f1 + $f2 + $f3 + $f4;
                
                $b1 = (isset($item->ink_1_back_id)?1:0);
                $b2 = (isset($item->ink_2_back_id)?1:0);
                $b3 = (isset($item->ink_3_back_id)?1:0);
                $b4 = (isset($item->ink_4_back_id)?1:0);
                $back_inks = $b1 + $b2 + $b3 + $b4;
            }
        
            $product = $this->determineProductToUse($total_items, $front_inks, $back_inks);
            if($lineItem->product_id !== $product->id){
                foreach($lineItems as $item){
                    $item->product_id = $product->id;
                    $item->save();
                }
            }

            if($lineItems){
                $items = [
                    "status" => true,
                    "reason" => "success"
                ];
            }
        } else {
            $items = [
                "status" => false,
                "reason" => "Record not found, edit not saved"
            ];
            return $items;
        }

        return $items;
        
    }

    public function copyLineItem($row_id)
    {

        $lineItems = QuoteBuilder::where('id', $row_id)->first();
        if($lineItems){
            $lineItems = $lineItems->replicate();
            $lineItems->save();

            if($lineItems){
                $items = [
                    "status" => true,
                    "reason" => "success"
                ];
            }
        } else {
            $items = [
                "status" => false,
                "reason" => "Record not found, copy failed"
            ];
            return $items;
        }

        return $items;
        
    }

    private function determineExtraSizingCosts($size){
        $additional_size_cost = 0.00;

        switch($size){
            case "xxlarge":
                $additional_size_cost = 2.00;
            break;

            case "xxxlarge":
                $additional_size_cost = 3.00;
            break;

            case "xxxxlarge":
                $additional_size_cost = 4.00;
            break;
        }

        return $additional_size_cost;
    }

    private function determineExtraSleeveCosts($sleeve_type)
    {
        switch($sleeve_type){
            case 8:
                $additional_sleeve_cost = 0.00;
                break;
            case 1:
            case 2:
                $additional_sleeve_cost = 1.00;
                break;
            case 4:
                $additional_sleeve_cost = 2.00;
                break;
            case 5:
            case 6:
                $additional_sleeve_cost = 2.20;
                break;
            case 7:
                $additional_sleeve_cost = 2.40;
                break;
            case 3:
                $additional_sleeve_cost = 0.00;
                break;
        }
        return $additional_sleeve_cost;
    }

    private function convertInkToID($color)
    {
        if($color == "designers_choice"){
            return 41;
        } else if($color == ""){
            return "";
        }
        
        $color = InkColors::where('name', $color)->first();
        return $color->id;
    }

    private function determineSleeveType($id)
    {
        $order = Preorders::where('id',$id)->first();
        $left_sleeve = unserialize($order->left_sleeve);
        $right_sleeve = unserialize($order->right_sleeve);
        $combo = "";
        
        if($right_sleeve['option'] == "flag"){
            if($left_sleeve['option'] == "logo"){
                $combo = "Flag & Logo";
            } else if($left_sleeve['option'] == "custom" || $left_sleeve['option'] == "left_custom"){
                $combo = "Custom & Flag";
            } else if($left_sleeve['option'] == "none"){
                $combo = "Flag";
            }
        } else if($right_sleeve['option'] == "none"){
            if($left_sleeve['option'] == "logo"){
                $combo = "Logo";
            } else if($left_sleeve['option'] == "custom" || $left_sleeve['option'] == "left_custom"){
                $combo = "Custom";
            } else if($left_sleeve['option'] == "none"){
                $combo = "None";
            }       
        } else if($right_sleeve['option'] == "custom" || $right_sleeve['option'] == "right_custom"){
            if($left_sleeve['option'] == "logo"){
                $combo = "Custom & Logo";
            } else if($left_sleeve['option'] == "custom" || $left_sleeve['option'] == "left_custom"){
                $combo = "Custom & Custom";
            } else if($left_sleeve['option'] == "none"){
                $combo = "Custom";
            }
        }

        $sleeve = Sleeves::where('options',$combo)->first();
        return $sleeve;
    }

    private function findAppropriateProduct($images, $inks, $total_num_garments)
    {
        switch($inks){
            case 1:
                $ptext = "1 COLOR FRONT";
            break;
            case 2:
                $ptext = "2 COLOR FRONT";
            break;
            case 3:
                $ptext = "3 COLOR FRONT";
            break;
            case 4:
                $ptext = "4 COLOR FRONT";
            break;

        }

        if($total_num_garments < 50){
            $total_num_garments = 50;
        }

        $product = Products::where('product',$ptext)
                            ->where('spots',$images)
                            ->where('min_pieces', '<=', $total_num_garments)
                            ->where('max_pieces', '>=', $total_num_garments)->first();

        return $product;
    }

    private function countGarments($garment)
    {
        $total = $garment->xsmall + $garment->small + $garment->medium + $garment->large + $garment->xlarge + $garment->xxlarge + $garment->xxxLarge + $garment->xxxxlarge;

        return $total;
    }

    private function howManyImages($order)
    {
        $images = [];

        if(isset($order->customerimage1)){
            $images[] = 1;
        }

        if(isset($order->customerimage2)){
            $images[] = 2;
        }

        if(isset($order->customerimage3)){
            $images[] = 3;
        }

        if(isset($order->customerimage1)){
            $images[] = 4;
        }

        return sizeof($images);
    }

    private function howManyInks($order)
    {
        $inks = [];
        $front = unserialize($order->front);
        $back = unserialize($order->back);

        $count_front = sizeof($front['colors']);
        //$count_back = sizeof($back['colors']);

        //$total_inks = $count_front + $count_back;

        return $count_front;
    }

    public function getQuoteNumericValues($quote)
    {
        if ($quote){
            $quote->salesperson = $this->findById($quote, 'Employees');
            $quote->customer_name = $this->findById($quote, 'Contacts');
            $quote->form_type = $this->findById($quote, 'Form_types');
            $quote->payment_type = $this->findById($quote, 'Payment_types');     

            return $quote;

        } else {
            return FALSE;
        }
    }

    public function findById($quote,$model)
    {

        switch($model){
            case "Employees":
                $employee = Employees::where('id',$quote->salesperson_id)->first();
                return $employee->name;
                
                break;

            case "Contacts":
                $contact = Contacts::where('id',$quote->customer_id)->first();
                return $contact->name;
                break;

            case "ContactEmail":
                $contact = Contacts::where('id',$quote->customer_id)->first();
                return $contact->email;
                break;

            case "Form_types":
                $form_type = Form_types::where('id',$quote->form_type_id)->first();
                return $form_type->type;
                break;

            case "Payment_types":
                $payment_type = Payment_types::where('id',$quote->payment_type_id)->first();
                return $payment_type->type;
                break;
        }

    }

    public function checkForExistingLineItem($id)
    {
        
        $line_items = QuoteBuilder::where('request_id','=',$id)->get();
       

        return $line_items;
    }

    public function convertTextToNumbers($key, $value)
    {
        $data = "";

        switch($key){
            case "Product":
                $data = Products::where("style", "=", $value)->first()->toArray();
                break;
            case "Garment":
                $data = Garments::where("garment", "=", $value)->first()->toArray();
                break;
            case "Garment #":
                $data = Shirt_types::where("type", "=", $value)->first()->toArray();
                break;
            case "Color":
                $data = Colors::where("name", "=", $value)->first()->toArray();
                break;
            case "Ink 1":
                $data = InkColors::where("name", "=", $value)->first()->toArray();
                break;
            case "Ink 2":
                $data = InkColors::where("name", "=", $value)->first()->toArray();
                break;
            case "Ink 3":
                $data = InkColors::where("name", "=", $value)->first()->toArray();
                break;
            case "Ink 4":
                $data = InkColors::where("name", "=", $value)->first()->toArray();
                break;
            case "Sleeves":
                $data = Sleeves::where("options", "=", $value)->first()->toArray();
                break;

        }

        return $data;
    }

    public function updateSalesQuote($request, $id)
    {
        $messages = [
            'purchase_order_number.regex' => "Only valid alpha numeric values are accepted",
            'shipping_address.required' => "A shipping address is required",
            'shipping_address.regex' => "Only valid alpha numeric values are accepted",
            'shipping_city.required' => "A city is required",
            'shipping_city.regex' => "Only valid alpha values are accepted",
            'shipping_state.required' => "A state is required",
            'shipping_state.regex' => "Only valid alpha values are accepted",
            'shipping_zip.required' => "A zipcode is required",
            'shipping_zip.regex' => "Only valid numeric values are accepted",


        ];

        $rules = [
            'purchase_order_number' => 'regex:/^[a-zA-Z0-9,.!? ]*$/u',
            'shipping_address' => 'required|regex:/^[(a-zA-Z0-9\s)]+$/u',
            'shipping_city' => 'required|regex:/^[(a-zA-Z\s)]+$/u',
            'shipping_state' => 'required|regex:/^[(a-zA-Z\s)]+$/u',
            'shipping_zip' => 'required|regex:/^[(0-9\s)]+$/u',
        ];

        $validator = Validator::make($request->all(), $messages, $rules);

        if ($validator->passes()) {
            $line_items_results = $this->saveLineItems(json_decode($request->hidden_item_data, true), $id);

            if($line_items_results){
                $quote = Quotes::where('order_id','=',$id)->first();
                if($quote !== false){

                    if($request->paymenttype != 5){
                        $current = Carbon::now();
                        $quote->estimate_date_shipped = $current->addDays(30);
                    }


                    if((int)$quote->form_type_id === 2 && (int)$quote->payment_type_id !== 5){
                        $this->fireOrderToOdoo($quote, $request->hidden_item_data);
                    }

                    $quote->purchase_order_number = $request->purchase_order_number;
                    $quote->form_type_id = $request->formtype;
                    $quote->payment_type_id = $request->paymenttype;
                    $quote->shipping_address = $request->shipping_address;
                    $quote->shipping_city = $request->shipping_city;
                    $quote->shipping_state = $request->shipping_state;
                    $quote->shipping_zip = $request->shipping_zip;
                    $quote->subtotal = $request->subtotal;
                    $quote->shipping_charged = $request->shipping_charged;
                    $quote->discount = $request->discount;
                    $quote->order_total = $request->order_total;
                    $quote->save();

                    return response()->json([
                        'success' => true,
                        'status' => 'Quote Updated'
                    ], 200);
                } else {
                    return response()->json([
                        'success' => false,
                        'status' => 'Quote Not Found'
                    ], 422);
                }
            } else {
                return response()->json([
                    'success' => false,
                    'status' => 'Line Items Not Saved, Quote NOT updated'
                ], 422);
            }
        } else {
            $errors = $validator->errors();
            $errors =  json_decode($errors);

            return response()->json([
                'success' => false,
                'statuserror' => $errors
            ], 422);
        }
    }

    public function buildFormTypeArray()
    {
        $sorted_list = array();
        $forms = Form_types::all()->toArray();
        foreach ($forms as $form){
            $sorted_list[$form['id']] =  trim($form['type']);
        }
        return $sorted_list;
    }

    public function buildPaymentTypeArray()
    {
        $sorted_list = array();
        $payments = Payment_types::all()->toArray();
        foreach ($payments as $payment){
            $sorted_list[$payment['id']] =  trim($payment['type']);
        }
        return $sorted_list;
    }

    public function numericToStringConversion($where, $id)
    {
        $our_data = '';

        switch($where){
            case "Products":
                $data = Products::where('id','=',$id)->first();
                $our_data = $data->style;
                break;
            case "Garments":
                $data = Garments::where('id','=',$id)->first();
                $our_data = $data->garment;
                break;
            case "GarmentNumber":
                $data = Shirt_types::where('id','=',$id)->first();
                $our_data = $data->type;
                break;
            case "Color":
                $data = Colors::where('id','=',$id)->first();
                $our_data = $data->name;
                break;
            case "Sleeves":
                $data = Sleeves::where('id','=',$id)->first();
                $our_data = $data->options;
                break;
            case "Ink":
                if($id == 0){
                    $our_data = "";
                } else {
                    $data = InkColors::where('id','=',$id)->first();
                    $our_data = $data->name;
                }
                break;
        }

        return $our_data;
    }

    public function rebuildRolesArrayForSelect($roles){
        $sorted_list = array();
        foreach ($roles as $role){
            $sorted_list[$role['id']] =  trim($role['role']);
        }
        return $sorted_list;
    }

    public function buildRevision($request, $id)
    {
        $messages = [
            'front_rev_notes.regex' => "Front notes can only contain valid alpha numeric characters",
            'back_rev_notes.regex' => "Back notes can only contain valid alpha numeric characters",
            'left_rev_notes.regex' => "Left sleeve notes can only contain valid alpha numeric characters",
            'right_rev_notes.regex' => "Right Sleeve notes can only contain valid alpha numeric characters",
            'general_rev_notes.regex' => "General notes can only contain valid alpha numeric characters",
            'revtype.required' => "A Revision type is required",
        ];

        $rules = [
            'front_rev_notes' => 'nullable|regex:/^[a-zA-Z0-9,.!?\- ]*$/u',
            'back_rev_notes' => 'nullable|regex:/^[a-zA-Z0-9,.!?\- ]*$/u',
            'left_rev_notes' => 'nullable|regex:/^[a-zA-Z0-9,.!?\- ]*$/u',
            'right_rev_notes' => 'nullable|regex:/^[a-zA-Z0-9,.!?\- ]*$/u',
            'general_rev_notes' => 'nullable|regex:/^[a-zA-Z0-9,.!?\- ]*$/u',
            'revtype' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->passes()){
                
            foreach(json_decode($request->hiddenDocumentID) as $document){
                $pr = new ProjectRevisions;
                $pr->revision_type_id = $request->revtype;
                $pr->front_rev_note = $request->front_rev_notes;
                $pr->back_rev_note = $request->back_rev_notes;
                $pr->left_rev_note = $request->left_rev_notes;
                $pr->right_rev_note = $request->right_rev_notes;
                $pr->general_rev_note = $request->general_rev_notes;
                $pr->file_name = $document[1];
                $pr->user_id = \Auth::user()->id;
                $pr->project_id = $request->hiddenProjectID;
                $pr->save();
            }

            return response()->json([
                'success' => true,
                'status' => 'Revision Added'
            ], 200);

        } else {
            $errors = $validator->errors();
            $errors =  json_decode($errors);

            return response()->json([
                'success' => false,
                'statuserror' => $errors
            ], 422);
        }
    }

    public function updateProject($request, $id)
    {

        $messages = [
            'company_id.required' => "A company/contact is required",
            'project_name.regex' => "Project name can only contain valid alpha numeric characters",
            'project_name.required' => "Project name is required",
        ];

        $rules = [
            'company_id' => 'required',
            'project_name' => 'required|regex:/^[a-zA-Z0-9,.!?\- ]*$/u',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->passes()){
            $project = Projects::findOrFail($id);
            if($project !== false){
                $project->name = $request->project_name;
                $project->company_id = $request->company_id;
                $project->head_designer_id = 13;
                $project->designer_id = $request->graphic_designer;
                $project->estimated_start_date = $request->estimated_start_date;
                $project->estimated_end_date = $request->estimated_end_date;
                $project->description = $request->description;
                $project->production_status()->associate($request->production_status);
                $project->save();

                $order = Preorders::where('id', $project->request_id)->first();
                $tax_rate = $this->getSampleTaxRate($order);
                // $project->tax_rate = $tax_rate->totalRate * 100;
                // $project->save();

                $exists = ProjectStatus::where('project_id','=',$project->id)->where('status_id','=',$request->project_status)->exists();
                if($exists === false){
                    $projectstatus = new ProjectStatus;
                    $projectstatus->user_id = \Auth::user()->id;
                    $projectstatus->project_id = $project->id;
                    $projectstatus->statuses()->associate($request->project_status);
                    $projectstatus->save();

                    return response()->json([
                        'success' => true,
                        'status' => 'Project Updated'
                    ], 200);
                } else {
                    return response()->json([
                        'success' => true,
                        'status' => 'Project Updated, Status Not Changed'
                    ], 200);
                }
            } else {
                return response()->json([
                    'success' => false,
                    'status' => 'Project Not Found'
                ], 422);
            }
        } else {
            $errors = $validator->errors();
            $errors =  json_decode($errors);

            return response()->json([
                'success' => false,
                'statuserror' => $errors
            ], 422);
        }
    }

    public function fireOrderToOdoo($quote, $lineItems)
    {
        $project = Projects::where('id','=',$quote->project_id)->first();
        $email = $project->contacts->email;
        $order_name = "CustomSales-".$project->name."-".$quote->order_id;

        if (empty($this->doesOrderExist($order_name))){

            $partner_id = OS::findOdooCustomerId($email,'CustomSales::fireOrderToOdoo');

            if (empty($partner_id)){
                $partner_id = $this->createPartnerId($quote, $project);
            }

            $lineitems = $this->createOdooProduct($quote,$lineItems);
            $order_line_items = $this->createOdooLineItems($lineitems, $partner_id);
            $order_array = $this->createOdooOrderArray($order_line_items, $partner_id, $project,$quote);
            $result = OCS::create('sale.order', $order_array);
            OCS::confirm('sale.order', array($result));
        }

        return;
    }

    private function createPartnerId($quote, $project)
    {
        $country = "US";
        $country_code = OIS::findCountryId($country,'CustomSales::fireOrderToOdoo');
        $state_code = OIS::findStateId($quote->shipping_state, $country_code,'CustomSales::fireOrderToOdoo');

        $address = array(
            "address" => $quote->shipping_address,
            "city" => $quote->shipping_city,
            "state" => $quote->shipping_state,
            "zip" => $quote->shipping_zip,
            "country_code" => $country_code,
            "state_code" => $state_code
        );

        $partner_id = $this->buildOdooCustomerArray($project, $address);

        return $partner_id;
    }

    private function buildOdooCustomerArray($project, $address)
    {
        $customer_vals_array = [
            'name'			=> $project->contacts->name,
            'email'			=> $project->contacts->email,
            'street'		=> $address['address'],
            'city'			=> $address['city'],
            'state_id'		=> $address['state_code'],
            'zip'			=> $address['zip'],
            'company_type'	=> 'person',
            'country_id'	=> $address['country_code'],
            'phone'			=> $project->contacts->phone,
        ];
        $partner_id = OCS::create('res.partner', $customer_vals_array);

         return $partner_id[0]['id'];
    }

    private function createOdooOrderArray($order_line_items, $partner_id, $project, $quote)
    {
        $created = Carbon::now()->setTimezone('UTC')->toDateTimeString();
        $written = Carbon::now()->setTimezone('UTC')->toDateTimeString();
        $order_array = [
            'date_order' 			=> $created,
            'user_id' 				=> 1,
            'partner_id' 			=> $partner_id,
            'client_order_ref' 		=> "CustomSales-".$project->name."-".$quote->order_id,
            'pricelist_id'			=> 1,
            'state' 				=> 'draft',
            'payment_term_id' 		=> 1,
            'invoice_status'		=> 'invoiced',
            'write_date' 			=> $written,
            'carrier_id' 			=> 86,
            'is_gs24' 				=> false,
            'is_club' 				=> false,
            'club_order_date' 		=> "",
            'order_line'			=> $order_line_items,
        ];

        return $order_array;
    }

    private function createOdooProduct($quote,$lineItems)
    {
        $lineItems = json_decode($lineItems, true);
        $newlineItems = array();
        foreach($lineItems as $item){
            $lineitem = [];
            $inks = $item['Ink 1'] . " " . $item['Ink 2'] . " " . $item['Ink 3'] . " " . $item['Ink 4'];
            $sku = trim(strtoupper(str_replace(" ","","CUSTOMSALES-".$item['Garment #']."-".$item['Color']."-".$item['Sleeves']."-".$item['Size']."-".$inks)));
            $product = OS::findOdooProductId($sku, 'CustomSales::fireOrderToOdoo');

            if($product === FALSE){
                $inks = $item['Ink 1'] . " " . $item['Ink 2'] . " " . $item['Ink 3'] . " " . $item['Ink 4'];
                $new_product_array = [
                    'default_code'=>$sku,
                    'name'=>"CUSTOMSALES-".$item['Garment'] ." - ". $item['Garment #'] . " - " . $item['Color'] ." - ". $item['Sleeves'] . " - " . $item['Size'] ." - ".$inks,
                    'list_price'=>$item['Price'],
                ];

                $product = OCS::create('product.product', $new_product_array);
                $template_id = $this->getProductTemplate($product);
                $this->addProductVariants($template_id);

                $lineitem['Id'] = $item['Id'];
                $lineitem['Product'] = $item['Product'];
                $lineitem['Garment'] = $item['Garment'];
                $lineitem['Garment #'] = $item['Garment #'];
                $lineitem['Color'] = $item['Color'];
                $lineitem['Size'] = $item['Size'];
                $lineitem['Ink 1'] = $item['Ink 1'];
                $lineitem['Ink 2'] = $item['Ink 2'];
                $lineitem['Ink 3'] = $item['Ink 3'];
                $lineitem['Ink 4'] = $item['Ink 4'];
                $lineitem['Sleeves'] = $item['Sleeves'];
                $lineitem['Price'] = $item['Price'];
                $lineitem['Quantity'] = $item['Quantity'];
                $lineitem['Total'] = $item['Total'];
                $lineitem['product_id'] = $product;
            } else {
                $lineitem['Id'] = $item['Id'];
                $lineitem['Product'] = $item['Product'];
                $lineitem['Garment'] = $item['Garment'];
                $lineitem['Garment #'] = $item['Garment #'];
                $lineitem['Color'] = $item['Color'];
                $lineitem['Size'] = $item['Size'];
                $lineitem['Ink 1'] = $item['Ink 1'];
                $lineitem['Ink 2'] = $item['Ink 2'];
                $lineitem['Ink 3'] = $item['Ink 3'];
                $lineitem['Ink 4'] = $item['Ink 4'];
                $lineitem['Sleeves'] = $item['Sleeves'];
                $lineitem['Price'] = $item['Price'];
                $lineitem['Quantity'] = $item['Quantity'];
                $lineitem['Total'] = $item['Total'];
                $lineitem['product_id'] = $product;
            }
            $newlineItems[] = $lineitem;
        }
        return $newlineItems;
    }

    private function addProductVariants($id)
    {
        $variant_array = [
            'product_tmpl_id'=>$id,
            'attribute_id'=>9,
            'value_ids'=>[[6, 0,[96,1,2,3,4,5,6]]],
        ];

        OCS::create('product.attribute.line', $variant_array);

        return;
    }

    private function getProductTemplate($id)
    {
        $criteria = [
    		['id', '=', $id]
    	];
	    $fields = ['id','name','product_tmpl_id'];
	    $limit = 1;
	    $product_data = OCS::searchRead('product.product', $criteria, $fields, $limit);

        return $product_data[0]['product_tmpl_id'][0];
    }

    private function createOdooLineItems($lineitems, $partner_id)
    {
        $line_item_array = array();

        foreach($lineitems as $line){
            $line_item_vals = [
                'product_uom_qty' => $line['Quantity'],
                'product_id' => $line['product_id'],
                'state' => 'sale',
                'invoice_status' => 'invoiced',
                'product_uom' => 1,
                'order_partner_id' => $partner_id,
                'qty_invoiced' => $line['Quantity'],
            ];

            $line_item_array[] = array(0,0,$line_item_vals);
        }

        return $line_item_array;
    }

    private function doesOrderExist($name)
    {
        $criteria = [
			['client_order_ref', '=', $name]
    	];
	    $fields = ['id'];
	    $limit = 1;
	    $order = OCS::searchRead('sale.order', $criteria, $fields, $limit);
        return $order[0]['id'];
    }

    public function getTaxRate($order)
    {
        $key = Config::get('services.avatax.key');
        $id = Config::get('services.avatax.id');
        $environment = Config::get('app.env');
        $customer_id = 0;
        $project = Projects::where('request_id', $order->id)->first();

        $tax_data = [];
        $tax_data['totalRate'] = 0;
        $tax_data['rates'] = [];
        

        $customer_id = $this->findCustomerInShopify($order);
        $lineitems = $this->findProductInShopify($project->id);

        $client = new AvaTaxClient('Custom Sales Application', '2.6', $environment, 'production');
        $client->withSecurity($id, $key);

        $tb = new TransactionBuilder($client, "", DocumentType::C_SALESORDER, $customer_id);
        $t = $tb->withAddress('ShipFrom', '', null, null, '', '', '', 'US')
            ->withAddress('ShipTo', $order->address, null, null, $order->city, $order->state, $order->zip, 'US');
        
        foreach($lineitems as $line){
            $product = CustomsShopifyService::getProductDetailsFromStore($line->shopify_product_id);
            $product_code = "";
            foreach($product as $p){
                foreach($p->variants as $v){
                    if(isset($v->tax_code)){
                        $product_code = $v->tax_code;
                        break;
                    }
                }
            }
            
            $t->withLine($line->line_total, $line->quantity, null, $product_code);
        }

        $result = $t->create();
        foreach($result->lines as $line){
            foreach($line->details as $d){
                $key = $this->findRateInArray($d->rate, $tax_data['rates']);
                
                if($key !== false){
                    if(trim($tax_data['rates'][$key]['name']) == trim($d->taxName) && trim($tax_data['rates'][$key]['rate']) == trim($d->rate)){
                        continue;
                    } else {
                        $tax_data['rates'][$key] = [
                            "name" => $d->taxName,
                            "rate" => $d->rate
                        ];
                    }
                } else {
                    $tax_data['rates'][] = [
                        "name" => $d->taxName,
                        "rate" => $d->rate
                    ];
                }
            }
        }

        $totalrate = 0;
        foreach($tax_data['rates'] as $td){
            $totalrate += $td['rate'];
        }

        $tax_data['totalRate'] = $totalrate;
        $tax_data = json_decode(json_encode($tax_data), FALSE);

        $project->tax_rate = $tax_data->totalRate * 100;
        $project->tax_data = serialize($tax_data->rates);
        $project->save();

        return $tax_data;
    }

    public function getSampleTaxRate($order)
    {
        $key = Config::get('services.avatax.key');
        $id = Config::get('services.avatax.id');
        $environment = Config::get('app.env');

        $project = Projects::where('request_id', $order->id)->first();

        $tax_data = [];
        $tax_data['totalRate'] = 0;
        $tax_data['rates'] = [];

        $customer_id = 3014767411259;

        $client = new AvaTaxClient('Custom Sales Application', '2.6', $environment, 'production');
        $client->withSecurity($id, $key);
        $product_code = "PC030100";

        $tb = new TransactionBuilder($client, "", DocumentType::C_SALESORDER, $customer_id);
        $result = $tb->withAddress('ShipFrom', '', null, null, '', '', '', 'US')
            ->withAddress('ShipTo', $order->address, null, null, $order->city, $order->state, $order->zip, 'US')
            ->withLine(100, 1, null, $product_code)
            ->create();

        foreach($result->lines as $line){
            foreach($line->details as $d){
                $key = $this->findRateInArray($d->rate, $tax_data['rates']);
                
                if($key !== false){
                    if(trim($tax_data['rates'][$key]['name']) == trim($d->taxName) && trim($tax_data['rates'][$key]['rate']) == trim($d->rate)){
                        continue;
                    } else {
                        $tax_data['rates'][$key] = [
                            "name" => $d->taxName,
                            "rate" => $d->rate
                        ];
                    }
                } else {
                    $tax_data['rates'][] = [
                        "name" => $d->taxName,
                        "rate" => $d->rate
                    ];
                }
            }
        }

        $totalrate = 0;
        foreach($tax_data['rates'] as $td){
            $totalrate += $td['rate'];
        }

        $tax_data['totalRate'] = $totalrate;
        $tax_data = json_decode(json_encode($tax_data), FALSE);

        $project->tax_rate = $tax_data->totalRate * 100;
        $project->tax_data = serialize($tax_data->rates);
        $project->save();

        return $tax_data;
    }

    function findRateInArray($match, $taxes) {
        foreach($taxes as $index => $tax) {
            if($tax['rate'] == $match) return $index;
        }
        return FALSE;
    }

    public function convertIdsToText($lineitems)
    {
        foreach($lineitems as $lineitem){
            $proj_product = Products::where('id',$lineitem->product_id)->first();
            $garment_color = Colors::where('id', $lineitem->garment_color_id)->first();
            $sleeve_type = Sleeves::where('id', $lineitem->sleeve_type_id)->first();

            if($lineitem->ink_color_1 == 0){
                $ink1 = "Designers Choice";
            } else if($lineitem->ink_color_1 == ""){
                $ink1 = "N/A";
            } else {
                $ink = InkColors::where('id', $lineitem->ink_color_1)->first();
                $ink1 = $ink->name;
            }

            if($lineitem->ink_color_2 == 0){
                $ink2 = "Designers Choice";
            } else if($lineitem->ink_color_2 == ""){
                $ink2 = "N/A";
            } else {
                $ink = InkColors::where('id', $lineitem->ink_color_2)->first();
                $ink2 = $ink->name;
            }

            if($lineitem->ink_color_3 == 0){
                $ink3 = "Designers Choice";
            } else if($lineitem->ink_color_3 == ""){
                $ink3 = "N/A";
            } else {
                $ink = InkColors::where('id', $lineitem->ink_color_3)->first();
                $ink3 = $ink->name;
            }

            if($lineitem->ink_color_4 == 0){
                $ink4 = "Designers Choice";
            } else if($lineitem->ink_color_1 == ""){
                $ink4 = "N/A";
            } else {
                $ink = InkColors::where('id', $lineitem->ink_color_4)->first();
                $ink4 = $ink->name;
            }

            $lineitem->prod = $proj_product->product . ", " . $proj_product->spots . " spots, " . $proj_product->min_pieces . " - " . $proj_product->max_pieces;
            $lineitem->garment_color = $garment_color->name;
            $lineitem->sleeve_type = $sleeve_type->options;
            $lineitem->ink1 = $ink1;
            $lineitem->ink2 = $ink2;
            $lineitem->ink3 = $ink3;
            $lineitem->ink4 = $ink4;

        }

        return $lineitems;
       
    }

    public function findProductInShopify($id)
    {
        $lines = QuoteBuilder::where('project_id', $id)->get();
        if($lines->isNotEmpty()){
            foreach($lines as $line){

                if(!isset($line->product_id)){
                    return [
                        "status" => "failed",
                        "reason" => "not all line items have products selected"
                    ];
                }

                $product = Products::where('id',$line->product_id)->first();
                $name = $product->product;
                $spots = $product->spots;
                $min = $product->min_pieces;
                $max = $product->max_pieces;
                $size = $line->size;
                $sleeve = Sleeves::where('id', $line->sleeve_id)->first();

                $shopify_product = CustomsShopifyService::findProductFromStore($name, $spots, $min, $max, $size, $sleeve);
                $line->shopify_product_id = $shopify_product['product_id'];
                $line->shopify_product_variant_id = $shopify_product['variant_id'];
            }

            return $lines;
        }
    }

    public function findCustomerInShopify($order)
    {
        $customers = CustomsShopifyService::findCustomerFromStore($order);
        
        if(empty($customers->customers)){
            //customer does exist, lets create them
            $customer_name = $this->createCustomerNameArray($order);
            
            $customer = [
                "customer" => [
                    "first_name" => $customer_name['first_name'],
                    "last_name" => $customer_name['last_name'],
                    "email" => $order->email,
                    "phone" => $order->phone,
                    "verified_email" => true,
                    "addresses" => [
                        [
                            "address1" => $order->address,
                            "city" => $order->city,
                            "province" => $order->state,
                            "zip" => $order->zip,
                            "country" => "US"
                        ]
                    ],
                    "send_email_welcome" => true

                ],

            ];
            
            $customers = CustomsShopifyService::submitNewCustomer($customer);
        
            return $customers->customer->id;

        } else {
            foreach($customers->customers as $customer){
                return $customer->id;
            }
        }
    }

    private function createCustomerNameArray($contact)
    {
        $customer_explode = explode(" ",$contact->name);
        $first_name = $customer_explode[0];

        if(isset($customer_explode[1])){
            $last_name = $customer_explode[1];
        } else {
            $last_name = "";
        }

        $customer = [
            "first_name" => $first_name,
            "last_name" => $last_name
        ];

        return $customer;
    }

    public function createDraftOrder($order, $lines, $contact_id, $quote, $draft_order_id, $project)
    {
        if($draft_order_id){
            $draft_orders = CustomsShopifyService::findDraftOrder($draft_order_id);
        } else {
            $draft_order_id = null;
        }
        
        if(empty($draft_orders->draft_orders) || $draft_order_id === null){
            return $this->buildDraftOrder($order, $lines, $contact_id, $quote, $project);
        } else {
            foreach($draft_orders->draft_orders as $order){
                return $order->id;
            }
        }
    }

    private function buildDraftOrder($order, $lines, $contact_id, $quote, $project)
    {
        $line_items_array = $this->buildLineItemsArray($lines);
        $taxdata = unserialize($project->tax_data);
        $tax_lines = [];

        foreach($taxdata as $td){
            $tax = round($quote->subtotal * $td->rate,2);
            $tax_lines[] = [
                "price" => $tax,
                "rate" => $td->rate,
                "title" => $td->name
            ];
        }

        $draft = [
            "draft_order" => [
                "note" => $project->name,
                "line_items" => $line_items_array,
                "customer" => [
                    "id" => $contact_id,
                ],
                "shipping_line" => [
                        "title" => "Custom Order Shipping",
                        "price" => $quote->shipping_charged,
                        "custom" => true,
                ],
                "tax_exempt" => false,
                "tax_lines" => $tax_lines,
                "taxes_included" => false,
                "total_tax" => $quote->tax,
                "subtotal_price" => $quote->subtotal,
                "total_price" => $quote->order_total,
                "use_customer_default_address" => true,
            ],
        ];


        $response = CustomsShopifyService::submitDraftOrder($draft);

        if(isset($response->draft_order->id)){
            return [
                "status" => "success",
                "shopify_id" => $response->draft_order->id,
                "project_number" => $response->draft_order->note,
                "order_name" => $response->draft_order->name,
                "invoice_url" => $response->draft_order->invoice_url
            ];
        } else {
            return [
                "status" => "failed",
                "reason" => "failed to create draft order in shopify"
            ];
        }
    }

    private function buildLineItemsArray($lines)
    {
        $line_items_array = [];

        foreach($lines as $line){
            $order = Preorders::where('id',$line->request_id)->first();
            $front = unserialize($order->front);
            $back = unserialize($order->back);
            $product = Products::where('id',$line->product_id)->first();
            $garment = Garments::where('id',$line->garment_id)->first();
            $color = Colors::where('id',$line->color_id)->first();
            $sleeves = Sleeves::where('id', $line->sleeve_id)->first();

            if($line->ink_1_front_id == 0){
                $line->ink_1_front_id = 41;
            } else if($line->ink_2_front_id == 0){
                $line->ink_2_front_id = 41;
            } else if($line->ink_3_front_id == 0){
                $line->ink_3_front_id = 41;
            } else if($line->ink_4_front_id == 0){
                $line->ink_4_front_id = 41;
            }
            
            if($line->ink_1_back_id == 0){
                $line->ink_1_back_id = 41;
            } else if($line->ink_2_back_id == 0){
                $line->ink_2_back_id = 41;
            } else if($line->ink_3_back_id == 0){
                $line->ink_3_back_id = 41;
            } else if($line->ink_4_back_id == 0){
                $line->ink_4_back_id = 41;
            }

            $ink1 = InkColors::where('id', $line->ink_1_front_id)->first();
            $ink2 = InkColors::where('id', $line->ink_2_front_id)->first();
            $ink3 = InkColors::where('id', $line->ink_3_front_id)->first();
            $ink4 = InkColors::where('id', $line->ink_4_front_id)->first();
            $bink1 = InkColors::where('id', $line->ink_1_back_id)->first();
            $bink2 = InkColors::where('id', $line->ink_2_back_id)->first();
            $bink3 = InkColors::where('id', $line->ink_3_back_id)->first();
            $bink4 = InkColors::where('id', $line->ink_4_back_id)->first();
            $garm_num = Order_sizes::where('order_id',$line->request_id)
                                    ->where('shirt_type', $garment->garment)
                                    ->where('shirt_color', $color->name)->first();
            

            $line_items_array[] = [
                "variant_id" => $line->shopify_product_variant_id,
                "product_id" => $line->shopify_product_id,
                "price" => $line->item_price,
                "quantity" => $line->quantity,
                "requires_shipping" => true,
                "taxable" => true,
                "properties" => [
                    ["name" => "garment","value" => $garment->garment],
                    ["name" => "garment number","value" => $garm_num->garment_number],
                    ["name" => "color","value" => $color->name],
                    ["name" => "sleeves","value" => $sleeves->options],
                    ["name" => "sleeves","value" => $sleeves->options],
                    ["name" => "ink 1 (Front)", "value" => (isset($ink1->name)?$ink1->name:"")],
                    ["name" => "ink 2 (Front)", "value" => (isset($ink2->name)?$ink2->name:"")],
                    ["name" => "ink 3 (Front)", "value" => (isset($ink3->name)?$ink3->name:"")],
                    ["name" => "ink 4 (Front)", "value" => (isset($ink4->name)?$ink4->name:"")],
                    ["name" => "Front Placement", "value" => $front['placement']],
                    ["name" => "ink 1 (Back)", "value" => (isset($bink1->name)?$bink1->name:"")],
                    ["name" => "ink 2 (Back)", "value" => (isset($bink2->name)?$bink2->name:"")],
                    ["name" => "ink 3 (Back)", "value" => (isset($bink3->name)?$bink3->name:"")],
                    ["name" => "ink 4 (Back)", "value" => (isset($bink4->name)?$bink4->name:"")],
                    ["name" => "Back Placement", "value" => $back['placement']],
                    ["name" => "size", "value" => $line->size]
                ],
            ];
        }

       return $line_items_array;
    }
}
