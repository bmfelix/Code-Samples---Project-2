<?php namespace App\Customs;

use Config;
use App\Helpers\Curl;
use Carbon\Carbon;
use Exception;
use Mail;
use Log;
use Redirect;
use Validator;
use App\Odoo\OdooClientService;
use App\Http\Models\SalesPersons;
use App\Http\Models\Preorders;
use CustomsMiscService;
use App\Http\Models\InkColors;
use App\Http\Models\Contacts;
use App\Http\Models\Projects;
use App\Http\Models\ProjectStatus;

class CustomsSubmissionService
{

    /**
     * main submission handler for the customs form.  All form data is processed by this function.  
     * Returns true if validation passed, returns invalid validation string otherwise
     *
     * @return $validation or true
     */
    public function customSalesSubmission($request)
    {
        $messages = CustomsMiscService::getSubmissionsMessages();
        $rules = CustomsMiscService::getSubmissionsRules();

        $validation = Validator::make($_POST, $rules, $messages);

        if($validation->passes()){

            if($request->file()){
                $file_names = CustomsMiscService::uploadCustomsGraphics($request->file());
                if(is_array($file_names) && isset($file_names['failed'])){
                    return $file_names['failed'];
                }
            } else {
                $file_names = [];
            }

            $order_array = $this->preprocessDataForOdoo($request->all(), $file_names);

            $order_return = $this->sendDataToDatabase($order_array);
            $order_array['request_id'] = $order_return->id;

            $this->createProjectFromForm($order_array);
            $this->send_email_to($order_array);
            //$this->send_data_to_odoo($order_array);

           return true;

        } else {
            return $validation;
        }
    }




    /**
     * send email to internal upon submission of form
     *
     * @param  \Illuminate\Http\Request  $post
     * @return \Illuminate\Http\Response
     */
    function send_email_to($post)
    {

        $subject = "Customs Project Request";

        $spinfo = CustomsMiscService::getEmployee($post['salesperson']);

        foreach($spinfo as $sp){
            $email = $sp->email;
            $emails = array($email);
        }
        
        //this is the proper syntax sending an email in laravel
        //************ EMAIL ******************* FORM DATA ***************** EMAIL SUBJ *** EMAIL ARRAY */
        Mail::send('email.internalcustom', ['post' => $post], function ($m) use ($subject, $emails) {
            $m->from('', ''); // FROM EMAIL (actual email address, name of from email)
            $m->to($emails)->subject($subject); // TO EMAIL and SUBJECT
        });
    }

    /**
     * create project from form data
     *
     * @return void
     */
    private function createProjectFromForm($order)
    {
        $contact = $this->isSubmitterAnExistingContact($order);
        $project_name = $this->createProjectName($order);
        $order['contact'] = $contact;
        $order['project_name'] = $project_name;

        $project = $this->createNewProject($order);
    }

     /**
     * determine if contact exists or not
     *
     * @return database object
     */
    private function isSubmitterAnExistingContact($order)
    {
        $contact = Contacts::where('email','=', $order['customer']['email'])->first();
        if(!$contact){
            $createContact = $this->createNewContact($order['customer']);
            return $createContact;
        } else {
            return $contact;
        }
    }

    private function createNewContact($customer_data)
    {
        $contact = new Contacts();
        $contact->name = $customer_data['name'];
        $contact->company_name = $customer_data['company_name'];
        $contact->phone = $customer_data['phone'];
        $contact->email = $customer_data['email'];  
        $contact->save();

        return $contact;
    }

    /**
     * create project name
     *
     * @return string
     */
    private function createProjectName($order)
    {
        $base = '';
        $salesp = $order['salesperson'];
        $now = Carbon::now()->format('YmdHms');
        
        //example 
        $name = $base."-".$salesp."-".$now;
        return $name;
    }

    /**
     * create new project during form submission
     *
     * @return void
     */
    private function createNewProject($order)
    {
        $project = Projects::where('name', '=', $order['project_name'])->where('company_id','=',$order['contact']->id)->exists();
        
        if($project === false){

            $spinfo = CustomsMiscService::getEmployee($order['salesperson']);
            $salesperson_id = '';
            foreach($spinfo as $sp){
                $salesperson_id = $sp->id;
            }

            $project = new Projects;
            $project->name = $order['project_name'];
            $project->company_id = $order['contact']->id;
            $project->salesperson_id = $salesperson_id;
            $project->request_id = $order['request_id'];
            $project->production_status()->associate(1);
            $project->save();

            $projectstatus = new ProjectStatus;
            $projectstatus->user_id = $salesperson_id;
            $projectstatus->project_id = $project->id;
            $projectstatus->statuses()->associate(14);
            $projectstatus->save();
        }
    }

    /**
     * save form data to database
     *
     * @return database object
     */
    private function sendDataToDatabase($order)
    {

        $preorder = new Preorders;
        $preorder->name = $order['customer']['name'];
        $preorder->phone = $order['customer']['phone'];
        $preorder->email = $order['customer']['email'];
        $preorder->address = $order['customer']['address'];
        $preorder->city = $order['customer']['city'];
        $preorder->state = $order['customer']['state'];
        $preorder->zip = $order['customer']['zip'];
        $preorder->shirt_types = serialize($order['shirt_types']);
        $preorder->front = serialize($order['front']);
        $preorder->back = serialize($order['back']);
        $preorder->left_sleeve = serialize($order['leftsleeve']);
        $preorder->right_sleeve = serialize($order['rightsleeve']);
        $preorder->creative_freedom = $order['creative_freedom'];
        $preorder->sleeve_notes_left = $order['sleeve_notes_left'];
        $preorder->artwork_notes_front = $order['artwork_notes_front'];
        $preorder->sleeve_notes_right = $order['sleeve_notes_right'];
        $preorder->artwork_notes_back = $order['artwork_notes_back'];
        $preorder->salesperson = $order['salesperson'];
        $preorder->company_name = $order['customer']['company_name'];
        $preorder->accepted_terms = $order['customer']['accepted_terms'];
        $preorder->project_reason = $order['customer']['project_reason'];
        $preorder->time_frame = $order['customer']['time_frame'];
        $preorder->time_frame = $order['customer']['time_frame'];
        $preorder->garments = serialize($order['garments']);
        $preorder->save();

        return $preorder;

    }

    /**
     * manipulate form data as we see fit, returns array of all values (overwriting modified values)
     *
     * @return array
     */
    private function preprocessDataForOdoo($post, $files){
        foreach($post as $key => $value){
            //example $first_name = "Brad"
            $$key = $value;
        }

        $order = array();
        $shirt_types = array();

        if(!isset($sleeveopts)){
            $sleeveopts = "not chosen";
        }

        $order['customer'] = array(
            "name" => $name,
            "address" => $address,
            "city" => $city,
            "state" => $state,
            "zip" => $zip,
            "email" => $email,
            "phone" => $phone,
            "company_name" => $company_name,
            "accepted_terms" => $accepted_terms,
            "project_reason" => $project_reason,
            "time_frame" => $time_frame,
        );

        foreach($shirt_type as $shirt){
            $type = $shirt;

            if(isset(${"shirtcolorselection"})) {
                $count = count(${"shirtcolorselection"});
            } else {
                $count = 1;
            }

            for($i = 1; $i <= $count; $i++){
                if(isset(${"shirtcolorselection"})) {
                    $color = CustomsMiscService::getColorDataNotEncoded(${"shirtcolorselection"}[$i - 1]);
                    $shirt_types['colors'][$i] = [
                        "color" => $color,
                        "quote" => 1,
                    ];
                } else {
                    $shirt_types['colors'][$i] = [
                        "color" => "red",
                        "quote" => 1,
                    ];;
                }
            }

            $shirt_types['types'][] = [
                "type" => $type,
                "quote" => 1,
            ];
        }

        $order['shirt_types'] = $shirt_types;

        if(isset($files['front'])) {
            $order['front']['images'] = $files['front'];
        }

        
            if(isset($front_ink_color_selection_1)){
                if($front_ink_color_selection_1 !== "designers_choice"){
                    $front_ink_color_selection_1 = $this->translateColor($front_ink_color_selection_1);
                    $front_ink_color_selection_1 = $front_ink_color_selection_1->name;
                }
            } else {
                $front_ink_color_selection_1 = "";
            }
        
                  
            if(isset($front_ink_color_selection_2)){
                if($front_ink_color_selection_2 !== "designers_choice"){  
                    $front_ink_color_selection_2 = $this->translateColor($front_ink_color_selection_2);
                    $front_ink_color_selection_2 = $front_ink_color_selection_2->name;
                }
            } else {
                $front_ink_color_selection_2 = "";
            }
        
            if(isset($front_ink_color_selection_3)){
                if($front_ink_color_selection_3 !== "designers_choice"){
                    $front_ink_color_selection_3 = $this->translateColor($front_ink_color_selection_3);
                    $front_ink_color_selection_3 = $front_ink_color_selection_3->name;
                }
            } else {
                $front_ink_color_selection_3 = "";
            }
        
            if(isset($front_ink_color_selection_4)){
                if($front_ink_color_selection_4 !== "designers_choice"){
                    $front_ink_color_selection_4 = $this->translateColor($front_ink_color_selection_4);
                    $front_ink_color_selection_4 = $front_ink_color_selection_4->name;
                }
            } else {
                $front_ink_color_selection_4 = "";
            }
        
        


        $order['front']['colors'] = array('ink1'=>$front_ink_color_selection_1, 'ink2'=>$front_ink_color_selection_2, 'ink3'=>$front_ink_color_selection_3, 'ink4'=>$front_ink_color_selection_4);
        // $order['front']['text'] = $front_shirt_text;

        if(!isset($front_placement)){
            $front_placement = "";
        }
        $order['front']['placement'] = $front_placement;

        $garments['garments'][] = [
            "name" => "Mens T-Shirt",
            "quote" => 1,
        ];

        if(isset($files['back'])) {
            $order['back']['images'] = $files['back'];
        }

        if(isset($back_ink_color_selection_1)){
            if($back_ink_color_selection_1 !== "designers_choice"){
                $back_ink_color_selection_1 = $this->translateColor($back_ink_color_selection_1);
                $back_ink_color_selection_1 = $back_ink_color_selection_1->name;
            }
        } else {
            $back_ink_color_selection_1 = "";
        }
    
              
        if(isset($back_ink_color_selection_2)){
            if($back_ink_color_selection_2 !== "designers_choice"){  
                $back_ink_color_selection_2 = $this->translateColor($back_ink_color_selection_2);
                $back_ink_color_selection_2 = $back_ink_color_selection_2->name;
            }
        } else {
            $back_ink_color_selection_2 = "";
        }
    
        if(isset($back_ink_color_selection_3)){
            if($back_ink_color_selection_3 !== "designers_choice"){
                $back_ink_color_selection_3 = $this->translateColor($back_ink_color_selection_3);
                $back_ink_color_selection_3 = $back_ink_color_selection_3->name;
            }
        } else {
            $back_ink_color_selection_3 = "";
        }
    
        if(isset($back_ink_color_selection_4)){
            if($back_ink_color_selection_4 !== "designers_choice"){
                $back_ink_color_selection_4 = $this->translateColor($back_ink_color_selection_4);
                $back_ink_color_selection_4 = $back_ink_color_selection_4->name;
            }
        } else {
            $back_ink_color_selection_4 = "";
        }

        $order['back']['colors'] = array('ink1'=>$back_ink_color_selection_1, 'ink2'=>$back_ink_color_selection_2, 'ink3'=>$back_ink_color_selection_3, 'ink4'=>$back_ink_color_selection_4);
       // $order['back']['text'] = $back_shirt_text;

        if(!isset($back_placement)){
            $back_placement = "";
        }
        $order['back']['placement'] = $back_placement;

        if(isset($files['leftsleeve'])) {
            $order['leftsleeve']['images'] = $files['leftsleeve'];
        }
        $order['leftsleeve']['option'] = $leftsleeveopts;

        // if(isset($leftsleevecolorselection)){
        //     $leftsleevecolorselection = $this->translateColor($leftsleevecolorselection);
        //     $leftsleevecolorselection = $leftsleevecolorselection->name;             

        // } else {
        //     $leftsleevecolorselection = "";
        // }

        // if(isset($rightsleevecolorselection)){           
        //     $rightsleevecolorselection = $this->translateColor($rightsleevecolorselection);
        //     $rightsleevecolorselection = $rightsleevecolorselection->name; 
        // } else {
        //     $rightsleevecolorselection = "";
        // }

        //$order['leftsleeve']['colors'] = array('ink1' => $leftsleevecolorselection);

        if(isset($files['rightsleeve'])) {
            $order['rightsleeve']['images'] = $files['rightsleeve'];
        }
        $order['rightsleeve']['option'] = $rightsleeveopts;
        //$order['rightsleeve']['colors'] = array('ink1' => $rightsleevecolorselection);

        $order['creative_freedom'] = $thisradio;
        $order['sleeve_notes_left'] = $sleeve_notes_left;
        $order['artwork_notes_front'] = $artwork_notes_front;
        $order['sleeve_notes_right'] = $sleeve_notes_right;
        $order['artwork_notes_back'] = $artwork_notes_back;
        $order['salesperson'] = $salesperson;
        $order['garments'] = $garments;
       
        return $order;
    }

    /**
     * translate hex values to string values
     *
     * @return string
     */
    private function translateColor($hex)
    {
        $hex = str_replace("#", "", $hex);
        $color = InkColors::where("hexcode", $hex)->first();
        return $color;
    }



   

      
}
