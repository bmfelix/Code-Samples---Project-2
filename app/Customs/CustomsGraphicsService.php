<?php namespace App\Customs;

use Config;
use App\Helpers\Curl;
use Carbon\Carbon;
use Exception;
use Mail;
use Log;
use Redirect;
use Validator;

class CustomsGraphicsService
{

    public function buildImageValidationArray($files)
    {
        $validation_array = array();

        foreach ($files as $key => $value){

            if ($value){
                $validation_array['files'][$key] = $value;
                $validation_array['rules'][$key] = 'required|mimes:jpeg,png,gif,bmp,tiff,tif,jpg,ai,psd,pdf';
                $validation_array['images'][$key]['name'] = \Uuid::generate(4).'.'.$value->getClientOriginalExtension();
            }

        }

        return $validation_array;
    }

    public function buildImageFileLocationArray($files, $image_names)
    {
        $path = storage_path() . "/app/designer/";
        $file_names = array();

        foreach ($files as $key => $value){
            if ($value){
                $value->move($path, $image_names[$key]['name']);
                switch($key){
                    case "frontimage1":
                        $file_names['front']['image1'] = $image_names[$key]['name'];
                        break;
                    case "frontimage2":
                        $file_names['front']['image2'] = $image_names[$key]['name'];
                        break;
                    case "frontimage3":
                        $file_names['front']['image3'] = $image_names[$key]['name'];
                        break;
                    case "backimage1":
                        $file_names['back']['image1'] = $image_names[$key]['name'];
                        break;
                    case "backimage2":
                        $file_names['back']['image2'] = $image_names[$key]['name'];
                        break;
                    case "backimage3":
                        $file_names['back']['image3'] = $image_names[$key]['name'];
                        break;
                    case "leftsleeveimage1":
                        $file_names['leftsleeve']['image1'] = $image_names[$key]['name'];
                        break;
                    case "leftsleeveimage2":
                        $file_names['leftsleeve']['image2'] = $image_names[$key]['name'];
                        break;
                    case "rightsleeveimage1":
                        $file_names['rightsleeve']['image1'] = $image_names[$key]['name'];
                        break;
                    case "rightsleeveimage2":
                        $file_names['rightsleeve']['image2'] = $image_names[$key]['name'];
                        break;

                }
            }
        }
        return $file_names;
    }
}
