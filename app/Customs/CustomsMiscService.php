<?php namespace App\Customs;

use Config;
use App\Helpers\Curl;
use Carbon\Carbon;
use Exception;
use Mail;
use Log;
use Redirect;
use Validator;
use App\Http\Models\Employees;
use App\Http\Models\Icons;
use App\Http\Models\Shirts;
use App\Http\Models\Shirt_types;
use App\Http\Models\Colors;
use CustomsGraphicsService;

class CustomsMiscService
{

    public function getSubmissionsMessages()
    {
        $messages = [
            'name.required' => "Name is required",
            'name.regex' => "Name can only contain alpha numeric characters",
            'phone.required' => "Phone number is required",
            'phone.regex' => "Phone number improperly formatted (format: XXX-XXX-XXXX)",
            'email.required' => "Email is required",
            'email.email' => "Email address should be in an email address format",
            'notes.regex' => "Notes can only contain alpha numeric characters",
            'address.required' => "Address is Required",
            'address.regex' => "Address can only contain alpha numeric characters",
            'city.required' => "City is required",
            'city.regex' => "City can only contain alpha numeric characters",
            'state.required' => "State is required",
            'state.regex' => "State can only contain alpha numeric characters",
            'zip.required' => "Zip Code is required",
            'zip.regex' => "Zip Code can only contain numeric characters",
            'shirt_type.required' => "You must select a garment",
            'company_name.regex' => "Company Name can only contain alpha numeric characters",
            'accepted_terms.accepted' => "Accepted Terms must be accepted",
            'thisradio.required' => "Grunge level is required",
            'project_reason.required' => "You must tell us what this project is for",
            'time_frame_from.date' => "A valid date is required",
            'time_frame_to.date' => "A valid date is required",
        ];

        return $messages;
    }

    public function getSubmissionsRules(){
        $rules = [
            'name' => 'required|regex:/^[(a-zA-Z0-9\s)]+$/u',
            'phone' => 'required|regex:/^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/',
            'email' => 'required|email',
            'notes' => 'regex:/^[a-zA-Z0-9,.!? ]*$/u',
            'address' => 'required|regex:/^[a-zA-Z0-9\s,.-]{3,}$/',
            'city' => 'required|regex:/^[(a-zA-Z0-9\s)]+$/u',
            'state' => 'required|regex:/^[(a-zA-Z0-9\s)]+$/u',
            'zip' => 'required|regex:/^[(0-9\s)]+$/u',
            'shirt_type' => 'required',
            'company_name' => 'regex:/^[(a-zA-Z0-9\s)]+$/u',
            'accepted_terms' => 'accepted',
            'thisradio' => 'required',
            'project_reason' => 'required',
            'time_frame_from' => 'date',
            'time_frame_to' => 'date'
        ];

        return $rules;
    }

    public function getEmployee($salesperson)
    {
        if(!$salesperson){
            $salesp = array();
        } else {
            $salesp = Employees::where('salesperson_url', $salesperson)->get();
        }

        return $salesp;
    }

    public function getIconData()
    {
        $icon_array = array();
  	    $icons = Icons::all();
  	    foreach($icons as $icon){
  		   $icon_array[] = array(
  			   $icon->name => array(
  				   'file' => $icon->file,
  				   'target' =>$icon->target
  			   )
  		   );
  	    }

  	   $icon_array = json_encode(array('icons' => $icon_array));

       return $icon_array;
    }

    public function getColorData($colors)
    {
        $colors_array = [];
        foreach($colors as $color){
            $color_data = Colors::where('id', '=', $color)->first();
            $colors_array[] = array(
                'id' => $color_data->id,
        		'hexcode' => $color_data->hexcode,
        		'name' => $color_data->name
            );
        }
        return json_decode(json_encode($colors_array));
    }

    public function getColorDataNotEncoded($color)
    {
        
        $color_data = Colors::where('id', '=', $color)->first();

        return $color_data->name;
    }

    public function getShirtData($shirt_type, $color)
    {
        $color_id = '';
  	    $type_id = '';

        $color_data = Colors::where('shorthand', '=', $color)->get();
        foreach($color_data as $cd){
           $color_id = $cd->id;
        }

        $type_data = Shirt_types::where('type', '=', $shirt_type)->get();
        foreach($type_data as $td){
           $type_id = $td->id;
        }

        $colors_for_shirt = Shirts::where('shirt_types_id','=',$type_id)->get();
        $colors_array = array();
        foreach($colors_for_shirt as $cfs){
           $color_name = Colors::find($cfs->colors_id);
           $key = strtolower(preg_replace('/\s+/', '', $color_name->name));
           $colors_array[$key] = array(
        		'hexcode' => $color_name->hexcode,
        		'name' => $color_name->name
           );
        }

        $shirts_data = Shirts::where('shirt_types_id','=',$type_id)->where('colors_id','=',$color_id)->get();
        $shirt_data = array();
        foreach($shirts_data as $sd){
           $shirt_data = array(
        	   $shirt_type => array(
        		   "images" => array(
        			   $color => array(
        				   "front" => array('file' => $sd->front_image),
        				   "back" => array('file' => $sd->back_image)
        			   )
        		   ),
        		   "sleeves" => array(
        			   "front" => array(
        				   "flag" => array('file' => $sd->sleeve_front_right),
        				   "logo" => array('file' => $sd->sleeve_front_left)
        			   ),
        			   "back" => array(
        				   "flag" => array('file' => $sd->sleeve_back_right),
        				   "logo" => array('file' => $sd->sleeve_back_left)
        			   )
        		   ),
        		   "colors" => $colors_array

        	   )
           );
        }

        return json_encode(array("shirts" => $shirt_data));
    }

    /**
     * validates upload is an image and then stores it, and returns a UUID file name
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function uploadCustomsGraphics($files)
    {

        $validation_array = CustomsGraphicsService::buildImageValidationArray($files);

        $validator = Validator::make($validation_array['files'], $validation_array['rules']);

        if ($validator->passes()) {

            $file_names = CustomsGraphicsService::buildImageFileLocationArray($files, $validation_array['images']);

            return $file_names;


        } else {
            $failed = ["failed" => $validator->errors()];
            return $failed;
        }

        return;
    }


}
