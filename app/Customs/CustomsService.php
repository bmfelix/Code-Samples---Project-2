<?php namespace App\Customs;

use Config;
use App\Helpers\Curl;
use Carbon\Carbon;
use Mail;
use Log;
use App\Http\Controllers\CustomsController;
use App\Jobs\RunCustomsJobs;
use App\Http\Models\Preorders;
use App\Http\Models\Garments;

class CustomsService
{

    public function getPreorderData($randomnum)
    {
        $order = Preorders::where('generated_url','like', "%".$randomnum)->first();
        $shirt_types = unserialize($order->shirt_types);
        $garments = unserialize($order->garments);
        $garment_info = Garments::all();
        $shirt_types['garments'] = $garments['garments'];

        $shirts = array();
        foreach($shirt_types['garments'] as $key => $value){
            foreach($shirt_types['colors'] as $k => $v) {
                if($value['quote'] == 1 && $v['quote'] == 1){
                    foreach($garment_info as $g){
                        if($value['name'] === $g->garment)
                        $shirts[$value['name']][] = [
                            "color" => $v['color'],
                            "xs" => $g->xsmall,
                            "xxxl" => $g->xxxlarge,
                            "xxxxl" => $g->xxxxlarge
                        ];
                    }
                }
            }
        }

        $order_array = array(
            "id" => $order->id,
            "name" => $order->name,
            "shirts" => $shirts,
            "customerimage1" => (isset($order['0']['customerimage1']))?$order['0']['customerimage1']:NULL,
            "customerimage2" => (isset($order['0']['customerimage2']))?$order['0']['customerimage2']:NULL,
            "customerimage3" => (isset($order['0']['customerimage3']))?$order['0']['customerimage3']:NULL,
            "customerimage4" => (isset($order['0']['customerimage4']))?$order['0']['customerimage4']:NULL,
            "customerimage5" => (isset($order['0']['customerimage5']))?$order['0']['customerimage5']:NULL,
        );

        // echo '<pre>';
        // print_r($order_array);
        // echo '</pre>';
        // die;

        return $order_array;
    }

}
