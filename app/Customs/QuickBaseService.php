<?php namespace App\Customs;

use Config;
use App\Helpers\Curl;
use Carbon\Carbon;
use Exception;
use Mail;
use Log;
use Excel;
use App\Http\Models\Products;
use App\Http\Models\Sleeves;
use App\Http\Models\Contacts;
use App\Http\Models\Garments;

class QuickBaseService
{
	protected $auth;

	public function startQuickBaseImport($type)
	{
		set_time_limit(0);
		if($type === "products"){
			$file = storage_path() . "/app/products.csv";
			$this->buildProductsArray($file);

		} else if ($type === "sleeves"){
			$file = storage_path() . "/app/sleeves.csv";
			$this->buildLineItemArray($file);
		} else if ($type === "contacts"){
			$file = storage_path() . "/app/contacts.xml";
			$this->buildCustomerArray($file);
		} else if ($type === "garments"){
			$file = storage_path() . "/app/singlelineitems.csv";
			$this->buildGarmentArray($file);
		} else {
			return;
		}


	}

	private function buildProductsArray($file)
	{
		Excel::load($file, function($reader) {
			// Getting all results
			$this->processProductData($reader->get()->toArray());
		});

		return;
	}

	private function processProductData($data)
	{
		foreach($data as $d){
			$price = str_replace(',', '', str_replace('$', '', $d['price']));

			$existing_product = $this->doesProductExist($d['product'],$price, $d['spots'],$d['min_pieces'],$d['max_pieces']);

			if(!$existing_product){
				$this->saveProductData($d['product'],$price, $d['spots'],$d['min_pieces'],$d['max_pieces']);
			} else {
				continue;
			}
		}
	}

	private function saveProductData($prod,$price,$spots,$min,$max)
	{
		$product = new Products;
		$product->product = $prod;
		$product->price = $price;
		$product->spots = $spots;
		$product->min_pieces = $min;
		$product->max_pieces = $max;
		$product->save();
	}

	private function doesProductExist($product,$price,$spots,$min,$max)
	{

        
		$product_data = Products::where('product', $product)
								->where('price', $price)
								->where('spots',$spots)
								->where('min_pieces',$min)
								->where('max_pieces', $max)->first();
        

        return $product_data;

	}

	private function buildLineItemArray($file)
	{
		Excel::load($file, function($reader) {
			// Getting all results
			$this->processLineItem($reader->get()->toArray());
		});

		return;
	}

	private function processLineItem($data)
	{
		foreach($data as $d){
			if(empty($d['sleeve'])){
				continue;
			}
			$existing_sleeves = $this->doesLineItemExist($d['sleeve']);

			if(empty($existing_sleeves)){
				$this->saveLineItem($d['sleeve']);
			} else {
				continue;
			}
		}
	}

	private function saveLineItem($option)
	{
		$sleeves = new Sleeves;
		$sleeves->options = $option;
		$sleeves->save();
	}

	private function doesLineItemExist($option)
	{

        if(!$option){
            $sleeves = array();
        } else {
            $sleeves = Sleeves::where('options', $option)->get()->toArray();
        }

        return $sleeves;

	}

	private function buildCustomerArray($file)
	{
		$xml = simplexml_load_file($file);
		foreach ($xml->record as $contact){

			$email = (string)$contact->email;
			$name = trim(preg_replace('/\s+/', ' ', (string)$contact->contact_full_name));

			if (!empty($email) && !empty($name) && ($email != $name)){
				$record = array(
					'name' 		   => $name,
					'company_name' => (string)$contact->company_name,
					'phone'		   => (string)$contact->phone,
					'mobile'	   => (string)$contact->mobile,
					'email'		   => $email,
				);

				$exists = $this->doesContactExist($record);

				if (empty($exists)){
					$this->updateContactTable($record);
				}
			}
		}

		return;

	}

	private function updateContactTable($record)
	{

		$contacts = new Contacts;
		$contacts->name = $record['name'];
		$contacts->company_name = $record['company_name'];
		$contacts->phone = $record['phone'];
		$contacts->mobile = $record['mobile'];
		$contacts->email = $record['email'];
		$contacts->save();
		return;

	}

	private function doesContactExist($record)
	{
		if(empty($record)){
            $contact = array();
        } else {
            $contact = Contacts::where('email', $record['email'])->get()->toArray();
        }

        return $contact;

	}

	private function buildGarmentArray($file)
	{
		Excel::load($file, function($reader) {
			// Getting all results
			$this->processGarmentData($reader->get()->toArray());
		});

		return;

		return;

	}

	private function processGarmentData($data)
	{
		foreach($data as $d){
			$existing_garment = $this->doesGarmentExist($d['garment']);

			if(empty($existing_garment)){
				$this->updateGarmentTable($d['garment']);
			} else {
				continue;
			}
		}
	}

	private function updateGarmentTable($record)
	{

		$garments = new Garments;
		$garments->garment = $record;
		$garments->save();
		return;

	}

	private function doesGarmentExist($record)
	{
		if(empty($record)){
            $garments = array();
        } else {
            $garments = Garments::where('garment', $record)->get()->toArray();
        }

        return $garments;

	}

}
