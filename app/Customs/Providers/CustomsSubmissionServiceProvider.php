<?php namespace App\Customs\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Foundation\AliasLoader;

class CustomsSubmissionServiceProvider extends ServiceProvider
{

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{

        AliasLoader::getInstance()->alias('CustomsSubmissionService', 'App\Customs\Facades\CustomsSubmissionServiceFacade');

		$this->app->bind('customssubmissionservice', function($app)
		{
			return new \App\Customs\CustomsSubmissionService;
		});
	}
}
