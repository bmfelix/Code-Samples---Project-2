<?php namespace App\Customs\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Foundation\AliasLoader;

class CustomsServiceProvider extends ServiceProvider
{

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{

		$this->app->register('App\Customs\Providers\CustomsMiscServiceProvider');
        $this->app->register('App\Customs\Providers\CustomsSubmissionServiceProvider');
		$this->app->register('App\Customs\Providers\CustomsGraphicsServiceProvider');
		$this->app->register('App\Customs\Providers\CustomsAdminServiceProvider');
        $this->app->register('App\Customs\Providers\CustomsShopifyServiceProvider');
		$this->app->register('App\Customs\Providers\QuickBaseServiceProvider');

        AliasLoader::getInstance()->alias('CustomsService', 'App\Customs\Facades\CustomsServiceFacade');

		$this->app->bind('customsservice', function($app)
		{
			return new \App\Customs\CustomsService;
		});
	}
}
