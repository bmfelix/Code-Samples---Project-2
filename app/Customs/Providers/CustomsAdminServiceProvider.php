<?php namespace App\Customs\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Foundation\AliasLoader;

class CustomsAdminServiceProvider extends ServiceProvider
{

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{

        AliasLoader::getInstance()->alias('CustomsAdminService', 'App\Customs\Facades\CustomsAdminServiceFacade');

		$this->app->bind('customsadminservice', function($app)
		{
			return new \App\Customs\CustomsAdminService;
		});
	}
}
