<?php namespace App\Customs\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Foundation\AliasLoader;

class CustomsGraphicsServiceProvider extends ServiceProvider
{

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{

        AliasLoader::getInstance()->alias('CustomsGraphicsService', 'App\Customs\Facades\CustomsGraphicsServiceFacade');

		$this->app->bind('customsgraphicsservice', function($app)
		{
			return new \App\Customs\CustomsGraphicsService;
		});
	}
}
