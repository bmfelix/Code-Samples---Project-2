<?php namespace App\Customs\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Foundation\AliasLoader;

class CustomsShopifyServiceProvider extends ServiceProvider
{

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
        AliasLoader::getInstance()->alias('CustomsShopifyService', 'App\Customs\Facades\CustomsShopifyServiceFacade');

		$this->app->bind('customsshopifyservice', function($app)
		{
			return new \App\Customs\CustomsShopifyService;
		});
	}
}
