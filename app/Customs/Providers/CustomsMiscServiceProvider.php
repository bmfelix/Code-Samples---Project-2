<?php namespace App\Customs\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Foundation\AliasLoader;

class CustomsMiscServiceProvider extends ServiceProvider
{

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{

        AliasLoader::getInstance()->alias('CustomsMiscService', 'App\Customs\Facades\CustomsMiscServiceFacade');

		$this->app->bind('customsmiscservice', function($app)
		{
			return new \App\Customs\CustomsMiscService;
		});
	}
}
