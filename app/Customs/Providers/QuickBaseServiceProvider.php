<?php namespace App\Customs\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Foundation\AliasLoader;

class QuickBaseServiceProvider extends ServiceProvider
{

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		AliasLoader::getInstance()->alias('QuickBaseService', 'App\Customs\Facades\QuickBaseServiceFacade');

		$this->app->bind('quickbaseservice', function($app)
		{
			return new \App\Customs\QuickBaseService;
		});
	}
}
