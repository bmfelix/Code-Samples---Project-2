<?php namespace App\Customs\Facades;

use Illuminate\Support\Facades\Facade;

class CustomsServiceFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'customsservice';
    }
}
