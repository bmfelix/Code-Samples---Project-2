<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Mail;

class Curl
{
    /**
     * @param $url
     * @param $json
     * @return mixed
     */
    public static function put($url, $json)
    {
        usleep(500000);
        $curl = curl_init();
        // Set some options - we are passing in a useragent too here
        curl_setopt_array($curl, [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $url,
            CURLOPT_CUSTOMREQUEST => "PUT",
            CURLOPT_POSTFIELDS => $json,
            CURLOPT_HTTPHEADER => ['Content-Type:application/json']
        ]);
        // Send the request & save response to $resp
        $resp = curl_exec($curl);
        // Close request to clear up some resources
        curl_close($curl);
        self::checkForErrors($resp, $url);

        return $resp;
    }

    /**
     * @param $resp
     * @param $url
     */
    private static function checkForErrors($resp, $url)
    {

        
    }

    /**
     * @param $url
     * @param $json
     * @return mixed
     */
    public static function post($url, $json)
    {
        usleep(500000);
        $curl = curl_init();
        // Set some options - we are passing in a useragent too here
        curl_setopt_array($curl, [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $url,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $json,
            CURLOPT_HTTPHEADER => ['Content-Type:application/json']
        ]);
        // Send the request & save response to $resp
        $resp = curl_exec($curl);
        // Close request to clear up some resources
        curl_close($curl);
        self::checkForErrors($resp, $url);

        return $resp;
    }

    /**
     * @param $url
     * @return mixed
     */
    public static function get($url,$auth = "")
    {
        
        $curl = curl_init();
        // Set some options - we are passing in a useragent too here
        if($auth){
            $header = [];
            $header[] = 'Content-Type:application/json';
            $header[] = 'Authorization: Basic ' . $auth;
        } else {
            $header = [];
        }

        curl_setopt_array($curl, [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $url,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => $header
        ]);
        // Send the request & save response to $resp
        $resp = curl_exec($curl);
        // Close request to clear up some resources
        curl_close($curl);
        self::checkForErrors($resp, $url);

        return $resp;
    }
}
