<?php

namespace App\Http\Controllers;

use Request;
use Response;
use View;
use Validator;
use Redirect;
use Hash;
use Mail;
use Carbon\Carbon;
use Session;
use App\Http\Models\Employees;
use App\Http\Models\Roles;
use App\Http\Models\Preorders;
use App\Http\Models\Order_sizes;
use App\Http\Models\Products;
use App\Http\Models\Contacts;
use App\Http\Models\Quotes;
use App\Http\Models\Shirts;
use App\Http\Models\Garments;
use App\Http\Models\Colors;
use App\Http\Models\InkColors;
use App\Http\Models\Shirt_types;
use App\Http\Models\Sleeves;
use App\Http\Models\LineItems;
use App\Http\Models\Designers;
use App\Http\Models\Projects;
use App\Http\Models\ProjectStatus;
use App\Http\Models\ProjectFiles;
use App\Http\Models\ProjectRevisions;
use App\Http\Models\Statuses;
use App\Http\Models\ProjectHistory;
use App\Http\Models\QuoteBuilder;
use App\Http\Models\DocTypes;
use App\Http\Models\RevTypes;
use Illuminate\Http\Request as IRequest;
use CustomsAdminService;
use CustomsMiscService;
use CustomsShopifyService;
use PDF;
use Config;

class CustomsAdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.customsales');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(IRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(IRequest $request, $id)
    {

        $result = CustomsAdminService::updateEmployee($request, $id);

        return $result;
    }

    public function updateProducts(IRequest $request, $id){
        $result = CustomsAdminService::updateProducts($request, $id);

        return $result;
    }

    public function updateGarments(IRequest $request, $id){
        $result = CustomsAdminService::updateGarments($request, $id);

        return $result;
    }

    public function updateContacts(IRequest $request, $id){
        $result = CustomsAdminService::updateContacts($request, $id);

        return $result;
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $salesperson = Employees::find($id);
        $salesperson->delete();
        return response()->json([
            'success' => true,
            'status' => 'Employee Removed'
        ], 200);
    }

    public function destroyProduct($id)
    {
        //
        $product = Products::find($id);
        $product->delete();
        return response()->json([
            'success' => true,
            'status' => 'Product "'.$product->style .'" Removed',
        ], 200);
    }

    public function destroyGarments($id)
    {
        //
        $product = Garments::find($id);
        $product->delete();
        return response()->json([
            'success' => true,
            'status' => 'Garment Removed',
        ], 200);
    }

    public function destroyContact($id)
    {
        //
        $contact = Contacts::find($id);
        $contact->delete();
        return response()->json([
            'success' => true,
            'status' => 'Contact "'.$contact->name .'" Removed',
        ], 200);
    }

    public function removeLineItem($id)
    {
        //
        $items = LineItems::find($id);
        $items->delete();
        return response()->json([
            'success' => true,
            'status' => 'Line Item Removed',
        ], 200);
    }

    public function addProducts()
    {
        return view('designer.custom_admin.addproducts');
    }

    public function addGarments()
    {
        return view('designer.custom_admin.addgarments');
    }

    public function addNewProducts(IRequest $request)
    {
        $result = CustomsAdminService::addNewProduct($request);

        return $result;
    }

    public function addNewGarments(IRequest $request)
    {
        $result = CustomsAdminService::addNewGarment($request);

        return $result;
    }

    public function addContacts()
    {
        return view('designer.custom_admin.addcontacts');
    }

    public function addNewContacts(IRequest $request)
    {
        $result = CustomsAdminService::addNewContact($request);

        return $result;
    }

    public function addProject()
    {
        $contacts = CustomsAdminService::buildContactsArray();
        $designers = CustomsAdminService::buildDesignersArray();
        $salesperson = \Auth::user()->id;
        $order_requests = CustomsAdminService::buildOrderRequestArray();
        $salespersons = CustomsAdminService::buildSalesPersonArray();
        return view('designer.custom_admin.addproject', compact('contacts','designers','salesperson','order_requests','salespersons'));
    }

    public function addNewProject(IRequest $request)
    {
        $result = CustomsAdminService::addNewProject($request);

        return $result;
    }

    public function Employees()
    {
        return View('designer.custom_admin.employees');
    }

    public function addEmployee()
    {
        $employee = new \stdClass();
        $current_user_role = (int)\Auth::user()->role_id;
        if($current_user_role === 1){
            $roles = Roles::get(['id','role'])->toArray();
            $roles = CustomsAdminService::rebuildRolesArrayForSelect($roles);
            $blank_value = array(0 => "");
            $roles = array_merge($blank_value, $roles);
        } else {
            $roles = Roles::where('id','>',$current_user_role)->get(['id','role'])->toArray();
            $roles = CustomsAdminService::rebuildRolesArrayForSelect($roles);
            $blank_value = array(0 => "");
            $roles = array_merge($blank_value, $roles);
        }
        return view('designer.custom_admin.addsalesperson', compact('employee','roles'));
    }

    public function addNewEmployee(IRequest $request)
    {
        $result = CustomsAdminService::addNewEmployee($request);

        return $result;
    }

    public function editEmployee($id)
    {
        $employee = Employees::findOrFail($id);
        $current_user_role = (int)\Auth::user()->role_id;
        if($current_user_role === 1){
            $roles = Roles::get(['id','role'])->toArray();
            $roles = CustomsAdminService::rebuildRolesArrayForSelect($roles);
            $blank_value = array(0 => "");
            $roles = array_merge($blank_value, $roles);
        } else {
            $roles = Roles::where('id','>',$current_user_role)->get(['id','role'])->toArray();
            $roles = CustomsAdminService::rebuildRolesArrayForSelect($roles);
            $blank_value = array(0 => "");
            $roles = array_merge($blank_value, $roles);
        }

        return view('designer.custom_admin.editsalesperson', compact('employee','roles'));
    }

    public function ajaxUpload(IRequest $request, $id)
    {
        $result = CustomsAdminService::processAjaxUpload($request, $id);

        return $result;
    }

    public function ajaxRequestUpload(IRequest $request, $id)
    {
        $result = CustomsAdminService::processRequestAjaxUpload($request, $id);

        return $result;
    }

    public function editProducts($id){
            $product = Products::findOrFail($id);
            return view('designer.custom_admin.editproducts', compact('product'));

    }

    public function editGarments($id){
        $garment = Garments::findOrFail($id);
        return view('designer.custom_admin.editgarments', compact('garment'));

    }

    public function editContacts($id){
            $contact = Contacts::findOrFail($id);

            if(isset($contact->id)){
                $projects = Projects::where('company_id',$contact->id)->get();
                foreach($projects as $project){
                    $sp = Employees::where('id',$project->salesperson_id)->first();
                    $project->salesperson = $sp->name;

                }
            } else {
                $projects = [];
            }
            
            return view('designer.custom_admin.editcontacts', compact('contact','projects'));

    }

    public function viewProjectHistory($id)
    {
        $project = Projects::findOrFail($id);
        $project_status = CustomsAdminService::buildProjectStatusTypeArray();
        $salespersons = CustomsAdminService::buildSalesPersonArray();
        $production_status = CustomsAdminService::buildProductionStatusTypeArray();
        $contacts = CustomsAdminService::buildContactsArray();
        $project_history = ProjectHistory::where("project_id", $id)->orderBy('created_at', 'DESC')->paginate(10);
        foreach($project_history as $ph){
            $employee = Employees::where('id','=',$ph->employee_id)->first();
            $ph->employee_name = $employee->name;
            $ph->created_at = Carbon::parse($ph->created_at)->setTimezone('America/Chicago');

            switch($ph->history_type){
                case "project status":
                    foreach($project_status as $k=>$v){
                        if($ph->old_value == $k){
                            $ph->old_value_text = $v;
                        }
    
                        if($ph->new_value == $k){
                            $ph->new_value_text = $v;
                        }
                    }
                    break;

                case "production status":
                    foreach($production_status as $k=>$v){
                        if($ph->old_value == $k){
                            $ph->old_value_text = $v;
                        }
    
                        if($ph->new_value == $k){
                            $ph->new_value_text = $v;
                        }
                    }
                    break;

                case "company id":
                    foreach($contacts as $k=>$v){
                        if($ph->old_value == $k){
                            $ph->old_value_text = $v;
                        }
    
                        if($ph->new_value == $k){
                            $ph->new_value_text = $v;
                        }
                    }
                    $ph->history_type = "company changed";
                    break;

                case "salesperson":
                    foreach($salespersons as $k=>$v){
                        if($ph->old_value == $k){
                            $ph->old_value_text = $v;
                        }
    
                        if($ph->new_value == $k){
                            $ph->new_value_text = $v;
                        }
                    }
                    break;
                case "graphic designer":
                    foreach($salespersons as $k=>$v){
                        if($ph->old_value == $k){
                            $ph->old_value_text = $v;
                        }
    
                        if($ph->new_value == $k){
                            $ph->new_value_text = $v;
                        }
                    }
                    break;

                default:
                    $ph->old_value_text = $ph->old_value;
                    $ph->new_value_text = $ph->new_value;
            }

        }

        $view = Response::json(View::make('designer.custom_admin.projecthistory', compact('project_history'))->render());
        
        return $view;
    }

    public function editProject($id){

        $project = Projects::findOrFail($id);
            
            // $shirt_types = Shirt_types::all();

            

            // $mst = [];
            // foreach($shirt_types as $shirt_type){
            //     $mst[$shirt_type->type] = $shirt_type->type;
            // }
            // $mst = serialize($mst);
            // compact('project','activity','contacts','designers','salespersons','order_request','order_sizes','doctypes','revtypes','note_locations','project_status','production_status','role','mst','quote','lineitems')

            return view('designer.custom_admin.editproject', compact('project'));

    }

    public function OrderRequests(){
        $rows = Preorders::paginate(10);
        $host = Config::get('app.sales_url');
        $search = "";
        
        
        foreach($rows as $row){
            $createdAt = Carbon::parse($row->created_at)->setTimezone('America/Chicago');
            $timecopyA = clone $createdAt;
            $timecopyB = clone $createdAt;

            $time = new \stdClass();
            $time->original = $createdAt;
            $time->plus = $timecopyA->addMinutes(5);
            $time->minus = $timecopyB->subMinutes(5);

            $customer = Contacts::where('name', '=', $row->name)
                                ->where('email','=', $row->email)->first();
            
            if(empty($customer)){
                $contact = new Contacts();
                $contact->name = $row->name;
                $contact->email = $row->email;
                $contact->phone = (isset($row->phone)?$row->phone:"");
                $contact->save();
                $customer = $contact;
            }

            $salesperson = CustomsMiscService::getEmployee($row->salesperson);
            foreach($salesperson as $sp){
                $sp_id = $sp->id;
            }
            

            $project = Projects::where('salesperson_id', '=', $sp_id)
                                ->where('company_id','=',$customer->id)
                                ->where('created_at','>=', $time->minus)
                                ->where('created_at','<=',$time->plus)->first();


            if(!empty($project)){
                $row->project_exists = true;
            } else {
                $row->project_exists = false;
            }

        }
        

        if (Request::ajax() && isset($_GET['page'])) {
            $view = Response::json(View::make('designer.custom_admin.orderrequests', compact('rows','host','search'))->render());
            return $view;
        }

        return View('designer.custom_admin.orderrequests', compact('rows','host','search'));
    }

    public function editOrderRequest($id){
        $order = Preorders::findOrFail($id);
        $order_sizes = Order_sizes::where('order_id', $id)->get()->toArray();
        $host = Config::get('app.sales_url');
        $shirt_types = Shirt_types::all();
        $mst = [];
        foreach($shirt_types as $shirt_type){
            $mst[$shirt_type->type] = $shirt_type->type;
        }
        $mst = serialize($mst);

        return view('designer.custom_admin.editorderrequest', compact('order', 'order_sizes', 'host', 'mst'));
    }

    public function destroyOrder($id){
        $order = Preorders::find($id);
        $order->delete();
        return back()->with('status','Order Removed');
    }

    public function getRandomString($id){

        do {
            $randomstring = CustomsAdminService::generateRandomString(8);
            $random = Preorders::where('generated_url', '=', $randomstring)->exists();
        } while ($random === true);

        $this->saveGeneratedUrl($id,$randomstring);
    }



    public function updateOrderDatabase(IRequest $request, $id){

        $result = CustomsAdminService::updateOrderDatabase($request, $id);

        if($result === TRUE){
            return response()->json([
                'success' => true,
                'status' => 'Order Updated'
            ], 200);
            //return redirect()->route('admin.orderrequests')->with('status','Order Updated');
        } else {
            return response()->json([
                'success' => false,
                'status' => 'Order Not Found'
            ], 422);
        }
    }

    public function sizeSelections($id){

        $salesperson = CustomsAdminService::getSizesFromRequestedQuote($id, $_POST);
        $order = Preorders::where('generated_url','like','%'.$id)->first();
        $project = Projects::where('request_id',$order->id)->first();
        $this->createQuote($project->id);

        return Redirect::route('custom.home', array('salesperson' => $salesperson))->with('status','Order Sizes Received, Thank you!');

    }

    function custom_complete_artwork(IRequest $request){

        $result = CustomsAdminService::generateIdsAndMoveImages($request);
        return $result;
    }

    public function viewProducts()
    {
        return View('designer.custom_admin.products');
    }

    public function viewGarments()
    {
        return View('designer.custom_admin.garments');
    }

    public function viewContacts()
    {
        return View('designer.custom_admin.contacts');
    }

    public function viewTheContactData()
    {
        $rows = Contacts::paginate(10);
        $search = "";

        return View('designer.custom_admin.main_sub_views.contacts', compact('rows','search'));
    }

    public function viewTheGarmentData()
    {
        $rows = Garments::paginate(10);
        $search = "";

        foreach($rows as $row){
            if($row->xsmall === 0){
                $row->xsmall = "No";
            } else {
                $row->xsmall = "Yes";
            }

            if($row->xxxlarge === 0){
                $row->xxxlarge = "No";
            } else {
                $row->xxxlarge = "Yes";
            }

            if($row->xxxxlarge === 0){
                $row->xxxxlarge = "No";
            } else {
                $row->xxxxlarge = "Yes";
            }
        }

        return View('designer.custom_admin.main_sub_views.garments', compact('rows','search'));
    }

    public function viewTheProductData()
    {
        $rows = Products::paginate(10);
        $search = "";

        return View('designer.custom_admin..main_sub_views.products', compact('rows','search'));
    }

    public function viewTheEmployeeData()
    {
        $rows = Employees::paginate(10);
        $current_user_role = (int)\Auth::user()->role_id;

        return View('designer.custom_admin..main_sub_views.employees', compact('rows','current_user_role'));
    }

    public function createQuote($id)
    {
        $project = Projects::where('id',$id)->first();
        $quote = CustomsAdminService::startNewQuote($id);
        $lineitems = CustomsAdminService::getLineItemsByProjectId($id);
        $order_sizes = Order_sizes::where('order_id', $project->request_id)->get();
        if($lineitems->isEmpty() && $order_sizes->isNotEmpty()){
            CustomsAdminService::AddLineItems($order_sizes, $project);
            $lineitems = CustomsAdminService::getLineItemsByProjectId($id);
        } else {
            return response()->json([
                'success' => false,
                'result' => 'quote already exists',
            ], 200);
        }

        $total_items = CustomsAdminService::getTotalItems($lineitems);

        if($total_items < 50){
            return response()->json([
                'success' => false,
                'result' => 'Cannot create a quote for less than 50 items',
            ], 200);
        } else {
            
            $calc_subtotal = CustomsAdminService::calcSubTotal($quote,$lineitems);
            $calc_shipping = CustomsAdminService::calcShipping($quote, $lineitems);
            $calc_tax = CustomsAdminService::calcTax($quote, $lineitems, $calc_subtotal, $calc_shipping);
            $calc_total = CustomsAdminService::calcTotal($quote,$lineitems,$calc_shipping,$calc_tax,$calc_subtotal);

            return response()->json([
                'success' => true,
                'result' => 'Quote Created',
            ], 200);
        }
        
        return;
        
    }

    public function updateQuotePricing($id)
    {
        $project = Projects::where('id',$id)->first();
        $quote = CustomsAdminService::startNewQuote($id);
        $lineitems = CustomsAdminService::getLineItemsByProjectId($id);
        $lineitems = CustomsAdminService::updateLineItemTotal($lineitems, $id);
            
        $calc_subtotal = CustomsAdminService::calcSubTotal($quote,$lineitems);
        $calc_shipping = CustomsAdminService::calcShipping($quote, $lineitems);
        $calc_tax = CustomsAdminService::calcTax($quote, $lineitems, $calc_subtotal, $calc_shipping);
        $calc_total = CustomsAdminService::calcTotal($quote,$lineitems,$calc_shipping,$calc_tax,$calc_subtotal);

        $quote = Quotes::where('order_id', $project->request_id)->first();
        $quote->subtotal = $calc_subtotal;
        $quote->shipping_charged = $calc_shipping;
        $quote->tax = $calc_tax;
        $quote->order_total = $calc_total;
        $quote->save();


        return response()->json([
            'success' => true,
            'result' => 'Quote Pricing Updated',
        ], 200);
        
        
        return;
    }

    public function viewQuote($id)
    {
        $salesperson_id = \Auth::user()->id;
        $quote = Quotes::where('project_id', '=', $id)->first();
        $quote = CustomsAdminService::getNumericValues($quote);
        $quote = json_encode($quote);

        return $quote;

    }

    public function viewProjects()
    {
        $statuses = Statuses::orderBy('status','asc')->get();
        foreach($statuses as $status){
            $sid = $status->id;
            $myprojects = Projects::whereHas('projstatus', function($query) use($sid) {
                $query->where('status_id', $sid);
            })->where('designer_id', NULL)->get();
            $count = sizeof($myprojects);
            $status->num = $count;
        }

        return View('designer.custom_admin.projects',compact('statuses'));

    }

    public function myProjects()
    {
        $user = \Auth::user();
        $user_id = $user->id;

        $statuses = Statuses::orderBy('status','asc')->get();
        foreach($statuses as $status){
            $sid = $status->id;
            if($user->role_id == 1 || $user->role_id == 4 || $user->role_id == 2){
                $myprojects = Projects::whereHas('projstatus', function($query) use($sid) {
                    $query->where('status_id', $sid);
                })->where('salesperson_id', $user_id)->paginate(10);
            } else if($user->role_id == 3) {
                $myprojects = Projects::whereHas('projstatus', function($query) use($sid) {
                    $query->where('status_id', $sid);
                })->where('designer_id', $user_id)->paginate(10);
            } else if($user->role_id == 5) {
                $myprojects = Projects::whereHas('projstatus', function($query) use($sid) {
                    $query->where('status_id', $sid);
                })->where('designer_id', NULL)->get();
            }
            $count = sizeof($myprojects);
            $status->num = $count;
        }

        return View('designer.custom_admin.sub_views.myprojects', compact('statuses'));

    }

    public function projectsByStatus($id)
    {
        $user_type = FALSE;
        $projects = "";

        $user = \Auth::user();
        $user_id = $user->id;

        if($user->role_id == 1 || $user->role_id == 4 || $user->role_id == 2){
            $myprojects = Projects::whereHas('projstatus', function($query) use($id) {
                $query->where('status_id', $id);
            })->where('salesperson_id', $user_id)->paginate(10);
        } else if($user->role_id == 3) {
            $myprojects = Projects::whereHas('projstatus', function($query) use($id) {
                $query->where('status_id', $id);
            })->where('designer_id', $user_id)->paginate(10);
        } else if($user->role_id == 5) {
            $myprojects = Projects::whereHas('projstatus', function($query) use($id) {
                $query->where('status_id', $id);
            })->where('designer_id', NULL)->paginate(10);
        }


        foreach ($myprojects as $project){
            $pstatus = ProjectStatus::where('project_id', $project->id)->orderBy('created_at','DESC')->first();
            $proj_status_name = Statuses::findOrFail($pstatus->status_id);
            $project->status = $proj_status_name->status;
            $salesperson = Employees::where('id',$project->salesperson_id)->first();
            $project->salesperson = $salesperson->name;
            $designer = Employees::where('id',$project->designer_id)->first();
            $project->designer = (isset($designer->name)?$designer->name:"");
            
        }
        return View('designer.custom_admin.sub_views.statusview', compact('myprojects'));
    }

    public function allProjectsByStatus($id)
    {
        $user_type = FALSE;
        $projects = "";

        $myprojects = Projects::whereHas('projstatus', function($query) use($id) {
            $query->where('status_id', $id);
        })->paginate(10);

        foreach ($myprojects as $project){
            $pstatus = ProjectStatus::where('project_id', $project->id)->orderBy('created_at','DESC')->first();
            $proj_status_name = Statuses::findOrFail($pstatus->status_id);
            $project->status = $proj_status_name->status;
            $salesperson = Employees::where('id',$project->salesperson_id)->first();
            $project->salesperson = $salesperson->name;
            $designer = Employees::where('id',$project->designer_id)->first();
            $project->designer = (isset($designer->name)?$designer->name:"");
            
        }
        return View('designer.custom_admin.sub_views.statusview', compact('myprojects'));
    }

    public function allProjects()
    {

        $statuses = Statuses::orderBy('status','asc')->get();
        foreach($statuses as $status){
            $sid = $status->id;
            $myprojects = Projects::whereHas('projstatus', function($query) use($sid) {
                $query->where('status_id', $sid);
            })->get();
            $count = sizeof($myprojects);
            $status->num = $count;
        }

        return View('designer.custom_admin.sub_views.allprojects', compact('statuses'));
    }

    public function buildRevision(IRequest $request, $id)
    {
        $result = CustomsAdminService::buildRevision($request, $id);
        return $result;
    }

    public function destroyQuote($id)
    {
        $quote = Quotes::find($id);
        $lineitems = LineItems::where('order_id','=',$id)->get();
        foreach($lineitems as $item){
            $item->delete();
        }

        $quote->delete();
        return ;
    }

    public function destroyProject($id)
    {
        $quote = Projects::find($id);
        $quote->delete();
        return response()->json([
            'success' => true,
            'status' => 'Project Removed',
        ], 200);
    }


    public function addNewQuotation(IRequest $request, $id)
    {
        $response = CustomsAdminService::updateSalesQuote($request, $id);
        return $response;
    }

    public function getProducts()
    {
        $products = Products::all('id','style','price')->toArray();
        $products = json_encode($products);
        return $products;
    }

    public function getContactInfo($id)
    {
        $contacts = Contacts::where('id','=',$id)->get()->toArray();
        $contacts = json_encode($contacts);
        return $contacts;
    }

    public function getColors()
    {
        $colors = Colors::all('id','name')->toArray();
        $colors = json_encode($colors);
        return $colors;
    }

    public function getGarments()
    {
        $garments = Garments::all('id','garment')->toArray();
        $garments = json_encode($garments);
        return $garments;
    }

    public function getGarmentTypes()
    {
        $types = Shirt_types::all('id','type')->toArray();
        $types = json_encode($types);
        return $types;
    }

    public function getInkColors()
    {
        $inks = InkColors::all('id','name')->toArray();
        $inks = json_encode($inks);
        return $inks;
    }

    public function getSleeves()
    {
        $sleeves = Sleeves::all('id','options')->toArray();
        $sleeves = json_encode($sleeves);
        return $sleeves;
    }

    public function editQuote($id)
    {
        $quotations = Quotes::where('order_id', '=', $id)->first();
        $quotations = CustomsAdminService::getNumericValuesOnCreation($quotations);
        $payment_types = CustomsAdminService::buildPaymentTypeArray();
        $form_types = CustomsAdminService::buildFormTypeArray();
        return View('designer.custom_admin.quotation', compact('quotations','contacts','payment_types','form_types'));

    }

    public function getLineItems($id)
    {
        $json = array();
        $items = LineItems::where('order_id','=',$id)->get()->toArray();

        foreach($items as $item){
            $json[] = array(
                "Id" => $item['id'],
                "Product" => CustomsAdminService::numericToStringConversion('Products', $item['product_id']),
                "Garment" => CustomsAdminService::numericToStringConversion('Garments', $item['garment']),
                "Garment #" => CustomsAdminService::numericToStringConversion('GarmentNumber', $item['garment_number']),
                "Color" => CustomsAdminService::numericToStringConversion('Color', $item['garment_color_id']),
                "Size" => $item['garment_size'],
                "Ink 1" => CustomsAdminService::numericToStringConversion('Ink', $item['ink_color_1']),
                "Ink 2" => CustomsAdminService::numericToStringConversion('Ink', $item['ink_color_2']),
                "Ink 3" => CustomsAdminService::numericToStringConversion('Ink', $item['ink_color_3']),
                "Ink 4" => CustomsAdminService::numericToStringConversion('Ink', $item['ink_color_4']),
                "Sleeves" => CustomsAdminService::numericToStringConversion('Sleeves', $item['sleeve_type_id']),
                "Price" => $item['item_price'],
                "Quantity" => $item['quantity'],
                "Total" => $item['linetotal'],
            );
        }
        $json = json_encode($json);
        return $json;
    }

    public function getRequestData($id)
    {
        $order = Preorders::findOrFail($id);
        $order->shirt_types = unserialize($order->shirt_types);
        $order->front = unserialize($order->front);
        $order->back = unserialize($order->back);
        $order->left_sleeve = unserialize($order->left_sleeve);
        $order->right_sleeve = unserialize($order->right_sleeve);

        $order = json_encode($order);

        return $order;
    }

    public function getProjectFiles($id)
    {
        $project_files = ProjectFiles::where('project_id','=',$id)->get();

        if(!$project_files->isEmpty())
        foreach ($project_files as $pf){
            $pf->doctype = $pf->doctypes->type;
            
            
            if ((int)$pf->final_art === 0){
                $final_art = "No";
            } else {
                $final_art = "Yes";
            }
            $pf->finalart = $final_art;
        }
        return json_encode($project_files);
    }

    public function getRevisions($id)
    {
        $revisions = ProjectRevisions::where('project_id',$id)->get();

        foreach ($revisions as $rv){
            $rv->revision_type = $rv->revtypes->type;
            $rv->rev_notes_location = $rv->notelocations->type;
            $rv->employee = $rv->employees->name;
        }

        return json_encode($revisions);
    }

    public function updateProject(IRequest $request, $id)
    {
        $result = CustomsAdminService::updateProject($request, $id);

        return $result;
    }

    public function sendToOdoo($id)
    {
        $result = CustomsAdminService::fireOrderToOdoo($id);

        return $result;
    }

    public function printQuote($id)
    {
        $quotation = Quotes::where('order_id', '=', $id)->first();
        $quotation = CustomsAdminService::getNumericValuesOnCreation($quotation);
        $quotation->created = Carbon::parse($quotation->created_at)->setTimezone('America/Chicago')->format('Y-m-d');
        $quotation->due = Carbon::parse($quotation->estimated_date_shipped)->setTimezone('America/Chicago')->format('Y-m-d');
        $project = Projects::where('id','=',$quotation->project_id)->first();
        $lineitems = LineItems::where('order_id','=',$id)->get();
        $running_total = 0.00;

        $invoice_title = $project->contacts->company_name . " " . Carbon::now()->setTimezone('America/Chicago')->format('Y-m-d') . " Invoice";

        $data = [
            "quotation" => $quotation,
            "project" => $project,
            "lineitems" =>$lineitems,
            "title"=>$invoice_title,
        ];



        $pdf = PDF::loadView('designer.custom_admin.quotepdf', $data);
        return $pdf->stream($invoice_title . '.pdf');

        //return View('designer.custom_admin.quotepdf', $data);
    }

    public function search(IRequest $request, $type)
    {
        switch($type){
            case "contacts":
                $rows = Contacts::where('name', 'like', '%'.$request->term.'%')
                            ->orWhere('company_name', 'like', '%'.$request->term.'%')
                            ->orWhere('phone', 'like', '%'.$request->term.'%')
                            ->orWhere('mobile', 'like', '%'.$request->term.'%')
                            ->orWhere('email', 'like', '%'.$request->term.'%')
                            ->paginate(10);
                $view = "designer.custom_admin.contacts";
                $host = Config::get('app.sales_url');
                break;
            case "preorders":
                $rows = Preorders::where('name', 'like', '%'.$request->term.'%')
                            ->orWhere('phone', 'like', '%'.$request->term.'%')
                            ->orWhere('email', 'like', '%'.$request->term.'%')
                            ->paginate(10);
                $host = Config::get('app.sales_url');
                $view = "designer.custom_admin.orderrequests";
                break;
            case "products":
                $rows = Products::where('style', 'like', '%'.$request->term.'%')
                            ->orWhere('price', 'like', '%'.$request->term.'%')
                            ->paginate(10);
                $view = "designer.custom_admin.products";
                $host = Config::get('app.sales_url');
                break;
            case "garments":
                $rows = Garments::where('garment', 'like', '%'.$request->term.'%')
                            ->orWhere('upcharge', 'like', '%'.$request->term.'%')
                            ->paginate(10);
                $view = "designer.custom_admin.garments";
                $host = Config::get('app.sales_url');
                break;
        }


        $search = $request->term;

        return Response::json(View::make($view, compact('rows','search','host'))->render());
    }

    public function addProjectHistory($id)
    {
        foreach($_POST as $k=>$v){
            $$k = $v;
        }

        switch($input_name){
            case "shirt_types_type[]":
                $input_name = "garment_change";
                break;
            case "shirt_types_color[]":
                $input_name = "garment color change";
                break;
            case "leftsleeveopts":
                $input_name = "left sleeve option";
                break;
            case "rightsleeveopts":
                $input_name = "right sleeve option";
                break;
        }

        $input_name = str_replace("_"," ",$input_name);

        $project_history = new ProjectHistory();
        $project_history->old_value = $old_value;
        $project_history->new_value = $new_value;
        $project_history->project_id = $id;
        $project_history->history_type = $input_name;
        $project_history->employee_id = \Auth::user()->id;
        $project_history->save();

        if(isset($project_history->id)){
            return response()->json([
                'success' => true,
                'result' => "success",
            ], 200);
        } else {
            return response()->json([
                'success' => false,
                'result' => 0,
            ], 200);
        }

        
    }

    public function getRequestShirtTypes($id)
    {
        $order = Preorders::where("id",$id)->first();
        if($order){
            $shirt_types = $this->convertShirtTypes(unserialize($order->shirt_types), "types");
            $stypes = Shirt_types::all();
            $mst = [];
            foreach($stypes as $stype){
                $mst[$stype->type] = $stype->type;
            }

            $role = $this->getUserRoleID();
            
            //return Response::json(View::make($view, compact('rows','search','host'))->render());
            return View('designer.custom_admin.sub_views.shirt_types', compact('shirt_types', 'mst', 'order', 'role'));
        }
    }

    public function convertShirtTypes($shirts, $type){
        if($type === "types"){

            foreach($shirts['types'] as $key=>$shirt){
                if(!isset($shirt['type'])){
                    $shirts['types'][] = [
                        "type" => $shirt,
                        "quote" => 1
                    ];
                    unset($shirts['types'][$key]);
                }
            }
            $shirts['types'] = $this->reindexNumeric($shirts['types'], 0);

        } else if($type === "colors") {

            foreach($shirts['colors'] as $key=>$shirt){
                if(!isset($shirt['color'])){
                    
                    $shirts['colors'][] = [
                        "color" => $shirt,
                        "quote" => 1
                    ];
                    unset($shirts['colors'][$key]);
                }
            }
            $shirts['colors'] = $this->reindexNumeric($shirts['colors'], 1);

        }
        
        return $shirts;
    }

    function reindexNumeric($array, $start)
    {
        $i = $start;
        foreach ($array as $key => $val) {
            if (ctype_digit((string) $key)) {
                $new_arr[$i++] = $val;
            } else {
                $new_arr[$key] = $val;
            }
        }
        return $new_arr;
    }

    public function changeRequestShirtInfo($id)
    {
        $order = Preorders::where("id",$id)->first();
        if($order){
            $shirt_types = unserialize($order->shirt_types);
            $colors = $shirt_types['colors'];
            $types = $shirt_types['types'];

            foreach($colors as $k=>$v){
                $colors[$k] = [
                    "color" => $v,
                    "quote" => 1
                ];
            }

            foreach($types as $k=>$v){
                $types[$k] = [
                    "type" => $v,
                    "quote" => 1
                ];
            }

            $shirt_types['colors'] = $colors;
            $shirt_types['types'] = $types;

            $order->shirt_types = serialize($shirt_types);
            $order->save();
            
        }
    }

    public function getRequestShirtColors($id)
    {
        $order = Preorders::where("id",$id)->first();
        if($order){
            $shirt_types = $this->convertShirtTypes(unserialize($order->shirt_types), "colors");
            $colors = Colors::all();
            $msc = CustomsAdminService::buildShirtColorsArray();
            $role = $this->getUserRoleID();
            
            //return Response::json(View::make($view, compact('rows','search','host'))->render());
            return View('designer.custom_admin.sub_views.shirt_colors', compact('shirt_types', 'msc','order','role'));
        }
    }

    public function getRequestShirtFront($id)
    {
        $order = Preorders::where("id",$id)->first();
        $host = Config::get('app.sales_url');

        if($order){
            $front = unserialize($order->front);
            
            foreach($front['colors'] as $k => $c){
                if($c === 41 || $c === "designers_choice"){
                    $front['colors'][$k] = "Designers Choice";
                }
            }

            if(!empty($front['colors'])){

                for($i = 1; $i <= 4; $i++){
                    if($front['colors']["ink".$i]){
                        $front['colors'][$i] = $front['colors']["ink".$i];
                        unset($front['colors']["ink".$i]);
                    } else {
                        $front['colors'][$i] = "";
                        unset($front['colors']["ink".$i]);
                    }
                }
                

                switch($front['placement']){
                    case "left_breast":
                        $front['placement'] = "Left Breast";
                    break;

                    case "right_breast":
                        $front['placement'] = "Right Breast";
                    break;

                    case "chest_centered":
                        $front['placement'] = "Chest Centered";
                    break;

                    case "large_as_possible":
                        $front['placement'] = "Large As Possible";
                    break;
                }

                $colors = CustomsAdminService::buildInksArray();
                $colors["Designers Choice"] = "Designers Choice";
                $placements = [
                    "Left Breast" => "Left Breast",
                    "Right Breast" => "Right Breast",
                    "Chest Centered" => "Chest Centered",
                    "Large As Possible" => "Large As Possible",
                ];

                $role = $this->getUserRoleID();
                
                return View('designer.custom_admin.sub_views.shirt_front', compact('front','colors','placements','host','role'));
            } else {
                return;
            }
        } else {
            return;
        }   
    }

    public function getRequestShirtBack($id)
    {
        $order = Preorders::where("id",$id)->first();
        $host = Config::get('app.sales_url');

        if($order){
            $back = unserialize($order->back);

            if($back['placement'] === ""){
                return;
            }

            foreach($back['colors'] as $k => $c){
                if($c === 41 || $c === "designers_choice"){
                    $back['colors'][$k] = "Designers Choice";
                }
            }

            if(!empty($back['colors'])){
                for($i = 1; $i <= 4; $i++){
                    if($back['colors']["ink".$i]){
                        $back['colors'][$i] = $back['colors']["ink".$i];
                        unset($back['colors']["ink".$i]);
                    } else {
                        $back['colors'][$i] = "";
                        unset($back['colors']["ink".$i]);
                    }
                }

                switch($back['placement']){
                    case "high_between_shoulders":
                        $back['placement'] = "Between Shoulders";
                    break;

                    case "middle_back":
                        $back['placement'] = "Middle Back";
                    break;

                    case "low_back":
                        $back['placement'] = "Low Back";
                    break;

                    case "full_back":
                        $back['placement'] = "Full Back";
                    break;
                }

                $colors = CustomsAdminService::buildInksArray();
                $colors["Designers Choice"] = "Designers Choice";
                $placements = [
                    "Between Shoulders" => "Between Shoulders",
                    "Middle Back" => "Middle Back",
                    "Low Back" => "Low Back",
                    "Full Back" => "Full Back",
                ];

                //$back = serialize($back);
                $role = $this->getUserRoleID();
                
                return View('designer.custom_admin.sub_views.shirt_back', compact('back','colors','placements','host','role'));
            } else {
                return;
            }
        } else {
            return;
        }
        
    }

    public function getRequestShirtLeftSleeve($id)
    {
        $order = Preorders::where("id",$id)->first();
        if($order){
            $left_sleeve = unserialize($order->left_sleeve);
            $sleeve_options = [
                "logo" => "Logo",
                "none" => "None",
                "left_custom" => "Left Custom",
            ];
            $lsleeve_option = explode("_",$left_sleeve['option']);
            if(sizeof($lsleeve_option) > 1){
                $lsleeve_option = ucwords($lsleeve_option[0] ." ".$lsleeve_option[1]);
            } else {
                $lsleeve_option = ucwords($lsleeve_option[0]);
            }
            
            $left_sleeve = serialize($left_sleeve);

        } else {
            $left_sleeve = [];
        }
        $host = Config::get('app.sales_url');
        $role = $this->getUserRoleID();
            
        //return Response::json(View::make($view, compact('rows','search','host'))->render());
        return View('designer.custom_admin.sub_views.shirt_lsleeve', compact('left_sleeve','sleeve_options','host','role','lsleeve_option'));
        
    }

    public function getRequestShirtRightSleeve($id)
    {
        $order = Preorders::where("id",$id)->first();
        if($order){
            $right_sleeve = unserialize($order->right_sleeve);
            
            foreach($right_sleeve as $rs){
                if($rs == "flag"){
                    $right_sleeve['option'] = "Assaulting Flag";
                }
            }

            $sleeve_options = [
                "flag" => "Assaulting Flag",
                "none" => "None",
                "right_custom" => "Right Custom",
            ];
            
            $right_sleeve = serialize($right_sleeve);

        } else {
            $right_sleeve = [];
        }
        $host = Config::get('app.sales_url');
        $role = $this->getUserRoleID();
            
        //return Response::json(View::make($view, compact('rows','search','host'))->render());
        return View('designer.custom_admin.sub_views.shirt_rsleeve', compact('right_sleeve','sleeve_options','host','role'));
        
    }

    public function getRequestShirtGrunge($id)
    {
        $order = Preorders::where("id",$id)->first();
        $grunge_levels = CustomsAdminService::buildGrungeArray();
        if($order){
            $creative_freedom = ucwords($order->creative_freedom);
        } else {
            $creative_freedom = [];
        } 

        $role = $this->getUserRoleID();
            
        //return Response::json(View::make($view, compact('rows','search','host'))->render());
        return View('designer.custom_admin.sub_views.shirt_grunge', compact('creative_freedom', 'grunge_levels','role'));
        
    }

    public function getRequestShirtFrontNotes($id)
    {
        $order = Preorders::where("id",$id)->first();
        if($order){
            $artwork_notes_front = $order->artwork_notes_front;
        } else {
            $artwork_notes_front = [];
        } 
        $role = $this->getUserRoleID();

        //return Response::json(View::make($view, compact('rows','search','host'))->render());
        return View('designer.custom_admin.sub_views.shirt_front_notes', compact('artwork_notes_front','role'));
        
    }

    public function getRequestShirtBackNotes($id)
    {
        $order = Preorders::where("id",$id)->first();
        if($order){
            $artwork_notes_back = $order->artwork_notes_back;
        } else {
            $artwork_notes_back = [];
        }
        $role = $this->getUserRoleID();
            
        //return Response::json(View::make($view, compact('rows','search','host'))->render());
        return View('designer.custom_admin.sub_views.shirt_back_notes', compact('artwork_notes_back','role'));
        
    }

    public function getRequestShirtLeftNotes($id)
    {
        $order = Preorders::where("id",$id)->first();
        if($order){
            $sleeve_notes_left = $order->sleeve_notes_left;
        } else {
            $sleeve_notes_left = [];
        }
        $role = $this->getUserRoleID();
            
        //return Response::json(View::make($view, compact('rows','search','host'))->render());
        return View('designer.custom_admin.sub_views.shirt_lsleeve_notes', compact('sleeve_notes_left','role'));
        
    }

    public function getRequestShirtRightNotes($id)
    {
        $order = Preorders::where("id",$id)->first();
        if($order){
            $sleeve_notes_right = $order->sleeve_notes_right;
        } else {
            $sleeve_notes_right = [];
        }
        $role = $this->getUserRoleID(); 
            
        //return Response::json(View::make($view, compact('rows','search','host'))->render());
        return View('designer.custom_admin.sub_views.shirt_rsleeve_notes', compact('sleeve_notes_right','role'));
        
    }

    public function getRequestContact($id)
    {
        $project = Projects::where("id",$id)->first();
        $order = Preorders::where("id",$project->request_id)->first();
        $contacts = CustomsAdminService::buildContactsArray();
        $role = $this->getUserRoleID();
        

        //return Response::json(View::make($view, compact('rows','search','host'))->render());
        return View('designer.custom_admin.sub_views.contact', compact('contacts','project','order','role'));
    }

    public function getRequestGarments($id)
    {
        //$project = Projects::where("id",$id)->first();
        $order = Preorders::where("id",$id)->first();
        $garms = unserialize($order->garments);
        if($garms == null){
            $garms = $this->addGenericGarments();
            $order->garments = serialize($garms);
            $order->save();

        }
        
        $garments = CustomsAdminService::buildGarmentsArray();
        $role = $this->getUserRoleID();

        //return Response::json(View::make($view, compact('rows','search','host'))->render());
        return View('designer.custom_admin.sub_views.garments', compact('garments','garms','order','role'));
    }

    public function addGenericGarments()
    {
        $generic = [];
        $generic["garments"][] = [
            "name" => "Mens T-Shirt",
            "quote" => 1
        ];
        
        return $generic;
    }

    public function getRequestQuote($id)
    {
        $project = Projects::where("id",$id)->first();
        $quote_lines = QuoteBuilder::where('project_id', $id)->paginate(10);
        foreach($quote_lines as $key=>$line){
            $product = Products::where('id',$line->product_id)->first();
            $garment = Garments::where('id',$line->garment_id)->first();
            $color = Colors::where('id',$line->color_id)->first();
            $sleeves = Sleeves::where('id', $line->sleeve_id)->first();
            $front_ink1 = InkColors::where('id', $line->ink_1_front_id)->first();
            $front_ink2 = InkColors::where('id', $line->ink_2_front_id)->first();
            $front_ink3 = InkColors::where('id', $line->ink_3_front_id)->first();
            $front_ink4 = InkColors::where('id', $line->ink_4_front_id)->first();
            $back_ink1 = InkColors::where('id', $line->ink_1_back_id)->first();
            $back_ink2 = InkColors::where('id', $line->ink_2_back_id)->first();
            $back_ink3 = InkColors::where('id', $line->ink_3_back_id)->first();
            $back_ink4 = InkColors::where('id', $line->ink_4_back_id)->first();


            $line->product_name = (isset($product->product)?$product->product . " " . $product->spots . " spots " . $product->min_pieces . " " . $product->max_pieces:"");
            $line->spots = (isset($product->product)?$product->spots:0);
            $line->garment_name = $garment->garment;
            $line->color_name = $color->name;
            $line->sleeve_name = $sleeves->options;
            $line->front_ink1 = (isset($front_ink1->name)?$front_ink1->name:"");
            $line->front_ink2 = (isset($front_ink2->name)?$front_ink2->name:"");
            $line->front_ink3 = (isset($front_ink3->name)?$front_ink3->name:"");
            $line->front_ink4 = (isset($front_ink4->name)?$front_ink4->name:"");
            $line->back_ink1 = (isset($back_ink1->name)?$back_ink1->name:"");
            $line->back_ink2 = (isset($back_ink2->name)?$back_ink2->name:"");
            $line->back_ink3 = (isset($back_ink3->name)?$back_ink3->name:"");
            $line->back_ink4 = (isset($back_ink4->name)?$back_ink4->name:"");
        }
        
        $role = $this->getUserRoleID();
        //return Response::json(View::make($view, compact('rows','search','host'))->render());
        return View('designer.custom_admin.sub_views.quote_build', compact('project','quote_lines','role'));
    }

    public function editLineInQuoteDatabase($id, $row_id)
    {
        $quote_line = QuoteBuilder::where('project_id', $id)->where('id',$row_id)->first();
        $product = Products::where('id',$quote_line->product_id)->first();
        $garment = Garments::where('id',$quote_line->garment_id)->first();
        $color = Colors::where('id',$quote_line->color_id)->first();
        $sleeves = Sleeves::where('id', $quote_line->sleeve_id)->first();
        $front_ink1 = InkColors::where('id', $quote_line->ink_1_front_id)->first();
        $front_ink2 = InkColors::where('id', $quote_line->ink_2_front_id)->first();
        $front_ink3 = InkColors::where('id', $quote_line->ink_3_front_id)->first();
        $front_ink4 = InkColors::where('id', $quote_line->ink_4_front_id)->first();
        $back_ink1 = InkColors::where('id', $quote_line->ink_1_back_id)->first();
        $back_ink2 = InkColors::where('id', $quote_line->ink_2_back_id)->first();
        $back_ink3 = InkColors::where('id', $quote_line->ink_3_back_id)->first();
        $back_ink4 = InkColors::where('id', $quote_line->ink_4_back_id)->first();

        $project = Projects::where("id",$id)->first();
        $id = $project->request_id;
        $order = Preorders::where('id',$project->request_id)->first();
        $front = unserialize($order->front);
        $back = unserialize($order->back);

        $order_front_ink1 = (isset($front['colors']['ink1'])?$front['colors']['ink1']:"");
        $order_front_ink2 = (isset($front['colors']['ink2'])?$front['colors']['ink2']:"");
        $order_front_ink3 = (isset($front['colors']['ink3'])?$front['colors']['ink3']:"");
        $order_front_ink4 = (isset($front['colors']['ink4'])?$front['colors']['ink4']:"");
        $order_back_ink1 = (isset($back['colors']['ink1'])?$back['colors']['ink1']:"");
        $order_back_ink2 = (isset($back['colors']['ink2'])?$back['colors']['ink2']:"");
        $order_back_ink3 = (isset($back['colors']['ink3'])?$back['colors']['ink3']:"");
        $order_back_ink4 = (isset($back['colors']['ink4'])?$back['colors']['ink4']:"");


        $quote_line->product_name = (isset($product->product)?$product->product . " " . $product->spots . " spots " . $product->min_pieces . " " . $product->max_pieces:"");
        $quote_line->garment_name = $garment->garment;
        $quote_line->color_name = $color->name;
        $quote_line->sleeve_name = $sleeves->options;
        $quote_line->front_ink1 = (isset($front_ink1->name)?$front_ink1->name:$order_front_ink1);
        $quote_line->front_ink2 = (isset($front_ink2->name)?$front_ink2->name:$order_front_ink2);
        $quote_line->front_ink3 = (isset($front_ink3->name)?$front_ink3->name:$order_front_ink3);
        $quote_line->front_ink4 = (isset($front_ink4->name)?$front_ink4->name:$order_front_ink4);
        $quote_line->back_ink1 = (isset($back_ink1->name)?$back_ink1->name:$order_back_ink1);
        $quote_line->back_ink2 = (isset($back_ink2->name)?$back_ink2->name:$order_back_ink2);
        $quote_line->back_ink3 = (isset($back_ink3->name)?$back_ink3->name:$order_back_ink3);
        $quote_line->back_ink4 = (isset($back_ink4->name)?$back_ink4->name:$order_back_ink4);

        $order = Preorders::where("id",$id)->first();
        $products = CustomsAdminService::buildProductsArray();
        $garments = CustomsAdminService::buildProjectGarmentsArray($order->garments);
        $colors = CustomsAdminService::buildProjectColorsArray($order->shirt_types);
        $sleeves = CustomsAdminService::buildProjectSleevesArray($id);
        $inks = CustomsAdminService::buildProjectInksArray($order->front,$order->back);

        $sizes = [
            "0" => "Pick a Size",
            "xsmall" => "XSmall",
            "small" => "Small",
            "medium" => "Medium",
            "large" => "Large",
            "xlarge" => "XLarge",
            "xxlarge" => "2XLarge",
            "xxxlarge" => "3XLarge",
            "xxxxlarge" => "4XLarge",
        ];

        $role = $this->getUserRoleID();

        //return Response::json(View::make($view, compact('rows','search','host'))->render());
        return View('designer.custom_admin.sub_views.quote_build_edit', compact('order','products','garments','colors','sleeves','inks','sizes','project','quote_line','role'));
    }

    public function orderSizeEdit($id)
    {
        $sizes = Order_sizes::where('id', $id)->first();
        $order = Preorders::where("id",$sizes->order_id)->first();
        $colors = CustomsAdminService::buildProjectColorsArray($order->shirt_types);
        $garments = CustomsAdminService::buildProjectGarmentsArray($order->garments);

        return View('designer.custom_admin.sub_views.genurledit', compact('sizes','colors','garments'));
    }

    public function orderSizeEditSave($row_id)
    {
        $post = json_decode($_POST['data_array']);
        $data = [];
        foreach($post as $key => $value){
            if($value->value === "-1"){
                $data[$value->input] = $value->value;
                $data[$value->input."_override"] = 1;
            } else {
                $data[$value->input] = $value->value;
            }
        }

        //$project = Projects::findOrFail($id);
        $line_items = CustomsAdminService::editOrderSizes($data, $row_id);

        if($line_items['status'] === true){
            return response()->json([
                'success' => true,
                'result' => "Item Updated",
            ], 200);
        } else if($line_items['status'] === false){
            return response()->json([
                'success' => false,
                'result' => $line_items['reason'],
            ], 200);
        }
    }

    public function addLineToQuote($id)
    {
        $project = Projects::where("id",$id)->first();
        $id = $project->request_id;
        $order = Preorders::where("id",$id)->first();
        $products = CustomsAdminService::buildProductsArray();
        $garments = CustomsAdminService::buildProjectGarmentsArray($order->garments);
        $colors = CustomsAdminService::buildProjectColorsArray($order->shirt_types);
        $sleeves = CustomsAdminService::buildProjectSleevesArray($id);
        $inks = CustomsAdminService::buildProjectInksArray($order->front,$order->back);
        $locations = [
            "0" => "Pick a Location",
            "front" => "Front",
            "back" => "Back",
        ];
        $sizes = [
            "0" => "Pick a Size",
            "xsmall" => "XSmall",
            "small" => "Small",
            "medium" => "Medium",
            "large" => "Large",
            "xlarge" => "XLarge",
            "xxlarge" => "2XLarge",
            "xxxlarge" => "3XLarge",
            "xxxxlarge" => "4XLarge",
        ];

        //return Response::json(View::make($view, compact('rows','search','host'))->render());
        return View('designer.custom_admin.sub_views.quote_build_add', compact('order','products','garments','colors','sleeves','inks','locations','sizes','project'));
    }

    public function addLineToQuoteDatabase($id)
    {
        $post = json_decode($_POST['data_array']);
        $data = [];
        foreach($post as $key => $value){

            $data[$value->input] = $value->value;
        }
 
        $project = Projects::findOrFail($id);
        $line_items = CustomsAdminService::addLineItem($data, $project);

        if($line_items['status'] === true){
            return response()->json([
                'success' => true,
                'result' => "Line Item Added",
            ], 200);
        } else if($line_items['status'] === false){
            return response()->json([
                'success' => false,
                'result' => $line_items['reason'],
            ], 200);
        }
    }

    public function editLineInQuoteDatabaseSave($id, $row_id)
    {
        $post = json_decode($_POST['data_array']);
        $data = [];
        foreach($post as $key => $value){

            $data[$value->input] = $value->value;
        }
 
        $project = Projects::findOrFail($id);
        $line_items = CustomsAdminService::editLineItem($data, $project, $row_id);

        if($line_items['status'] === true){
            return response()->json([
                'success' => true,
                'result' => "Line Item Updated",
            ], 200);
        } else if($line_items['status'] === false){
            return response()->json([
                'success' => false,
                'result' => $line_items['reason'],
            ], 200);
        }
    }

    public function copyLineInQuoteDatabase($id, $row_id)
    {
        $project = Projects::findOrFail($id);
        $line_items = CustomsAdminService::copyLineItem($row_id);

        if($line_items['status'] === true){
            return response()->json([
                'success' => true,
                'result' => "Line Item Copied",
            ], 200);
        } else if($line_items['status'] === false){
            return response()->json([
                'success' => false,
                'result' => $line_items['reason'],
            ], 200);
        }
    }

    public function deleteLineItem($id, $row_id)
    {
        $quote = QuoteBuilder::where('id',$row_id)->first();
        if($quote->exists()){
            $quote->delete();

            if($quote->exists === false){
                $line_items = [
                    "status" => true,
                ];  
            } else {
                $line_items = [
                    "status" => false,
                    "reason" => "Could Not Delete Line Item Exists"
                ];
            }
        } else {
            $line_items = [
                "status" => false,
                "reason" => "No Line Item Exists"
            ];
        }

        if($line_items['status'] === true){
            return response()->json([
                'success' => true,
                'result' => "Line Item Deleted",
            ], 200);
        } else if($line_items['status'] === false){
            return response()->json([
                'success' => false,
                'result' => $line_items['reason'],
            ], 200);
        }
    }

    public function deleteOrderSize($id)
    {
        $quote = Order_sizes::where('id',$id)->first();
        if($quote->exists()){
            $quote->delete();

            if($quote->exists === false){
                $line_items = [
                    "status" => true,
                ];  
            } else {
                $line_items = [
                    "status" => false,
                    "reason" => "Could Not Delete"
                ];
            }
        } else {
            $line_items = [
                "status" => false,
                "reason" => "No Line Item Exists"
            ];
        }

        if($line_items['status'] === true){
            return response()->json([
                'success' => true,
                'result' => "Line Item Deleted",
            ], 200);
        } else if($line_items['status'] === false){
            return response()->json([
                'success' => false,
                'result' => $line_items['reason'],
            ], 200);
        }
    }

    public function getProjectInfo($id)
    {
        $project = Projects::where('id',$id)->first();
        $activity = ProjectStatus::where('project_id', $id)->orderBy('created_at','DESC')->first();
        $designers = CustomsAdminService::buildDesignersArray();
        $salespersons = CustomsAdminService::buildSalesPersonArray();
        $project_status = CustomsAdminService::buildProjectStatusTypeArray();
        $production_status = CustomsAdminService::buildProductionStatusTypeArray();
        $pstatus = ProjectStatus::where('project_id', $id)->orderBy('created_at','DESC')->first();
        $project->status_id = $pstatus->status_id;
        
        $status = Statuses::where('id','=',$activity->status_id)->first();
        $employee = Employees::where('id','=',$activity->user_id)->first();
        $activity->status = $status->status;
        $activity->employee = $employee->name;

        if(!isset($project->tax_rate)){
            $order = Preorders::where('id', $project->request_id)->first();
            CustomsAdminService::getSampleTaxRate($order);
            $project = Projects::where('id',$id)->first();
        }

        $role = $this->getUserRoleID();
 
        //return Response::json(View::make($view, compact('rows','search','host'))->render());
        return View('designer.custom_admin.sub_views.project_details', compact('project','activity','designers','salespersons','project_status','production_status','role'));
    }

    public function getUserRoleID()
    {
        $user = \Auth::user();
        $role = $user->role_id;

        return $role;
    }

    public function getProjectRequest($id)
    {
        $project = Projects::findOrFail($id);
        $order_request = Preorders::findOrFail($project->request_id);

        return View('designer.custom_admin.sub_views.request', compact('order_request'));
    }

    public function getProjectPrice($id)
    {
        $project = Projects::findOrFail($id);
        $quote = Quotes::where('order_id',$project->request_id)->first();

        return View('designer.custom_admin.sub_views.quote_price', compact('quote'));
    }

    public function getProjectLineItems($id)
    {
        $project = Projects::findOrFail($id);
        $lineitems = CustomsAdminService::checkForExistingLineItem($project->request_id);
        $lineitems = CustomsAdminService::convertIdsToText($lineitems);

        return View('designer.custom_admin.sub_views.quote_lineitems', compact('lineitems'));
    }

    public function getProjectFileView($id)
    {
        $project = Projects::findOrFail($id);
        $doctypes = CustomsAdminService::buildDocTypeArray();
        $files = ProjectFiles::where('project_id', $id)->orderBy('id','desc')->get();
        $revisions = ProjectRevisions::where('project_id', $id)->get();
        $revision_files = [];
        foreach($revisions as $rv){
            if(isset($rv->revision_file_id)){
                $revision_files[] = $rv->revision_file_id;
            }
        }

        foreach($files as $file){

            if($file->final_art === 0){
                $file->final = "No";
            } else {
                $file->final = "Yes";
            }
        }
        

        return View('designer.custom_admin.sub_views.project_files', compact('doctypes','files'));
    }

    public function getProjectRevisions($id)
    {
        $project = Projects::findOrFail($id);
        $doctypes = CustomsAdminService::buildDocTypeArray();
        $revtypes = CustomsAdminService::buildRevTypeArray();
        $revisions = ProjectRevisions::where('project_id', $id)->get();
        
        foreach ($revisions as $rv){
            $rv->revision_type = $rv->revtypes->type;
            $rv->employee = $rv->employees->name;
            
        }

        return View('designer.custom_admin.sub_views.project_revisions', compact('doctypes','revtypes','revisions'));
    }

    public function updateProjectShirtTypes($id)
    {
        $order = Preorders::where("id",$id)->first();
        $current_shirt_types = unserialize($order->shirt_types);
        $csf = $current_shirt_types;
        $shirt_types_type = $_POST['shirt_types_type'];
        $use_shirt_type = $_POST['use_shirt_type'];

        foreach($shirt_types_type as $key=>$value){
            if(isset($csf['types'][$key])){
                $is_in_array = in_array($value, $use_shirt_type);
                if($is_in_array === true){                    
                    $csf['types'][$key] = [
                        "type" => $value,
                        "quote" => 1
                    ];                    
                } else {
                    $csf['types'][$key] = [
                        "type" => $value,
                        "quote" => 0
                    ]; 
                }
            } else {
                $csf['types'][] = [
                    "type" => $value,
                    "quote" => 1
                ];
            }
        }
        
        $csf = serialize($csf);
        $order->shirt_types = $csf;
        $order->save();

        CustomsAdminService::buildOrderSizesNow($id);

        if($order){
            return response()->json([
                'success' => true,
                'result' => "Shirt Type Updated",
            ], 200);
        } else if(!$order){
            return response()->json([
                'success' => false,
                'result' => "Shirt Type Not Updated",
            ], 200);
        }

    }

    public function saveProjectFrontInkColors($project, $post, $inputName, $id)
    {
        $order = Preorders::where("id",$id)->first();
        $current_shirt_types = unserialize($order->front);
        $cst = $current_shirt_types;

        for($i = 1; $i <= 4; $i++){
            if(isset($post['front_ink_color_selection_'.$i])){
                if($cst['colors']['ink'.$i] !== $post['front_ink_color_selection_'.$i]){
                    
                        $cst['colors']['ink'.$i] = $post['front_ink_color_selection_'.$i];
                    
                }
            }
        }
       
        $cst = serialize($cst);
        $order->front = $cst;
        $order->save();

        $friendly_name = "Front Ink Color";

        if($order){
            return [
                "result" => true,
                "friendly_name" => $friendly_name,
            ];
        } else if(!$order){
            return [
                "result" => false,
                "friendly_name" => $friendly_name,
            ];
        }

    }

    public function saveProjectBackInkColors($project, $post, $inputName, $id)
    {
        $order = Preorders::where("id",$id)->first();
        $current_shirt_types = unserialize($order->back);
        $cst = $current_shirt_types;

        for($i = 1; $i <= 4; $i++){
            if($cst['colors']['ink'.$i] !== $post['back_ink_color_selection_'.$i]){
                $cst['colors']['ink'.$i] = $post['back_ink_color_selection_'.$i];
            }
        }
       
        $cst = serialize($cst);
        $order->back = $cst;
        $order->save();

        $friendly_name = "Back Ink Color";

        if($order){
            return [
                "result" => true,
                "friendly_name" => $friendly_name,
            ];
        } else if(!$order){
            return [
                "result" => false,
                "friendly_name" => $friendly_name,
            ];
        }

    }

    public function saveProjectShirtPlacement($project, $post, $inputName, $id)
    {
        $order = Preorders::where("id",$id)->first();
        $current_shirt_types = unserialize($order->front);
        $cst = $current_shirt_types;

        if($inputName == "front_placement"){
            if($cst['placement'] !== $post['front_placement']){
                $cst['placement'] = $post['front_placement'];
            }

            $cst = serialize($cst);
            $order->front = $cst;
            $order->save();
        } else {
            if($cst['placement'] !== $post['back_placement']){
                $cst['placement'] = $post['back_placement'];
            }

            $cst = serialize($cst);
            $order->back = $cst;
            $order->save();
        }


        $friendly_name = "Placement";

        if($order){
            return [
                "result" => true,
                "friendly_name" => $friendly_name,
            ];
        } else if(!$order){
            return [
                "result" => false,
                "friendly_name" => $friendly_name,
            ];
        }

    }

    public function saveProjectShirtSleeves($project, $post, $inputName, $id)
    {
        $order = Preorders::where("id",$id)->first();

        if($inputName == "leftsleeveopts"){
            $current_shirt_sleeves = unserialize($order->left_sleeve);
            $css = $current_shirt_sleeves;
            if($css['option'] !== $post['leftsleeveopts']){
                $css['option'] = $post['leftsleeveopts'];
            }
            $friendly_name = "Left Sleeve";
            $css = serialize($css);
            $order->left_sleeve = $css;
            $order->save();

        } else {
            $current_shirt_sleeves = unserialize($order->right_sleeve);
            $css = $current_shirt_sleeves;
            if($css['option'] !== $post['rightsleeveopts']){
                $css['option'] = $post['rightsleeveopts'];
            }
            $friendly_name = "Right Sleeve";
            $css = serialize($css);
            $order->right_sleeve = $css;
            $order->save();

        }

        if($order){
            return [
                "result" => true,
                "friendly_name" => $friendly_name,
            ];
        } else if(!$order){
            return [
                "result" => false,
                "friendly_name" => $friendly_name,
            ];
        }

    }

    public function updateProjectShirtColors($id)
    {
        $order = Preorders::where("id",$id)->first();
        $current_shirt_front = unserialize($order->shirt_types);
        $csf = $current_shirt_front;

        $shirt_types_color = $_POST['shirt_types_color'];
        $use_shirt_color = $_POST['use_shirt_color'];

        foreach($shirt_types_color as $key=>$value){
            if(isset($csf['colors'][$key+1])){
                $is_in_array = isset($use_shirt_color[$key]);
                
                if($is_in_array === true){ 
                    if($use_shirt_color[$key] == "on"){                   
                        $csf['colors'][$key+1] = [
                            "color" => $value,
                            "quote" => 1
                        ];
                    } else {
                        $csf['colors'][$key+1] = [
                            "color" => $value,
                            "quote" => 0
                        ];
                    }                 

                } else {
                    $csf['colors'][$key+1] = [
                        "color" => $value,
                        "quote" => 1
                    ]; 
                }
            } else {
                $csf['colors'][$key+1] = [
                    "color" => $value,
                    "quote" => 1
                ];
            }
        }

        $csf = serialize($csf);
        $order->shirt_types = $csf;
        $order->save();

        CustomsAdminService::buildOrderSizesNow($id);

        if($order){
            return response()->json([
                'success' => true,
                'result' => "Shirt Color Updated",
            ], 200);
        } else if(!$order){
            return response()->json([
                'success' => false,
                'result' => "Shirt Color Not Updated",
            ], 200);
        }

    }

    public function updateProjectGarments($id, $record_id)
    {
        $order = Preorders::where("id",$id)->first();
        $current_shirt_garments = unserialize($order->garments);
        $csf = $current_shirt_garments;

        if(isset($_POST['use_garment'])){            
            if($csf['garments'][$record_id]['quote'] == 0){                
                $csf['garments'][$record_id] = [
                    "name" => $csf['garments'][$record_id]['name'],
                    "quote" => 1
                ];
            } else {
                
                $csf['garments'][$record_id] = [
                    "name" => $csf['garments'][$record_id]['name'],
                    "quote" => 0
                ];
            }

            $csf = serialize($csf);
            $order->garments = $csf;
            $order->save();

            CustomsAdminService::buildOrderSizesNow($id);

            if($order){
                return response()->json([
                    'success' => true,
                    'result' => "Garment Updated",
                ], 200);
            } else if(!$order){
                return response()->json([
                    'success' => false,
                    'result' => "Garment Not Updated",
                ], 200);
            }
        }
        
        if(isset($_POST['garments'])){ 
            $csf['garments'][$record_id] = [
                "name" => $_POST['garments'][$record_id],
                "quote" => 1
            ]; 

            $csf = serialize($csf);
            $order->garments = $csf;
            $order->save();

            CustomsAdminService::buildOrderSizesNow($id);

            if($order){
                return response()->json([
                    'success' => true,
                    'result' => "Garment Updated",
                ], 200);
            } else if(!$order){
                return response()->json([
                    'success' => false,
                    'result' => "Garment Not Updated",
                ], 200);
            }

        }
    }

    public function addProjectGarments($id){
        $order = Preorders::where("id",$id)->first();
        $current_shirt_garments = unserialize($order->garments);
        $csf = $current_shirt_garments;

        $garments = $_POST['add_garment'];
        foreach($garments as $key=>$value){       
            $garmentExistsInProject = $this->garmentExistsInProject($value, $current_shirt_garments);     
            if($garmentExistsInProject === false){
                $csf['garments'][] = [
                    "name" => $value,
                    "quote" => 1
                ];
            }
        }

        $csf = serialize($csf);
        $order->garments = $csf;
        $order->save();

        CustomsAdminService::buildOrderSizesNow($id);

        if($order){
            return response()->json([
                'success' => true,
                'result' => "Garment Updated",
            ], 200);
        } else if(!$order){
            return response()->json([
                'success' => false,
                'result' => "Garment Not Updated",
            ], 200);
        }
    }

    private function garmentExistsInProject($needle, $haystack)
    {
        foreach($haystack['garments'] as $key => $value){
            if($needle === $value['name']){
                return true;
            }
        }

        return false;
    }

    public function updateProjectTextBox($id, $inputName)
    {
        
        foreach($_POST as $key => $value){
            $$key = $value;
        }

        switch($inputName){
            case "project_name":
                $input = "name";
                $textbox = $project_name;
                $friendly_name = "Project Name";
                $project = Projects::where("id",$id)->first();
                break;
            case "description":
                $input = "description";
                $textbox = $description;
                $friendly_name = "Description";
                $project = Projects::where("id",$id)->first();
                break;
            case "artwork_notes_front":
                $input = "artwork_notes_front";
                $textbox = $artwork_notes_front;
                $friendly_name = "Artwork Notes (Front)";
                $project = Preorders::where("id",$id)->first();
                break;
            case "artwork_notes_back":
                $input = "artwork_notes_back";
                $textbox = $artwork_notes_back;
                $friendly_name = "Artwork Notes (Back)";
                $project = Preorders::where("id",$id)->first();
                break;
            case "sleeve_notes_left":
                $input = "sleeve_notes_left";
                $textbox = $sleeve_notes_left;
                $friendly_name = "Sleeve Notes (Left)";
                $project = Preorders::where("id",$id)->first();
                break;
            case "sleeve_notes_right":
                $input = "sleeve_notes_right";
                $textbox = $sleeve_notes_right;
                $friendly_name = "Sleeve Notes (Right)";
                $project = Preorders::where("id",$id)->first();
                break;
        }

        $current_textbox = $project->$input;

        $project->$input = $textbox;
        $project->save();

        if($project->$input === $textbox){
            return response()->json([
                'success' => true,
                'result' => $friendly_name . " Updated",
            ], 200);
        } else if($project->$input === $current_textbox){
            return response()->json([
                'success' => false,
                'result' => $friendly_name . " Not Updated",
            ], 200);
        }   
    }

    public function updateProjectSelectBox($id, $inputName)
    {
        $project = Projects::where("id",$id)->first();

        switch($inputName){
            case "project_status":
                $project_save = $this->saveProjectStatusAjax($project, $_POST, $inputName, $id);
                break;
            case "salesperson":
            case "graphic_designer":
            case "production_status":
            case "company_id":
            case "creative_freedom":
                $project_save = $this->saveProjectDropDownGeneric($project, $_POST, $inputName, $id);
                break;
            case "front_placement":
            case "back_placement":
                $project_save = $this->saveProjectShirtPlacement($project, $_POST, $inputName, $id);
                break;
            case "front_ink_color_selection_1":
            case "front_ink_color_selection_2":
            case "front_ink_color_selection_3":
            case "front_ink_color_selection_4":
                $project_save = $this->saveProjectFrontInkColors($project, $_POST, $inputName, $id);
                break;
            case "back_ink_color_selection_1":
            case "back_ink_color_selection_2":
            case "back_ink_color_selection_3":
            case "back_ink_color_selection_4":
                $project_save = $this->saveProjectBackInkColors($project, $_POST, $inputName, $id);
                break;
            case "leftsleeveopts":
            case "rightsleeveopts":
                $project_save = $this->saveProjectShirtSleeves($project, $_POST, $inputName, $id);
                        break;
        }

        if($project_save['result'] === true){
            return response()->json([
                'success' => true,
                'result' => $project_save['friendly_name'] . " Updated",
            ], 200);
        } else if($project_save['result'] === false){
            return response()->json([
                'success' => false,
                'result' => $project_save['friendly_name'] . " Not Updated",
            ], 200);
        }   
    }

    private function saveProjectDropDownGeneric($project, $post, $inputName, $id)
    {

        foreach($post as $key => $value){
            $$key = $value;
        }

        switch($inputName){
            case "salesperson":
                $input = "salesperson_id";
                $selectbox = $salesperson;
                $friendly_name = "Salesperson";
                $current_selectbox = $project->$input;
                $project->$input = $selectbox;
                $project->save();
                break;
            case "graphic_designer":
                $input = "designer_id";
                $selectbox = $graphic_designer;
                $friendly_name = "Graphic Designer";
                $current_selectbox = $project->$input;
                $project->$input = $selectbox;
                $project->save();

                $employee = Employees::where('id',$graphic_designer)->first();
                $emails = [
                    $employee->email
                ];
                $subject = "Customs Project - You've Been Assigned A Project";
                $this->sendEmailToDesign($project->name, $emails, $subject, 'email.assigned');
                break;
            case "production_status":
                $input = "production_status_id";
                $selectbox = $production_status;
                $friendly_name = "Production Status";
                $current_selectbox = $project->$input;
                $project->$input = $selectbox;
                $project->save();
                break;
            case "company_id":
                $input = "company_id";
                $selectbox = $company_id;
                $friendly_name = "Contact";
                $current_selectbox = $project->$input;
                $project->$input = $selectbox;
                $project->save();
                break;
            case "placement":
                $input = "front_placement";
                $selectbox = $front_placement;
                $friendly_name = "Front Placement";
                $current_selectbox = $project->$input;
                $project->$input = $selectbox;
                $project->save();
                break;
            case "creative_freedom":
                $input = "creative_freedom";
                $selectbox = $creative_freedom;
                $friendly_name = "Grunge Level";
                $project = Preorders::where('id',$id)->first();
                $current_selectbox = $project->$input;
                $project->$input = $selectbox;
                $project->save();
                break;
        }

        if($project->$input === $selectbox){
            return [
                "result" => true,
                "friendly_name" => $friendly_name,
            ];
        } else if($project->$input === $current_selectbox){
            return [
                "result" => false,
                "friendly_name" => $friendly_name,
            ];
        }
    }

    private function saveProjectStatusAjax($project, $post, $inputName, $id)
    {
        foreach($post as $key => $value){
            $$key = $value;
        }

        $status = ProjectStatus::where('project_id', $project->id)->orderBy('created_at','DESC')->first();
        $input = "status_id";
        $current_selectbox = $status->$input;
        $selectbox = $project_status;
        $friendly_name = "Project Status";

        $status->$input = $selectbox;
        $status->save();

        $statuses = Statuses::where('id',$selectbox)->first();

        if($statuses->status === "Graphics Review"){
            $emails = [
                ''
            ];
            $subject = "Customs Project - Ready For Graphics Review";
            $this->sendEmailToDesign($project->name, $emails, $subject,'email.reviewready');
        }  else if($statuses->status === "Jpeg Complete"){
            $salesperson = Employees::where('id',$project->salesperson_id)->first();
            $designer = Employees::where('id',$project->designer_id)->first();

            $emails = [
                $salesperson->email,
            ];
            if(isset($designer->email)){
                $emails[] = $designer->email;
                
            }
            
            $subject = "Customs Project - JPEG Complete";
            $this->sendEmailToDesign($project->name, $emails, $subject,'email.jpegcomplete');
        } else if($statuses->status === "Tdemo Complete"){
            $salesperson = Employees::where('id',$project->salesperson_id)->first();
            $designer = Employees::where('id',$project->designer_id)->first();
            $emails = [
                $salesperson->email,
            ];
            if(isset($designer->email)){
                $emails[] = $designer->email;
                
            }
            
            $subject = "Customs Project - TDEMO Complete";
            $this->sendEmailToDesign($project->name, $emails, $subject,'email.tdemocomplete');
        } else if($statuses->status === "Invoice Paid"){
            
            
                $emails[] = '';
                
            
            
            $subject = "Customs Project - Invoice Paid";
            $this->sendEmailToDesign($project->name, $emails, $subject,'email.invoicepaid');
        }

        if($status->status_id === $selectbox){
            return [
                "result" => true,
                "friendly_name" => $friendly_name,
            ];
        } else if($status->status_id === $current_selectbox){
            return [
                "result" => false,
                "friendly_name" => $friendly_name,
            ];
        }
    }

    private function sendEmailToDesign($name, $emails, $subject, $template)
    {
        //this is the proper syntax sending an email in laravel
        //************ EMAIL ******************* FORM DATA ***************** EMAIL SUBJ *** EMAIL ARRAY */
        Mail::send($template, ['name' => $name], function ($m) use ($subject, $emails) {
            $m->from('', ''' Customs App'); // FROM EMAIL (actual email address, name of from email)
            $m->to($emails)->subject($subject); // TO EMAIL and SUBJECT
        });
    }

    private function saveGeneratedUrl($id, $randomstring)
    {
        $host = Config::get('app.sales_url');
        $url = $host ."/customer/sizing/".$randomstring;

        $project = Preorders::where("id",$id)->first();

        $project->generated_url = $url;
        $project->save();

        if($project){
            return response()->json([
                'success' => true,
                'result' =>  "Sizing URL Generated",
            ], 200);
        } else {
            return response()->json([
                'success' => false,
                'result' =>  "Sizing URL Not Generated",
            ], 200);
        }
        

    }

    public function getRequestGenUrl($id)
    {
        $order_request = Preorders::where("id",$id)->first();
        if($order_request){
            $order_sizes = Order_sizes::where('order_id', $id)->get()->toArray();

        } else {
            $order_request = [];
        } 
        $role = $this->getUserRoleID();
            
        //return Response::json(View::make($view, compact('rows','search','host'))->render());
        return View('designer.custom_admin.sub_views.genurl', compact('order_request','order_sizes','role'));
    }

    public function getGarmentTemplate()
    {
        $garments = CustomsAdminService::buildGarmentsArray();

        //return Response::json(View::make($view, compact('rows','search','host'))->render());
        return View('designer.custom_admin.templates.garment', compact('garments'));
    }

    public function getColorsTemplate()
    {
        $colors = CustomsAdminService::buildShirtColorsArray();
        //return Response::json(View::make($view, compact('rows','search','host'))->render());
        return View('designer.custom_admin.templates.colors', compact('colors'));
    }

    public function getTypesTemplate()
    {
        $types = Shirt_types::all();

        $msc = [];
        foreach($types as $type){
            $msc[$type->type] = $type->type;
        }

        $types = $msc;
        //return Response::json(View::make($view, compact('rows','search','host'))->render());
        return View('designer.custom_admin.templates.type', compact('types'));
    }

    public function removeGarmentFromProject($id, $garment_id)
    {
        $order_request = Preorders::where("id",$id)->first();
        $garments = unserialize($order_request->garments);
        $current_garments = $garments;
        unset($garments['garments'][$garment_id]);
        $order_request->garments = serialize($garments);
        $order_request->save();
        CustomsAdminService::buildOrderSizesNow($id);

        if((sizeof($current_garments['garments']) % sizeof($garments['garments'])) === 1){
            return response()->json([
                'success' => true,
                'result' =>  "Garments Updated",
            ], 200);
        } else {
            return response()->json([
                'success' => false,
                'result' =>  "Garments Not Updated",
            ], 200);
        }

    }

    public function removeGarmentColorFromProject($id, $garment_id)
    {
        $order_request = Preorders::where("id",$id)->first();
        $garments = unserialize($order_request->shirt_types);
        $current_garments = $garments;
        unset($garments['colors'][$garment_id]);
        $order_request->shirt_types = serialize($garments);
        $order_request->save();
         
        CustomsAdminService::buildOrderSizesNow($id);

        if((sizeof($current_garments['colors']) % sizeof($garments['colors'])) === 1){
            return response()->json([
                'success' => true,
                'result' =>  "Garment Colors Updated",
            ], 200);
        } else {
            return response()->json([
                'success' => false,
                'result' =>  "Garment Colors Not Updated",
            ], 200);
        }

    }

    public function removeGarmentTypeFromProject($id, $garment_id)
    {
        $order_request = Preorders::where("id",$id)->first();
        $garments = unserialize($order_request->shirt_types);
        $current_garments = $garments;
        unset($garments['types'][$garment_id]);
        $order_request->shirt_types = serialize($garments);
        $order_request->save();

        if((sizeof($current_garments['types']) % sizeof($garments['types'])) === 1){
            return response()->json([
                'success' => true,
                'result' =>  "Garment Types Updated",
            ], 200);
        } else {
            return response()->json([
                'success' => false,
                'result' =>  "Garment Types Not Updated",
            ], 200);
        }

    }


    public function createDraftOrder($id)
    {
        $project = Projects::where('id',$id)->first();
        $order = Preorders::where('id', $project->request_id)->first();
        $quote = Quotes::where('project_id', $id)->first();
        $lines = CustomsAdminService::findProductInShopify($id);
        if(isset($lines['status']) && $lines['status'] === "failed"){
            return response()->json([
                'status' => false,
                'result' =>  $lines['reason'],
            ], 200);
        }
        $contact_id = CustomsAdminService::findCustomerInShopify($order);
        $draft_orders = CustomsAdminService::createDraftOrder($order, $lines, $contact_id, $quote, $project->draft_order_id, $project);
        
        if(isset($draft_orders['status']) && $draft_orders['status'] === "failed"){
            return response()->json([
                'status' => false,
                'result' =>  $draft_orders['reason'],
            ], 200);
        } else {
            $project->shopify_id = $draft_orders['shopify_id'];
            $project->draft_order_name = $draft_orders['order_name'];
            $project->invoice_url = $draft_orders['invoice_url'];
            $project->save();

            $message = "Shopify Order Created";

            return response()->json([
                'status' => true,
                'result' =>  $message,
            ], 200);
        }
        
    }

    public function getTaxRate($id)
    {
        $project = Projects::where('id',$id)->first();
        $order = Preorders::where('id', $project->request_id)->first();
        $tax_data = CustomsAdminService::getTaxRate($order);
        $project->tax_rate = $tax_data->totalRate * 100;
        $project->save();

        return $project;
    }

    public function addImagesToDatabaseRecordFront(IRequest $request,$id)
    {
        $data_array = json_decode($request->data_array);
        $project = Projects::where('id', $id)->first();
        $order = Preorders::where('id', $project->request_id)->first();
        $front = unserialize($order->front);
        $images = (isset($front['images'])?$front['images']:$front['images'] = []);
        $image_count = sizeof($images);
        
        foreach($data_array as $d){
            for($i=1; $i<=$image_count + sizeof($data_array); $i++){
                if(!isset($images["image".$i])){
                    $images["image".$i] = $d[1];
                }
            }
        }
        $front['images'] = $images;
        $order->front = serialize($front);
        $order->save();
    }

    public function addImagesToDatabaseRecordBack(IRequest $request,$id)
    {
        $data_array = json_decode($request->data_array);
        $project = Projects::where('id', $id)->first();
        $order = Preorders::where('id', $project->request_id)->first();
        $back = unserialize($order->back);
        $images = (isset($back['images'])?$back['images']:$back['images'] = []);
        $image_count = sizeof($images);
        
        foreach($data_array as $d){
            for($i=1; $i<=$image_count + sizeof($data_array); $i++){
                if(!isset($images["image".$i])){
                    $images["image".$i] = $d[1];
                }
            }
        }
        $back['images'] = $images;
        $order->back = serialize($back);
        $order->save();
    }

    public function addImagesToDatabaseRecordLeft(IRequest $request,$id)
    {
        $data_array = json_decode($request->data_array);
        $project = Projects::where('id', $id)->first();
        $order = Preorders::where('id', $project->request_id)->first();
        $left = unserialize($order->left_sleeve);
        $images = (isset($left['images'])?$left['images']:$left['images'] = []);
        $image_count = sizeof($images);
        
        foreach($data_array as $d){
            for($i=1; $i<=$image_count + sizeof($data_array); $i++){
                if(!isset($images["image".$i])){
                    $images["image".$i] = $d[1];
                }
            }
        }
        $left['images'] = $images;
        $order->left_sleeve = serialize($left);
        $order->save();
    }

    public function addImagesToDatabaseRecordRight(IRequest $request,$id)
    {
        $data_array = json_decode($request->data_array);
        $project = Projects::where('id', $id)->first();
        $order = Preorders::where('id', $project->request_id)->first();
        $right = unserialize($order->right_sleeve);
        $images = (isset($right['images'])?$right['images']:$right['images'] = []);
        $image_count = sizeof($images);
        
        foreach($data_array as $d){
            for($i=1; $i<=$image_count + sizeof($data_array); $i++){
                if(!isset($images["image".$i])){
                    $images["image".$i] = $d[1];
                }
            }
        }
        $right['images'] = $images;
        $order->right_sleeve = serialize($right);
        $order->save();
    }

    public function removeImagesToDatabaseRecordFront($id, $row_id)
    {
        $order_request = Preorders::where("id",$id)->first();
        $front = unserialize($order_request->front);
        $current_front = $front;
        unset($front['images'][$row_id]);
        $order_request->front = serialize($front);
        $order_request->save();

        return response()->json([
            'success' => true,
            'result' =>  "Image Removed Updated",
        ], 200);
    }

    public function removeImagesToDatabaseRecordBack($id, $row_id)
    {
        $order_request = Preorders::where("id",$id)->first();
        $back = unserialize($order_request->back);
        $current_back = $back;
        unset($back['images'][$row_id]);
        $order_request->back = serialize($back);
        $order_request->save();

        return response()->json([
            'success' => true,
            'result' =>  "Image Removed Updated",
        ], 200);
    }

    public function removeImagesToDatabaseRecordLeft($id, $row_id)
    {
        $order_request = Preorders::where("id",$id)->first();
        $left = unserialize($order_request->left_sleeve);
        $current_left = $left;
        unset($left['images'][$row_id]);
        $order_request->left_sleeve = serialize($left);
        $order_request->save();

        return response()->json([
            'success' => true,
            'result' =>  "Image Removed Updated",
        ], 200);
    }

    public function removeImagesToDatabaseRecordRight($id, $row_id)
    {
        $order_request = Preorders::where("id",$id)->first();
        $right = unserialize($order_request->right_sleeve);
        $current_right = $right;
        unset($right['images'][$row_id]);
        $order_request->right_sleeve = serialize($right);
        $order_request->save();
        return response()->json([
            'success' => true,
            'result' =>  "Image Removed Updated",
        ], 200);
        
    }

    public function editRevision($id, $row_id)
    {
        $revtypes = CustomsAdminService::buildRevTypeArray();
        $revisions = ProjectRevisions::where('id', $row_id)->first();

        $revisions->revision_type = $revisions->revtypes->id;
        $revisions->employee = $revisions->employees->name;
 
        return View('designer.custom_admin.sub_views.project_revisions_edit', compact('revtypes','revisions'));
    }

    public function updateRevision(IRequest $request,$id)
    {

        $post = json_decode($request->data_array);
        $data = [];
        foreach($post as $key => $value){

            $data[$value->input] = $value->value;
        }

        $revisions = ProjectRevisions::where('id', $id)->first();
        $revisions->revision_type_id = $data['revtype'];
        $revisions->front_rev_note = $data['front_rev_notes'];
        $revisions->back_rev_note = $data['back_rev_notes'];
        $revisions->left_rev_note = $data['left_rev_notes'];
        $revisions->right_rev_note = $data['right_rev_notes'];
        $revisions->general_rev_note = $data['general_rev_notes'];
        $revisions->user_id = \Auth::user()->id;
        $revisions->save();

        return response()->json([
            'success' => true,
            'result' =>  "Revision Updated",
        ], 200);
        
    }

    public function deleteRevision($id)
    {
        $revisions = ProjectRevisions::where('id', $id)->first();
        $revisions->delete();
        return response()->json([
            'success' => true,
            'status' => 'Revision Removed'
        ], 200);
    }

}
