<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Permission;
use App\Role;
use App\User;
use Input;
use Validator;
use Redirect;
use Hash;
use Mail;
use Carbon\Carbon;
use Session;
use App\Http\Models\Icons;
use App\Http\Models\Shirts;
use App\Http\Models\Clipart;
use App\Http\Models\Shirt_types;
use App\Http\Models\Colors;
use App\Http\Models\Share;
use App\Http\Models\InkColors;
use App\Http\Models\Fonts;
use App\Http\Models\SalesPersons;
use App\Http\Models\Preorders;
use App\Http\Models\Order_sizes;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use OdooClient\Client;
use Config;

/**
 * Designer Controller
 *
 * the custom designer
 *
 * @author    Brad Felix <bradfelix1@gmail.com>
 * @version   v1.0
 */

class DesignerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
	    $inks6 = InkColors::all()->take(6);
	    $inksrest = InkColors::where('id', '>', 6)->get();
	    $fonts = Fonts::all();

	    $ink_colors = InkColors::all();
	    $ink_color_selection_content_dynamic = "";

	    foreach($ink_colors as $ic){


		    switch($ic->codename){
			    case "black":
			    case "royalblue":
			    case "darkgrey":
			    case "midnightblue":
			    case "kentuckyblue":
			    case "brightblue":
			    case "forestgreen":
			    case "green":
			    case "darkbrown":
			    case "brown":
			    	$fontcolor = "#ffffff";
			    	break;

			    default:
			    	 $fontcolor = "#000000";
		    }

		    $ink_color_selection_content_dynamic .= "<option id='".$ic->codename."' value='".$ic->hexcode."' style='-webkit-appearance:none; background:#".$ic->hexcode." !important; color:".$fontcolor."'>".$ic->name."</option>";
	    }

	    //front upload colors
	    $ink_color_selection_top = "<select class='selectpicker' style='display:none;' id='first_upload_ink_front' name='first_upload_ink_front'>";
		$ink_color_selection_bottom = "</select>";
		$ink_color_selection_content = "<option value=''>Select First Color</option>" . $ink_color_selection_content_dynamic;
	    $the_ink_selection_first_front = $ink_color_selection_top . $ink_color_selection_content . $ink_color_selection_bottom;

	    $ink_color_selection_top = "<select class='selectpicker' style='display:none;'  id='second_upload_ink_front' name='second_upload_ink_front'>";
		$ink_color_selection_bottom = "</select>";
		$ink_color_selection_content = "<option value=''>Select Second Color</option>" . $ink_color_selection_content_dynamic;
	    $the_ink_selection_second_front = $ink_color_selection_top . $ink_color_selection_content . $ink_color_selection_bottom;

	    $ink_color_selection_top = "<select class='selectpicker' style='display:none;'  id='third_upload_ink_front' name='third_upload_ink_front'>";
		$ink_color_selection_bottom = "</select>";
		$ink_color_selection_content = "<option value=''>Select Third Color</option>" . $ink_color_selection_content_dynamic;
		$the_ink_selection_third_front = $ink_color_selection_top . $ink_color_selection_content . $ink_color_selection_bottom;

	    //back upload colors
	    $ink_color_selection_top = "<select class='selectpicker' style='display:none;' id='first_upload_ink_back' name='first_upload_ink_back'>";
		$ink_color_selection_bottom = "</select>";
		$ink_color_selection_content = "<option value=''>Select First Color</option>" . $ink_color_selection_content_dynamic;
	    $the_ink_selection_first_back = $ink_color_selection_top . $ink_color_selection_content . $ink_color_selection_bottom;

	    $ink_color_selection_top = "<select class='selectpicker' style='display:none;'  id='second_upload_ink_back' name='second_upload_ink_back'>";
		$ink_color_selection_bottom = "</select>";
		$ink_color_selection_content = "<option value=''>Select Second Color</option>" . $ink_color_selection_content_dynamic;
	    $the_ink_selection_second_back = $ink_color_selection_top . $ink_color_selection_content . $ink_color_selection_bottom;

	    $ink_color_selection_top = "<select class='selectpicker' style='display:none;'  id='third_upload_ink_back' name='third_upload_ink_back'>";
		$ink_color_selection_bottom = "</select>";
		$ink_color_selection_content = "<option value=''>Select Third Color</option>" . $ink_color_selection_content_dynamic;
	    $the_ink_selection_third_back = $ink_color_selection_top . $ink_color_selection_content . $ink_color_selection_bottom;

	    $garment_types = array(''=>'Select A Garment','3400L'=>'Female V-Neck','3600'=>'Male T-Shirt','3633'=>'Male Tank','3900'=>'Female T-Shirt','8420'=>'Male Moisture Wicked','G185'=>'Unisex Hoodie','G240'=>'Male Long Sleeve','1533'=>'Female Racerback');
        $garment_types_check = array('3400L'=>'Female V-Neck','3600'=>'Male T-Shirt','3633'=>'Male Tank','3900'=>'Female T-Shirt','8420'=>'Male Moisture Wicked','G185'=>'Unisex Hoodie','G240'=>'Male Long Sleeve','1533'=>'Female Racerback');

	    //$one_color

        return view('designer.designer', compact('inks6', 'inksrest', 'fonts', 'the_ink_selection_first_front', 'the_ink_selection_second_front', 'the_ink_selection_third_front', 'the_ink_selection_first_back', 'the_ink_selection_second_back', 'the_ink_selection_third_back', 'garment_types','garment_types_check'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $app = DB::table('orders')->where('id', '=', $id)->delete();
        return Redirect::route('designer.admin');
    }

    /**
     * displays the custom designer
     *
     * @return \Illuminate\Http\Response
     */
    public function shirtadmin(){
        return view('designer.admin');
    }

    /**
     * handles validation of designer form and performs required operations if validation passes
     *
     * @param  \Illuminate\Http\Request  $request
     * @return redirect
     */
    public function shirtsubmit(Request $request){


        $messages = [
            'garment.required' => "You must select a type of garment",
            'name.required' => "Name is required",
            'name.regex' => "Name can only contain alpha numeric characters",
            'phone.required' => "Phone number is required",
            'phone.regex' => "Phone number improperly formatted (format: XXX-XXX-XXXX)",
            'email.required' => "Email is required",
            'email.email' => "Email address should be in an email address format",
            'notes.regex' => "Notes can only contain alpha numeric characters",
            'quantity.between' => "You must order 50 or more shirts"
        ];

        $rules = [
          'garment' => 'required', // alpha - only contain letters
          'quantity' => 'required|numeric|between:50,9999999"',
          'name' => 'required|regex:/^[(a-zA-Z0-9\s)]+$/u',
          'phone' => 'required|regex:/^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/',
          'email' => 'required|email',
          'notes' => 'regex:/^[a-zA-Z0-9,.!? ]*$/u'

        ];

        $sleeveink_rules = $this->sleeveinkrules();

        if(!empty($sleeveink_rules)){
            $rules = array_merge($rules, $sleeveink_rules);
        }

        $validation = Validator::make($_POST, $rules, $messages);

        if($validation->passes()){

            $file_names = $this->upload_designer_graphics($request);

            $order_array = $this->process_designer($_POST, $file_names);

            $full_string = $this->createShopifyProduct($_POST);

            $this->send_email_to_customer($_POST);
            $this->send_email_to($_POST);

            //$this->send_data_to_odoo($order_array);

            return Redirect::away($full_string);
            //return redirect()->action('DesignerController@index');

        } else {
            return back()->withInput()->withErrors($validation);
        }
    }

    /**
     * send email to customer upon submission of form and right before redirect to shopify
     *
     * @param  \Illuminate\Http\Request  $post
     * @return \Illuminate\Http\Response
     */
    function send_email_to_customer($post){

        foreach($post as $key => $value){
            $$key = $value;
        }
        $subject = "Custom Design";
        $emails = array("".$email."");
        Mail::send('email.designer', ['share_id' => $share_id], function ($m) use ($subject, $emails) {
            $m->from('', '');

            $m->to($emails)->subject($subject);
        });
    }

    /**
     * send email to internal upon submission of form and right before redirect to shopify
     *
     * @param  \Illuminate\Http\Request  $post
     * @return \Illuminate\Http\Response
     */
    function send_email_to($post){

        foreach($post as $key => $value){
            $$key = $value;
        }
        $subject = "Custom Design";
        $emails = array('');
        Mail::send('email.internalcustom', [''], function ($m) use ($subject, $emails) {
            $m->from('', '');

            $m->to($emails)->subject($subject);
        });
    }

    /**
     * creates product on shopify
     *
     * @param  \Illuminate\Http\Request  $post
     * @return string
     */
    function createShopifyProduct($post){
        $shopify_checkout_string = "";
        $api_url = "/admin/products.json";
        foreach($post as $key => $value){
            $$key = $value;
        }


        if(!isset($frontprintcolor)){
            $frontprintcolor = '';
        }

        $frontprintcolor = $this->is_var_set('frontprintcolor', $post);

        $backprintcolor = $this->is_var_set('backprintcolor', $post);

        $xxlarge = $this->is_var_set('2xlarge', $post);
        $xxxlarge = $this->is_var_set('3xlarge', $post);


        $variants = array();
        $tags ='';

        switch($garment){
	        case '1530':
			case '3600':
			case '3633':
			case '8420':
			case 'G240':
				$variants = $this->create_product_variant($post);
				break;

			case '3400L':
			case '3900':
			case '1533':
				$variants = $this->create_product_variant($post);
				break;

			case 'G185':
				$variants = $this->create_product_variant($post);
				break;
        }

        if(!isset($shirtcolorselection)){
			$shirtcolorselection = "black";
		}

        if($shirtcolorselection == "militarygreen" ){
            $shirtcolorselection = "Military Green";
        } else if($shirtcolorselection == "heathergrey" ){
            $shirtcolorselection = "Heather Grey";
        } else if($shirtcolorselection == "heavymetal" ){
            $shirtcolorselection = "Heavy Metal";
        } else {
            $shirtcolorselection = ucwords($shirtcolorselection);
        }


            $product_array["product"] = array(
                'title' => 'Custom Product - ' . $name . ' - ' . $shirtcolorselection,
                'body_html' => 'custom product designed by: '. $name,
                'vendor' => 'TShirt Designer',
                'product_type' => 'Custom Shirts',
                'variants' => $variants,
                'tags' => 'Designer',
                'published_scope' => 'web'
            );


            $json = json_encode($product_array);
            $data = json_decode(\App\Helpers\Curl::post($api_url, $json));

            $d_s_id = 0;
            $d_m_id = 0;
            $d_l_id = 0;
            $d_xl_id = 0;
            $d_xxl_id = 0;
            $d_xxxl_id = 0;

            foreach($data->product->variants as $d){
                if($d->option1 == "XSmall"){
                    $d_xs_id = $d->id;
                } else if($d->option1 == "Small"){
                    $d_s_id = $d->id;
                } else if($d->option1 == "Medium"){
                    $d_m_id = $d->id;
                } else if($d->option1 == "Large"){
                    $d_l_id = $d->id;
                } else if($d->option1 == "XLarge"){
                    $d_xl_id = $d->id;
                } else if($d->option1 == "2XLarge"){
                    $d_xxl_id = $d->id;
                } else if($d->option1 == "3XLarge"){
                    $d_xxxl_id = $d->id;
                }
            }

            if(!isset($xsmall)){
				$xsmall = 0;
			}

			if(!isset($xxxlarge)){
				$xxxlarge = 0;
			}

                $sizes_needed = array(
	                "xsmall" => array( 'id'=> $d_xs_id, 'quantity' => $xsmall),
                    "small" => array( 'id'=> $d_s_id, 'quantity' => $small),
                    "medium" => array( 'id'=> $d_m_id, 'quantity' => $medium),
                    "large" => array( 'id'=> $d_l_id, 'quantity' => $large),
                    "xlarge" => array( 'id'=> $d_xl_id, 'quantity' => $xlarge),
                    "xxlarge" => array( 'id'=> $d_xxl_id, 'quantity' => $xxlarge),
                    "xxxlarge" => array( 'id'=> $d_xxxl_id, 'quantity' => $xxxlarge),
                );

                foreach($sizes_needed as $m){
                    if($m['quantity'] > 0){
                        $shopify_checkout_string .= $m['id'].":".$m['quantity'].",";
                    }
                }

                $full_string = "https://www..com/cart/" . $shopify_checkout_string;
                return $full_string;


    }

    /**
     * calculates price of product
     *
     * @param  \Illuminate\Http\Request  $post
     * @return float
     */
    function calc_price($post){
        foreach($post as $key => $value){
            $$key = $value;
        }

        $product_price = '';
        $print_price = '';
        $num_of_ink = '';
        $base_product_price = 2.00;

        switch($garment){
	        case '3600':
		   	case '3900':
		   		$base_product_price = $base_product_price + 1;
		   		break;

		   	case '3633':
		   		$base_product_price = $base_product_price + 1.5;
		   		break;

		   	case '1533':
		   		$base_product_price = $base_product_price ;
		   		break;

		   	case '3400L':
		   		$base_product_price = $base_product_price + 1.5;
		   		break;

			case '8420':
				$base_product_price = $base_product_price + 2;
				break;

			case 'G185':
				$base_product_price = $base_product_price + 8;
				break;

			case 'G240':
				$base_product_price = $base_product_price + 2;
				break;

		}

        if($quantity >= 50 && $quantity <= 99){
            $multiplier = 3.333;
        } else if($quantity >= 100 && $quantity <= 144){
             $multiplier = 3.226;
        } else if($quantity >= 145 && $quantity <= 250){
             $multiplier = 3.125;
        } else if($quantity >= 251 && $quantity <= 500){
             $multiplier = 2.941;
        } else if($quantity > 500){
            $multiplier = 2.857;
        }


        if(!isset($backprintcolor)){
            $backprintcolor = 0;
        }

        if(!isset($frontprintcolor)){
            $frontprintcolor = 0;
        }


        $num_of_ink = intval($frontprintcolor) + intval($backprintcolor);


        if(isset($leftsleeve) && isset($rightsleeve)){
            $sleeve_cost = 2.00;
        } else if(!isset($leftsleeve) && isset($rightsleeve)){
            $sleeve_cost = 1.00;
        } else if(isset($leftsleeve) && !isset($rightsleeve)){
            $sleeve_cost = 1.00;
        } else {
            $sleeve_cost = 0.00;
        }

        $ink_price = 0.04;
        $base_product_price = $base_product_price + 2.15;

        if(isset($backprintcolor) && isset($frontprintcolor)){
	        $labor = 0.50;
        } else {
	        $labor = 0;
        }


        $total_ink_price = $ink_price * $num_of_ink;


        if($num_of_ink > 0){
           $calc_price = ($base_product_price + $labor + $total_ink_price + $sleeve_cost);
        } else {
           $calc_price = ($base_product_price + $total_print_price + $sleeve_cost);
        }

        $markedup = round(($calc_price * $multiplier), 2);
        $new_price = floatval($calc_price) + floatval($markedup);

        return $new_price;
    }

    /**
     * creates mens product variants
     *
     * @param  \Illuminate\Http\Request  $post
     * @return array
     */
    function create_product_variant($post){

        $calc_price = $this->calc_price($post);

        foreach($post as $key => $value){
            $$key = $value;
        }

        $variants = array(
	        array(
                'option1' => 'XSmall',
                'price' => $calc_price,
                'sku' => 'CUSTOM-' . strtoupper($name),
                'weight' => 0.3,
                'weight_unit' => 'lb',
                'inventory_policy' => 'deny',
                'requires_shipping' => true,
                'taxable' => true,
            ),
            array(
                'option1' => 'Small',
                'price' => $calc_price,
                'sku' => 'CUSTOM-' . strtoupper($name),
                'weight' => 0.3125,
                'weight_unit' => 'lb',
                'inventory_policy' => 'deny',
                'requires_shipping' => true,
                'taxable' => true,
            ),
            array(
                'option1' => 'Medium',
                'price' => $calc_price,
                'sku' => 'CUSTOM-' . strtoupper($name),
                'weight' => 0.375,
                'weight_unit' => 'lb',
                'inventory_policy' => 'deny',
                'requires_shipping' => true,
                'taxable' => true,
            ),
            array(
                'option1' => 'Large',
                'price' => $calc_price,
                'sku' => 'CUSTOM-' . strtoupper($name),
                'weight' => 0.4,
                'weight_unit' => 'lb',
                'inventory_policy' => 'deny',
                'requires_shipping' => true,
                'taxable' => true,
            ),
            array(
                'option1' => 'XLarge',
                'price' => $calc_price,
                'sku' => 'CUSTOM-' . strtoupper($name),
                'weight' => 0.4375,
                'weight_unit' => 'lb',
                'inventory_policy' => 'deny',
                'requires_shipping' => true,
                'taxable' => true,
            ),
            array(
                'option1' => '2XLarge',
                'price' => $calc_price + 2.00,
                'sku' => 'CUSTOM-' . strtoupper($name),
                'weight' => 0.5,
                'weight_unit' => 'lb',
                'inventory_policy' => 'deny',
                'requires_shipping' => true,
                'taxable' => true,
            ),
            array(
                'option1' => '3XLarge',
                'price' => $calc_price + 3.00,
                'sku' => 'CUSTOM-' . strtoupper($name),
                'weight' => 0.6,
                'weight_unit' => 'lb',
                'inventory_policy' => 'deny',
                'requires_shipping' => true,
                'taxable' => true,
            ),



        );

        return $variants;
    }



    /**
     * creates front ink rules array
     *
     * @return array
     */
    function frontinkrules(){
        if(isset($_POST['frontprintcolor']) && $_POST['frontprintcolor'] == 1){

            $frontink_rules = [
                'frontinkcolorselection' => 'required'
            ];
            return $frontink_rules;

        } else if(isset($_POST['frontprintcolor']) && $_POST['frontprintcolor'] == 2){

            $frontink_rules = [
                'frontinkcolorselection' => 'required',
                'frontinkcolorselection2' => 'required'
            ];
            return $frontink_rules;

        } else if(isset($_POST['frontprintcolor']) && $_POST['frontprintcolor'] == 3){
            $frontink_rules = [
                'frontinkcolorselection' => 'required',
                'frontinkcolorselection2' => 'required',
                'frontinkcolorselection3' => 'required'
            ];
            return $frontink_rules;
        }
    }

    /**
     * creates back ink rules array
     *
     * @return array
     */
    function backinkrules(){
        if(isset($_POST['backprintcolor']) && $_POST['backprintcolor'] == 1){

            $backink_rules = [
                'backinkcolorselection' => 'required'
            ];
            return $backink_rules;

        } else if(isset($_POST['backprintcolor']) && $_POST['backprintcolor'] == 2){

            $backink_rules = [
                'backinkcolorselection' => 'required',
                'backinkcolorselection2' => 'required'
            ];
            return $backink_rules;

        } else if(isset($_POST['backprintcolor']) && $_POST['backprintcolor'] == 3){
            $backink_rules = [
                'backinkcolorselection' => 'required',
                'backinkcolorselection2' => 'required',
                'backinkcolorselection3' => 'required'
            ];
            return $backink_rules;
        }
    }

    /**
     * creates sleeve ink rules array
     *
     * @return array
     */
    function sleeveinkrules(){
        if(isset($_POST['leftsleeve']) || isset($_POST['rightsleeve'])){

            $sleeveink_rules = [
                'leftsleevecolorselection' => 'required',
                'rightsleevecolorselection' => 'required'
            ];
            return $sleeveink_rules;

        }
    }

    /**
     * process form data and saves to database
     *
     * @param  \Illuminate\Http\Request  $post
     * @param array $file_names
     * @return array
     */
    function process_designer($post, $file_names){


        foreach($post as $key => $value){
            $$key = $value;
        }

        $front_image = '';
        $back_image = '';

        if(!empty($file_names)){
            foreach($file_names as $k => $v){
                $$k = $v;
            }
        }

        if(!isset($frontprintcolor)){
            $frontprintcolor = '';
        }

        $frontprintcolor = $this->is_var_set('frontprintcolor', $post);
        $backprintcolor = $this->is_var_set('backprintcolor', $post);


        $leftsleeve = $this->is_var_set('leftsleeve', $post);
        $rightsleeve = $this->is_var_set('rightsleeve', $post);
        $leftsleevecolorselection = $this->is_var_set('leftsleevecolorselection', $post);
        $rightsleevecolorselection = $this->is_var_set('rightsleevecolorselection', $post);


        if($leftsleeve === null){
	        $leftsleeve = 0;
	        $leftsleevecolorselection = "none";
        }

        if($rightsleeve === null){
	        $rightsleeve = 0;
	        $rightsleevecolorselection = "none";
        }

        $sleevecolorselection = serialize(array($leftsleevecolorselection, $rightsleevecolorselection));

        $xxlarge = $this->is_var_set('2xlarge', $post);
        $xxxlarge = $this->is_var_set('3xlarge', $post);

		if(!isset($shirtcolorselection)){
			$shirtcolorselection = "black";
		}

		if(!isset($xsmall)){
			$xsmall = 0;
		}

		if(!isset($xxxlarge)){
			$xxxlarge = 0;
		}

		$order_array = array(
            'name' => $name,
            'phone' => $phone,
            'email' => $email,
            'garment_type' => $garment,
            'garment_color' => $shirt_color,
            'leftsleeve' => $leftsleeve,
            'rightsleeve' => $rightsleeve,
            'sleeve_ink' => $sleevecolorselection,
            'front_ink_number' => $frontprintcolor,
            'back_ink_number' => $backprintcolor,
            'total_quantity' => $quantity,
            'small' => $small,
            'medium' => $medium,
            'large' => $large,
            'xlarge' => $xlarge,
            'xxlarge' => $xxlarge,
            'xxxlarge' => $xxxlarge,
            'xsmall' => $xsmall,
            'share_id' => $share_id,
            'front_image' => $front_image,
            'back_image'=> $back_image,
            'front_upload_ink_one' => $first_upload_ink_front,
            'front_upload_ink_two' => $second_upload_ink_front,
            'front_upload_ink_three' => $third_upload_ink_front,
            'back_upload_ink_one' => $first_upload_ink_back,
            'back_upload_ink_two' => $second_upload_ink_back,
            'back_upload_ink_three' => $third_upload_ink_back,
            'notes' => $notes,
            'created_at' => $now->toDateTimeString(),
            'updated_at' => $now->toDateTimeString()
        );

        $now = Carbon::now();
        DB::table('orders')->insert($order_array);

        return $order_array;

    }

    /**
     * validates upload is an image and then stores it, and returns a UUID file name
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    function upload_designer_graphics($request){
        if( $request->file('frontimage') || $request->file('backimage') ){
            if( $request->file('frontimage') && $request->file('backimage')  ){
                $files = array('frontimage' => $request->file('frontimage'), 'backimage' => $request->file('backimage'));
                $rules = array('frontimage' => 'required|image', 'backimage' => 'required|image');
                $validator = Validator::make($files, $rules);
                $front_image_name = \Uuid::generate(4).'.'.$request->file('frontimage')->getClientOriginalExtension();
                $back_image_name = \Uuid::generate(4).'.'.$request->file('backimage')->getClientOriginalExtension();
            } else if( $request->file('frontimage') && !$request->file('backimage')  ){
                $files = array('frontimage' => $request->file('frontimage'));
                $rules = array('frontimage' => 'required|image');
                $validator = Validator::make($files, $rules);
                $front_image_name = \Uuid::generate(4).'.'.$request->file('frontimage')->getClientOriginalExtension();
            } else if( !$request->file('frontimage') && $request->file('backimage')  ){
                $files = array('backimage' => $request->file('backimage') );
                $rules = array('backimage' => 'required|image');
                $validator = Validator::make($files, $rules);
                $back_image_name = \Uuid::generate(4).'.'.$request->file('backimage')->getClientOriginalExtension();
            }
            if ($validator->passes()) {
                $path = storage_path() . "/app/designer/";
                if(isset($front_image_name) && isset($back_image_name)){
                    $files['frontimage']->move($path, $front_image_name); // uploading file to given path
                    $files['backimage']->move($path, $back_image_name); // uploading file to given path
                    $file_names = array('front_image' => $front_image_name, 'back_image' => $back_image_name);
                    return $file_names;
                } else if(isset($front_image_name) && !isset($back_image_name)){
                    $files['frontimage']->move($path, $front_image_name); // uploading file to given path
                    $file_names = array('front_image' => $front_image_name);
                    return $file_names;
                } else if(!isset($front_image_name) && isset($back_image_name)){
                    $files['backimage']->move($path, $front_image_name); // uploading file to given path
                    $file_names = array('back_image' => $back_image_name);
                    return $file_names;
                }
            } else {
                return Redirect::route('designer.show')
                    ->withInput()
                    ->withErrors($validator);
            }
        }
        return;
    }

    /**
     * validates upload is an image and then stores it, and returns a UUID file name
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    function upload_customs_graphics($request){

        if( $request->file('frontimage1') ||
            $request->file('frontimage2') ||
            $request->file('frontimage3') ||
            $request->file('backimage1') ||
            $request->file('backimage2') ||
            $request->file('backimage3') ||
            $request->file('leftsleeveimage1') ||
            $request->file('leftsleeveimage2') ||
            $request->file('rightsleeveimage1') ||
            $request->file('rightsleeveimage2') ){

            $files = array();
            $rules = array();

            if( $request->file('frontimage1') ){
                $files['frontimage1'] = $request->file('frontimage1');
                $rules['frontimage1'] = 'required|image';
                $front_image_name_1 = \Uuid::generate(4).'.'.$request->file('frontimage1')->getClientOriginalExtension();
            }

            if( $request->file('frontimage2') ){
                $files['frontimage2'] = $request->file('frontimage2');
                $rules['frontimage2'] = 'required|image';
                $front_image_name_2 = \Uuid::generate(4).'.'.$request->file('frontimage1')->getClientOriginalExtension();
            }

            if( $request->file('frontimage3') ){
                $files['frontimage3'] = $request->file('frontimage3');
                $rules['frontimage3'] = 'required|image';
                $front_image_name_3 = \Uuid::generate(4).'.'.$request->file('frontimage1')->getClientOriginalExtension();
            }

            if( $request->file('backimage1') ){
                $files['backimage1'] = $request->file('backimage1');
                $rules['backimage1'] = 'required|image';
                $back_image_name_1 = \Uuid::generate(4).'.'.$request->file('backimage1')->getClientOriginalExtension();
            }

            if( $request->file('backimage2') ){
                $files['backimage2'] = $request->file('backimage2');
                $rules['backimage2'] = 'required|image';
                $back_image_name_2 = \Uuid::generate(4).'.'.$request->file('backimage2')->getClientOriginalExtension();
            }

            if( $request->file('backimage3') ){
                $files['backimage3'] = $request->file('backimage3');
                $rules['backimage3'] = 'required|image';
                $back_image_name_3 = \Uuid::generate(4).'.'.$request->file('backimage3')->getClientOriginalExtension();
            }

            if( $request->file('leftsleeveimage1') ){
                $files['leftsleeveimage1'] = $request->file('leftsleeveimage1');
                $rules['leftsleeveimage1'] = 'required|image';
                $leftsleeve_image_name_1 = \Uuid::generate(4).'.'.$request->file('leftsleeveimage1')->getClientOriginalExtension();
            }

            if( $request->file('leftsleeveimage2') ){
                $files['leftsleeveimage2'] = $request->file('leftsleeveimage2');
                $rules['leftsleeveimage2'] = 'required|image';
                $leftsleeve_image_name_2 = \Uuid::generate(4).'.'.$request->file('leftsleeveimage2')->getClientOriginalExtension();
            }

            if( $request->file('rightsleeveimage1') ){
                $files['rightsleeveimage1'] = $request->file('rightsleeveimage1');
                $rules['rightsleeveimage1'] = 'required|image';
                $rightsleeve_image_name_1 = \Uuid::generate(4).'.'.$request->file('rightsleeveimage1')->getClientOriginalExtension();
            }

            if( $request->file('rightsleeveimage2') ){
                $files['rightsleeveimage2'] = $request->file('rightsleeveimage2');
                $rules['rightsleeveimage2'] = 'required|image';
                $rightsleeve_image_name_2 = \Uuid::generate(4).'.'.$request->file('rightsleeveimage2')->getClientOriginalExtension();
            }

            $validator = Validator::make($files, $rules);

            if ($validator->passes()) {

                $path = storage_path() . "/app/designer/";
                $file_names = array();

                if(isset($front_image_name_1)){
                    $files['frontimage1']->move($path, $front_image_name_1); // uploading file to given path
                    $file_names['front']['image1'] = $front_image_name_1;
                }

                if(isset($front_image_name_2)){
                    $files['frontimage2']->move($path, $front_image_name_2); // uploading file to given path
                    $file_names['front']['image2'] = $front_image_name_2;
                }

                if(isset($front_image_name_3)){
                    $files['frontimage3']->move($path, $front_image_name_3); // uploading file to given path
                    $file_names['front']['image3'] = $front_image_name_3;
                }

                if(isset($back_image_name_1)){
                    $files['backimage1']->move($path, $back_image_name_1); // uploading file to given path
                    $file_names['back']['image1'] = $back_image_name_1;
                }

                if(isset($back_image_name_2)){
                    $files['backimage2']->move($path, $back_image_name_2); // uploading file to given path
                    $file_names['back']['image2'] = $back_image_name_2;
                }

                if(isset($back_image_name_3)){
                    $files['backimage3']->move($path, $back_image_name_3); // uploading file to given path
                    $file_names['back']['image3'] = $back_image_name_3;
                }

                if(isset($leftsleeve_image_name_1)){
                    $files['leftsleeveimage1']->move($path, $leftsleeve_image_name_1); // uploading file to given path
                    $file_names['leftsleeve']['image1'] = $leftsleeve_image_name_1;
                }

                if(isset($leftsleeve_image_name_2)){
                    $files['leftsleeveimage2']->move($path, $leftsleeve_image_name_2); // uploading file to given path
                    $file_names['leftsleeve']['image2'] = $leftsleeve_image_name_2;
                }

                if(isset($rightsleeve_image_name_1)){
                    $files['rightsleeveimage1']->move($path, $rightsleeve_image_name_1); // uploading file to given path
                    $file_names['rightsleeve']['image1'] = $rightsleeve_image_name_1;
                }

                if(isset($rightsleeve_image_name_2)){
                    $files['rightsleeveimage2']->move($path, $rightsleeve_image_name_2); // uploading file to given path
                    $file_names['rightsleeve']['image2'] = $rightsleeve_image_name_2;
                }

                return $file_names;


            } else {
                return Redirect::route('designer.show')
                    ->withInput()
                    ->withErrors($validator);
            }
        }
        return;
    }


    public function designer_upload_artwork(){
        $img = $_POST['image'];
        $img = str_replace('data:image/png;base64,', '', $img);
    	$img = str_replace(' ', '+', $img);
    	$data = base64_decode($img);

        $file_name = \Uuid::generate(4) . ".png";
        $path = storage_path() . "/app/designer/";
        $file = $path . $file_name;
	    $image = imagecreatefromstring($data);
        if ($image !== false) {
            imageAlphaBlending($image, true);
            imageSaveAlpha($image, true);

            imagepng($image, $file);
            imagedestroy($image);
        } else {
            return 0;
        }

        return $file_name;
    }

    /**
     * checks to see if a variable exists in the giant post array
     *
     * @param string $var
     * @param  \Illuminate\Http\Request  $post
     * @return array
     */
    function is_var_set($var, $post){
        foreach($post as $key => $value){
            if($key == $var){
                return $value;
            }
        }

        return null;
    }

    public function designer_admin(){
        $orders = DB::table('orders')->orderBy('created_at', 'desc')->paginate(10);
        foreach($orders as $order){
            $order->created_at = Carbon::parse(''.$order->created_at.'')->setTimezone('America/Chicago');
        }
        return view('designer.admin', compact('orders', $orders));
    }

    public function designer_data($id){
        $orders = DB::table('orders')->where('id', '=', $id)->take(1)->get();
        foreach($orders as $a){

            if($a->leftsleeve == 1){
                $a->leftsleeve = "Yes";
            }else{
                $a->leftsleeve = "No";
            }

            if($a->rightsleeve == 1){
                $a->rightsleeve = "Yes";
            }else{
                $a->rightsleeve = "No";
            }

            $sleeve_ink =  unserialize($a->sleeve_ink);
            $a->sleeve_ink = "LEFT: ".$sleeve_ink[0].", RIGHT: " . $sleeve_ink[1];



            $a->created_at = Carbon::parse(''.$a->created_at.'')->setTimezone('America/Chicago');
        }

        return view('designer.shirtdata', compact('orders', $orders));
    }

   public function icons(){
	   $rows = Icons::all();

	   return view('designer.icons', compact('rows'));
   }

   public function addIcon(Request $request){
	    $this->validate($request, [
        	'image'  => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        	'name'   => 'required|alpha_dash',
        	'target' => 'required|alpha_dash'
		]);


	    $image = $request->file('image');
	    $path = public_path() . "/images/designer/icons";
        $fileName = \Uuid::generate(4).'.'.$image->getClientOriginalExtension(); // renameing image
	    $image->move($path, $fileName);
	    $fullpath = "/images/designer/icons/" . $fileName;

	    $icons = new Icons;
        $icons->name = $request->name;
        $icons->target = $request->target;
        $icons->file = $fullpath;
        $icons->save();


	    return back()->with('status','Image Upload successful');

   }

   public function Icondelete($id){
	   $icon = Icons::find($id);
	   $icon->delete();
	   return back()->with('status','Icon Deleted');
   }

   public function clipart(){
	   $rows = Clipart::paginate(10);

	   return view('designer.clipart', compact('rows'));
   }

   public function addClipart(Request $request){
	    $this->validate($request, [
        	'image'  => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        	'name'   => 'required|alpha_dash',
        	'sequence' => 'required|alpha_dash',
        	'type' => 'required|alpha_dash'
		]);


	    $image = $request->file('image');
	    $path = public_path() . "/images/designer/clipart/" . $request->type;
        $fileName = \Uuid::generate(4).'.'.$image->getClientOriginalExtension(); // renameing image
	    $image->move($path, $fileName);
	    $fullpath = "/images/designer/clipart/" . $request->type. "/" . $fileName;

	    $clipart = new Clipart;
        $clipart->name = $request->name;
        $clipart->sequence = $request->sequence;
        $clipart->type = $request->type;
        $clipart->file = $fullpath;
        $clipart->save();


	    return back()->with('status','Clipart creation successful');

   }

   public function Clipartdelete($id){
	   $clipart = Clipart::find($id);
	   $clipart->delete();
	   return back()->with('status','Clipart Deleted');
   }

   public function shirts(){
	   $rows = DB::table('shirts')
			        ->join('shirt_colors', 'shirts.colors_id', '=', 'shirt_colors.id')
			        ->join('shirt_type', 'shirts.shirt_types_id', '=', 'shirt_type.id')
			        ->orderBy('shirts.created_at', 'asc')
			        ->select('shirts.*', 'shirt_colors.name', 'shirt_type.type')
			        ->paginate(10);

	   $types = Shirt_types::orderBy('type','asc')->pluck('type', 'id');
	   $colors = Colors::orderBy('name','asc')->pluck('name', 'id');



	   return view('designer.shirts', compact('rows', 'types', 'colors'));
   }

   public function addShirt(Request $request){
	    $this->validate($request, [
        	'front_image'  => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        	'back_image'  => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        	'sleeve_front_left'  => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        	'sleeve_front_right'  => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        	'sleeve_back_left'  => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        	'sleeve_back_right'  => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        	'type'   => 'required',
        	'colors'   => 'required|min:1|max:1'
		]);

		$type_name = Shirt_types::find($request->type);


	    $front_image = $request->file('front_image');
	    $back_image = $request->file('back_image');
	    $path = public_path() . "/images/designer/shirts/" . $type_name->type;
	    $fileName1 = \Uuid::generate(4).'.'.$front_image->getClientOriginalExtension(); // renameing image
        $fileName2 = \Uuid::generate(4).'.'.$back_image->getClientOriginalExtension(); // renameing image_type_to_extension
        $fullpath1 = "/images/designer/shirts/" . $type_name->type. "/" . $fileName1;
	    $fullpath2 = "/images/designer/shirts/" . $type_name->type. "/" . $fileName2;
	    $front_image->move($path, $fileName1);
	    $back_image->move($path, $fileName2);


	    $front_left_image = $request->file('sleeve_front_left');
	    if($front_left_image !== null){
		     $fileName3 = \Uuid::generate(4).'.'.$front_left_image->getClientOriginalExtension(); // renameing image
		     $front_left_image->move($path, $fileName3);
		     $fullpath3 = "/images/designer/shirts/" . $type_name->type. "/" . $fileName3;
	    } else {
		    $fullpath3 = null;
	    }

	    $front_right_image = $request->file('sleeve_front_right');
	    if($front_right_image !== null){
		     $fileName4 = \Uuid::generate(4).'.'.$front_right_image->getClientOriginalExtension(); // renameing image
		     $front_right_image->move($path, $fileName4);
		     $fullpath4 = "/images/designer/shirts/" . $type_name->type. "/" . $fileName4;
	    } else {
		    $fullpath4 = null;
	    }

	    $back_left_image = $request->file('sleeve_back_left');
	    if($back_left_image !== null){
		     $fileName5 = \Uuid::generate(4).'.'.$back_left_image->getClientOriginalExtension(); // renameing image
		     $back_left_image->move($path, $fileName5);
		     $fullpath5 = "/images/designer/shirts/" . $type_name->type. "/" . $fileName5;
	    } else {
		    $fullpath5 = null;
	    }

	    $back_right_image = $request->file('sleeve_back_right');
	    if($back_right_image !== null){
		     $fileName6 = \Uuid::generate(4).'.'.$back_right_image->getClientOriginalExtension(); // renameing image
		     $back_right_image->move($path, $fileName6);
		     $fullpath6 = "/images/designer/shirts/" . $type_name->type. "/" . $fileName6;
	    } else {
		    $fullpath6 = null;
	    }

	    $shirts = new Shirts;
        $shirts->shirt_types_id = $request->type;
        $shirts->colors_id = $request->colors[0];
        $shirts->front_image = $fullpath1;
        $shirts->back_image = $fullpath2;
        $shirts->sleeve_front_left = $fullpath3;
        $shirts->sleeve_front_right = $fullpath4;
        $shirts->sleeve_back_left = $fullpath5;
        $shirts->sleeve_back_right = $fullpath6;
        $shirts->save();


	    return back()->with('status','Shirt creation successful');

   }

   public function shirtdelete($id){
	   $clipart = Shirts::find($id);
	   $clipart->delete();
	   return back()->with('status','Clipart Deleted');
   }

   public function shirttypes(){
	   $rows = Shirt_types::paginate(10);
	   return view('designer.type', compact('rows'));
   }

   public function addShirttype(Request $request){
	    $this->validate($request, [
        	'type' => 'required|alpha_num'
		]);

	    $type = Shirt_types::where('name', '=', $request->type)->exists();
	    if($type === false){
		    $shirt_types = new Shirt_types;
	        $shirt_types->type = $request->type;
	        $shirt_types->save();
			return back()->with('status','Shirt Type Added');
		} else {
			return back()->with('statuserror','Shirt Type Exists');
		}

   }

   public function shirttypedelete($id){
	   $shirt_types = Shirt_types::find($id);
	   $shirt_types->delete();
	   return back()->with('status','Shirt Type Deleted');
   }


   public function colors(){
	   $rows = Colors::paginate(10);
	   return view('designer.color', compact('rows'));
   }


   public function addColors(Request $request){
	    $this->validate($request, [
        	'name' => 'required|alpha_spaces',
        	'hexcode' => 'required|alpha_num'
		]);

		$color = Colors::where('name', '=', $request->name)->exists();
	    if($color === false){
		    $colors = new Colors;
	        $colors->name = $request->name;
	        $colors->hexcode = $request->hexcode;
	        $colors->save();
		    return back()->with('status','Color Added');
	    } else {
		    return back()->with('statuserror','Color Exists');
	    }

   }

   public function colordelete($id){
	   $colors = Colors::find($id);
	   $colors->delete();
	   return back()->with('status','Color Deleted');
   }

   public function getIconData(){

	   $icon_array = array();
	   $icons = Icons::all();
	   foreach($icons as $icon){
		   $icon_array[] = array(
			   $icon->name => array(
				   'file' => $icon->file,
				   'target' =>$icon->target
			   )
		   );
	   }

	   $icon_array = json_encode(array('icons' => $icon_array));

	   echo $icon_array;
	   die;
   }

   public function getShirtData($shirt_type, $color){
	   $color_id = '';
	   $type_id = '';

	   $color_data = Colors::where('shorthand', '=', $color)->get();
	   foreach($color_data as $cd){
		   $color_id = $cd->id;
	   }

	   $type_data = Shirt_types::where('type', '=', $shirt_type)->get();
	   foreach($type_data as $td){
		   $type_id = $td->id;
	   }

	   $colors_for_shirt = Shirts::where('shirt_types_id','=',$type_id)->get();
	   $colors_array = array();
	   foreach($colors_for_shirt as $cfs){
		   $color_name = Colors::find($cfs->colors_id);
		   $key = strtolower(preg_replace('/\s+/', '', $color_name->name));
		   $colors_array[$key] = array(
				'hexcode' => $color_name->hexcode,
				'name' => $color_name->name
		   );
	   }

	   $shirts_data = Shirts::where('shirt_types_id','=',$type_id)->where('colors_id','=',$color_id)->get();
	   $shirt_data = array();
	   foreach($shirts_data as $sd){
		   $shirt_data = array(
			   $shirt_type => array(
				   "images" => array(
					   $color => array(
						   "front" => array('file' => $sd->front_image),
						   "back" => array('file' => $sd->back_image)
					   )
				   ),
				   "sleeves" => array(
					   "front" => array(
						   "flag" => array('file' => $sd->sleeve_front_right),
						   "logo" => array('file' => $sd->sleeve_front_left)
					   ),
					   "back" => array(
						   "flag" => array('file' => $sd->sleeve_back_right),
						   "logo" => array('file' => $sd->sleeve_back_left)
					   )
				   ),
				   "colors" => $colors_array

			   )
		   );
	   }

	   echo json_encode(array("shirts" => $shirt_data));
	   die;
   }

   public function getWeaponsData(){
	   $weapon_array = array();
	   $weapons = Clipart::all()->where('type', '=', 'weapon');

	   foreach($weapons as $weapon){
		   $weapon_array[] = array(
			   $weapon->name => array(
				   'file' => $weapon->file,
				   'sequence' =>$weapon->sequence,
				   'type' =>$weapon->type
			   )
		   );
	   }

	   $weapon_array = json_encode(array('clipartweapons' => $weapon_array));

	   echo $weapon_array;
	   die;
   }

   public function getSkullsData(){
	   $skulls_array = array();
	   $skulls = Clipart::all()->where('type', '=', 'skull');

	   foreach($skulls as $skull){
		   $skulls_array[] = array(
			   $skull->name => array(
				   'file' => $skull->file,
				   'sequence' =>$skull->sequence,
				   'type' =>$skull->type
			   )
		   );
	   }

	   $skulls_array = json_encode(array('clipartskulls' => $skulls_array));

	   echo $skulls_array;
	   die;
   }

   public function saveShareData(){
	   $shirt_color = strip_tags($_POST["shirtcolor"]);
	   $shirt_type = strip_tags($_POST["shirttype"]);
	   $left_sleeve = strip_tags($_POST["leftsleeve"]);
	   $right_sleeve = strip_tags($_POST["rightsleeve"]);
	   $front_canvas = strip_tags($_POST["frontcanvas"]);
	   $back_canvas = strip_tags($_POST["backcanvas"]);
	   $canvas_colors = strip_tags($_POST["canvascolors"]);
	   $front_font_data = strip_tags($_POST["front_font_data"]);
	   $back_font_data = strip_tags($_POST["back_font_data"]);
	   $share_id = self::generateRandomString(6);



	   $share = new Share;
	   $share->shirt_color = $shirt_color;
	   $share->shirt_type = $shirt_type;
	   $share->left_sleeve_color = $left_sleeve;
	   $share->right_sleeve_color = $right_sleeve;
	   $share->front_canvas = $front_canvas;
	   $share->back_canvas = $back_canvas;
	   $share->share_id = $share_id;
	   $share->canvas_colors = $canvas_colors;
	   $share->front_font_data = $front_font_data;
	   $share->back_font_data = $back_font_data;
	   $share->save();

	   $url = "http://designer..com/share/" . $share_id;

	   return $url;
   }

   public function generateRandomString($length) {
	    $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}

	public function getShareData($share_id){
		$share_data = Share::all()->where('share_id', '=', $share_id)->toArray();
		$need_to_know_data = array();
		$master_shirt_array = array();

		foreach($share_data as $shared){

			$canvas_colors = json_decode(json_decode($shared['canvas_colors']));
			$front_font_data = json_decode($shared['front_font_data'],true);
			$back_font_data = json_decode($shared['back_font_data'],true);

			if($canvas_colors){
				$color_name_array = array();
				foreach($canvas_colors as $cc){
					$hex = $cc;
					$color_name = self::translateColors($hex);
					$color_name_array[] = $color_name;
				}
			} else {
				$color_name_array = array();
			}


			$color_data = Colors::all()->where('shorthand', '=', $shared['shirt_color']);
			foreach($color_data as $cd){
			   $color_id = $cd->id;
			   $shirt_color = $cd->name;
			}

			$type_data = Shirt_types::all()->where('type', '=', $shared['shirt_type']);
			foreach($type_data as $td){
			   $type_id = $td->id;
			}

			$shirt_data = Shirts::all()->where('shirt_types_id','=',$type_id)->where('colors_id','=',$color_id)->toArray();

			foreach($shirt_data as $sd){
				$need_to_know_data = array(
					"front_left_sleeve_image" => $sd['sleeve_front_left'],
					"front_right_sleeve_image" => $sd['sleeve_front_right'],
					"back_left_sleeve_image" => $sd['sleeve_back_left'],
					"back_right_sleeve_image" => $sd['sleeve_back_right'],
					"front_image" => $sd['front_image'],
					"back_image" => $sd['back_image'],
				);
			}

			if($shared['left_sleeve_color'] === 'null' || $shared['left_sleeve_color'] == ""){
				$shared['left_sleeve_color'] = '#ffffff';
			}

			if($shared['right_sleeve_color'] === 'null' || $shared['right_sleeve_color'] == ""){
				$shared['right_sleeve_color'] = '#ffffff';
			}


			switch($shared['shirt_type']){

	            case '3400L':
	            	$shirt_type = "Female V-Neck";
		        	break;

		        case '3600':
	            	$shirt_type = "Male T-Shirt";
		        	break;

		        case '3633':
	            	$shirt_type = "Male Tank";
		        	break;

		        case '3900':
	            	$shirt_type = "Female T-Shirt";
		        	break;

		        case '8420':
	            	$shirt_type = "Male Moisture Wicked";
		        	break;

		        case 'G185':
	            	$shirt_type = "Unisex Hoodie";
		        	break;

				case 'G240':
	            	$shirt_type = "Male Long Sleeve";
		        	break;

		        case '1533':
	            	$shirt_type = "Female Racerback";
		        	break;
			}

			$master_shirt_array = array(
				"front_canvas" => $shared['front_canvas'],
				"back_canvas" => $shared['back_canvas'],
				"left_sleeve_color" => $shared['left_sleeve_color'],
				"right_sleeve_color" => $shared['right_sleeve_color'],
				"shirt_type" => $shirt_type,
				"canvas_colors" => $color_name_array,
				"shirt_color" => $shirt_color,
				"share_id" => $share_id,
				"front_font_data" => json_encode($front_font_data),
				"back_font_data" => json_encode($back_font_data),

			);
		}

		$master_shirt_array = array_merge($master_shirt_array, $need_to_know_data);

		return view('designer.share', compact('master_shirt_array'));
	}

	public function translateColors($record){
		switch($record){
            case "#ffffff":
                $color = "white";
            break;

            case "#231f20":
                $color = "black";
            break;

            case "#cf112b":
                $color = "red";
            break;

            case "#0033a0":
                $color = "royal blue";
            break;

            case "#9a989b":
                $color = "grey";
            break;

            case "#fdcd01":
                $color = "maize";
            break;

            case "#56555a":
                $color = "dark grey";
            break;

            case "#ccc8c7":
                $color = "light grey";
            break;

            case "#9A381f":
                $color = "brick";
            break;

            case "#A41F34":
                $color = "cardinal";
            break;

            case "#c24d00":
                $color = "team orange";
            break;

            case "#fd8103":
                $color = "bright orange";
            break;

            case "#f1a693":
                $color = "peach";
            break;

            case "#ec6ab2":
                $color = "pink";
            break;

            case "#b218ac":
                $color = "purple";
            break;

            case "#0f1f2e":
                $color = "midnight blue";
            break;

            case "#0a2f87":
                $color = "kentucky blue";
            break;

            case "#1b45bb":
                $color = "bright blue";
            break;

            case "#209bde":
                $color = "periwinkle";
            break;

            case "#318ede":
                $color = "infantry blue";
            break;

            case "#7b9bc1":
                $color = "steel blue";
            break;

            case "#cd9700":
                $color = "old gold";
            break;

            case "#ff9e19":
                $color = "gold";
            break;

            case "#ffa409":
                $color = "yellow gold";
            break;

            case "#fddb00":
                $color = "yellow";
            break;

            case "#ffc525":
                $color = "banana";
            break;

            case "#f6e0a4":
                $color = "wheat";
            break;

            case "#295234":
                $color = "forest green";
            break;

            case "#5a6219":
                $color = "green";
            break;

            case "#01963a":
                $color = "bright green";
            break;

            case "#39d52c":
                $color = "neon green";
            break;

            case "#522a1e":
                $color = "dark brown";
            break;

            case "#753c29":
                $color = "brown";
            break;

            case "#954d12":
                $color = "light brown";
            break;

            case "#836040":
                $color = "army tan";
            break;

            case "#9d5f16":
                $color = "rust";
            break;

            case "#bf9559":
                $color = "combat tan";
            break;

            case "#cda776":
                $color = "usa tan";
            break;

            case "#b6a16a":
                $color = "vegas gold";
            break;

            case "#cec5a6":
                $color = "vintage white";
            break;

            default:
            	$color = "black";
            	break;
        }

        return $color;
	}

	public function getShareData2($share_id){
		$share_data = Share::all()->where('share_id', '=', $share_id)->toArray();
		$need_to_know_data = array();
		$master_shirt_array = array();

		foreach($share_data as $shared){

			$canvas_colors = json_decode(json_decode($shared['canvas_colors']));
			$front_font_data = json_decode($shared['front_font_data'],true);
			$back_font_data = json_decode($shared['back_font_data'],true);


			$color_name_array = array();
			if($canvas_colors){
				foreach($canvas_colors as $cc){
					$hex = "#".$cc;
					$color_name = self::translateColors($hex);
					$color_name_array[] = $color_name;
				}
			} else {
				$color_name_array = array();
			}


			$color_data = Colors::all()->where('shorthand', '=', $shared['shirt_color']);
			foreach($color_data as $cd){
			   $color_id = $cd->id;
			   $shirt_color = $cd->name;
			}

			$type_data = Shirt_types::all()->where('type', '=', $shared['shirt_type']);
			foreach($type_data as $td){
			   $type_id = $td->id;
			}
			$shirt_data = Shirts::all()->where('shirt_types_id','=',$type_id)->where('colors_id','=',$color_id)->toArray();

			foreach($shirt_data as $sd){
				$need_to_know_data = array(
					"front_left_sleeve_image" => $sd['sleeve_front_left'],
					"front_right_sleeve_image" => $sd['sleeve_front_right'],
					"back_left_sleeve_image" => $sd['sleeve_back_left'],
					"back_right_sleeve_image" => $sd['sleeve_back_right'],
					"front_image" => $sd['front_image'],
					"back_image" => $sd['back_image'],
				);
			}

			if($shared['left_sleeve_color'] === 'null' || $shared['left_sleeve_color'] == ""){
				$shared['left_sleeve_color'] = 'ffffff';
			}

			if($shared['right_sleeve_color'] === 'null' || $shared['right_sleeve_color'] == ""){
				$shared['right_sleeve_color'] = 'ffffff';
			}


			switch($shared['shirt_type']){

	            case '3400L':
	            	$shirt_type = "Female V-Neck";
		        	break;

		        case '3600':
	            	$shirt_type = "Male T-Shirt";
		        	break;

		        case '3633':
	            	$shirt_type = "Male Tank";
		        	break;

		        case '3900':
	            	$shirt_type = "Female T-Shirt";
		        	break;

		        case '8420':
	            	$shirt_type = "Male Moisture Wicked";
		        	break;

		        case 'G185':
	            	$shirt_type = "Unisex Hoodie";
		        	break;

				case 'G240':
	            	$shirt_type = "Male Long Sleeve";
		        	break;

		        case '1533':
	            	$shirt_type = "Female Racerback";
		        	break;
			}

			$master_shirt_array = array(
				"front_canvas" => $shared['front_canvas'],
				"back_canvas" => $shared['back_canvas'],
				"left_sleeve_color" => $shared['left_sleeve_color'],
				"right_sleeve_color" => $shared['right_sleeve_color'],
				"shirt_type" => $shirt_type,
				"shirt_type_number" => $shared['shirt_type'],
				"canvas_colors" => $color_name_array,
				"shirt_color" => $shirt_color,
				"share_id" => $share_id,
				"front_font_data" => json_encode($front_font_data),
				"back_font_data" => json_encode($back_font_data),

			);
		}

		$master_shirt_array = array_merge($master_shirt_array, $need_to_know_data);

		return view('designer.share2', compact('master_shirt_array'));
	}

    public function customs($salesperson = '')
    {
        $inks6 = InkColors::all()->take(6);
        $inksrest = InkColors::where('id', '>', 6)->get();
        $fonts = Fonts::all();
        if(!$salesperson){
            $salesp = array();
        } else {
            $salesp = SalesPersons::where('url', $salesperson)->get();
        }

        $ink_colors = InkColors::all();

        $garment_types = array('3400L'=>'Female V-Neck','3600'=>'Male T-Shirt','3633'=>'Male Tank','3900'=>'Female T-Shirt','8420'=>'Male Moisture Wicked','G185'=>'Unisex Hoodie','G240'=>'Male Long Sleeve','1533'=>'Female Racerback');

        //$one_color

        return view('designer.customs', compact('ink_colors', 'fonts','garment_types','salesp'));
    }

    public function randomNumSizing($randomnum){
        $order = Preorders::where('generated_url', $randomnum)->get()->toArray();
        $shirt_types = unserialize($order[0]['shirt_types']);

        $shirts = array();
        foreach($shirt_types as $k =>$v){
            foreach($v as $color) {
                $shirts[] = array("type" => $k, "color" => $color);
            }
        }

        $order_array = array(
            "name" => $order['0']['name'],
            "shirts" => $shirts,
            "customerimage1" => (isset($order['0']['customerimage1']))?$order['0']['customerimage1']:"",
            "customerimage2" => (isset($order['0']['customerimage2']))?$order['0']['customerimage2']:"",
            "customerimage3" => (isset($order['0']['customerimage3']))?$order['0']['customerimage3']:"",
            "customerimage4" => (isset($order['0']['customerimage4']))?$order['0']['customerimage4']:"",
            "customerimage5" => (isset($order['0']['customerimage5']))?$order['0']['customerimage5']:"",
        );

        $host = Config::get('app.sales_url');


        return view('designer.customs_sub_views.quantity', compact('order_array','randomnum', 'host'));
    }
}
