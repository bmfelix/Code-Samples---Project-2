<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Input;
use Validator;
use Redirect;
use Hash;
use Mail;
use Carbon\Carbon;
use Session;
use Config;
use App\Http\Models\InkColors;
use App\Http\Models\Fonts;
use App\Http\Models\Colors;
use App\Http\Models\Preorders;
use App\Http\Models\Order_sizes;
use CustomsService;
use CustomsMiscService;
use CustomsSubmissionService;
use App\Jobs\RunCustomsJobs;
use QuickBaseService;
use CustomsAdminService;

/**
 * Designer Controller
 *
 * the custom designer
 *
 * @author    Brad Felix <bradfelix1@gmail.com>
 * @version   v1.0
 */

class CustomsController extends Controller
{
    public function index($salesperson = '')
    {
        $fonts = Fonts::all();
        $salesp = CustomsMiscService::getEmployee($salesperson);
        $ink_colors = InkColors::all();
        $color_ids = [91, 90, 29, 1, 34, 23, 5, 2, 28, 78, 3, 80, 31];
        sort($color_ids);
        $colors = CustomsMiscService::getColorData($color_ids);
        foreach($colors as $color){
            if($color->name === "Tan 499" || $color->name === "Navy Green"){
                $color->name = $color->name . " (Men's Only)";
            }
        }
        $garment_types = array('6405'=>'Female V-Neck','3600'=>'Male T-Shirt','3633'=>'Male Tank','B6400'=>'Female T-Shirt','8420'=>'Male Moisture Wicked','G185'=>'Unisex Hoodie','G240'=>'Male Long Sleeve','1533'=>'Female Racerback');

        return view('designer.customs', compact('ink_colors', 'fonts','garment_types','salesp','colors'));
    }

    public function randomNumSizing($randomnum)
    {
        $order_array = CustomsService::getPreorderData($randomnum);
        $order_sizes = Order_sizes::where('order_id', $order_array['id'])->get();
        $host = Config::get('app.sales_url');
        $size_list = [];
        if($order_sizes->isNotEmpty()){
            foreach($order_sizes as $os){
                foreach($order_array['shirts'] as $key=>$value){
                    foreach($value as $v){
                        if($os->shirt_type == $key && $os->shirt_color == $v['color']){
                            $size_list[$key][$v['color']]['sizes'] = [
                                "xsmall" => ["size"=>(($os->xsmall === "-1")?0:$os->xsmall),"addl_size" => $v['xs'],"override" => $os->xsmall_override],
                                "small" => ["size"=>(($os->small === "-1")?0:$os->small),"override" => $os->small_override],
                                "medium" => ["size"=>(($os->medium === "-1")?0:$os->medium),"override" => $os->medium_override],
                                "large" => ["size"=>(($os->large === "-1")?0:$os->large),"override" => $os->large_override],
                                "xlarge" => ["size"=>(($os->xlarge === "-1")?0:$os->xlarge),"override" => $os->xlarge_override],
                                "xxlarge" => ["size"=>(($os->xxlarge === "-1")?0:$os->xxlarge),"override" => $os->xxlarge_override],
                                "xxxlarge" => ["size"=>(($os->xxxlarge === "-1")?0:$os->xxxlarge),"addl_size" => $v['xxxl'], "override" => $os->xxxlarge_override],
                                "xxxxlarge" => ["size"=>(($os->xxxxlarge === "-1")?0:$os->xxxxlarge),"addl_size" => $v['xxxxl'], "override" => $os->xxxxlarge_override],
                            ];
                        }
                    }
                }
            }
        } else {
            foreach($order_array['shirts'] as $key=>$value){
                foreach($value as $v){
                    $size_list[$key][$v['color']]['sizes'] = [
                        "xsmall" => ['size' => 0, 'addl_size' => $v['xs'], "override" => ""],
                        "small" => ['size' => 0, "override" => 0],
                        "medium" => ['size' => 0, "override" => 0],
                        "large" => ['size' => 0, "override" => 0],
                        "xlarge" => ['size' => 0, "override" => 0],
                        "xxlarge" => ['size' => 0, "override" => 0],
                        "xxxlarge" => ['size' => 0, 'addl_size' => $v['xxxl'], "override" => 0],
                        "xxxxlarge" => ['size' => 0, 'addl_size' => $v['xxxxl'], "override" => 0],
                    ];
                }
            }
        }

        $size_list = serialize($size_list);

        return view('designer.customs_sub_views.quantity', compact('order_array','randomnum', 'host','size_list'));
    }

    public function customssubmit(Request $request)
    {
        $result = CustomsSubmissionService::customSalesSubmission($request);
        if($result === true){
             return Redirect::route('custom.home', array('salesperson' => $request->salesperson))->with('status','Request was Sent!');
        } else {
            return back()->withInput()->withErrors($result);
        }
    }

    public function getIconData()
    {
 	   $icon_array = CustomsMiscService::getIconData();

 	   echo $icon_array;
 	   die;
    }

    public function getShirtData($shirt_type, $color)
    {
        $shirt_data = CustomsMiscService::getShirtData($shirt_type, $color);

 	    echo $shirt_data;
 	    die;
    }

    public function getColorData()
    {
        $colors = [91, 90, 29, 1, 34, 23, 5, 2, 28, 78, 3, 80, 31];
        $shirt_data = CustomsMiscService::getColorData($colors);

 	    echo $shirt_data;
 	    die;
    }

    public function importQuickBaseData($type)
    {
        QuickBaseService::startQuickBaseImport($type);
        return;
    }

    public function getTaxRate($id)
    {
        $order = Preorders::where('id',$id)->first();        
        $response = CustomsAdminService::getTaxRate($order);
        
        return response()->json([
            $response,
        ], 200);
    }
}
