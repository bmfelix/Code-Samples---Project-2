<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Models\Roles;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/admin';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'role_id' => 'required|integer',
            'salesperson_url' => 'string|max:20',
            'phone' => 'required|phone',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'role_id' => $data['role_id'],
            'salesperson_url' => $data['salesperson_url'],
            'phone' => $data['phone'],
        ]);
    }

    public function showRegistrationForm()
    {
        if(\Auth::check()){
            $current_user_role = (int)\Auth::user()->role_id;
            if($current_user_role === 1){
                $roles = Roles::get(['id','role'])->toArray();
                $roles = $this->rebuildRolesArrayForSelect($roles);
                $blank_value = array(0 => "");
                $roles = array_merge($blank_value, $roles);
            } else {
                $roles = Roles::where('id','>',$current_user_role)->get(['id','role'])->toArray();
                $roles = $this->rebuildRolesArrayForSelect($roles);
                $blank_value = array(0 => "");
                $roles = array_merge($blank_value, $roles);
            }
            return view('auth.register', compact('roles'));
        }
        else{
            return redirect('auth.login');
        }

    }

    private function rebuildRolesArrayForSelect($roles){
        $sorted_list = array();
        foreach ($roles as $role){
            $sorted_list[$role['id']] =  trim($role['role']);
        }
        return $sorted_list;
    }
}
