<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class ProductionStatus extends Model
{
    //
    protected $table = 'production_status';

    public function statuses()
    {
        return $this->hasMany('App\Http\Models\Projects');
    }
}
