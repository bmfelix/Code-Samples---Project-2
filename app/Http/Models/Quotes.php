<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Quotes extends Model
{
    //
    protected $table = 'quotations';
    public $primaryKey = 'order_id';

    public function formTypes()
    {
        return $this->belongsTo('App\Http\Models\Form_types', 'form_type_id');
    }

    public function paymentTypes()
    {
        return $this->belongsTo('App\Http\Models\Payment_types', 'payment_type_id');
    }

    public function getContacts()
    {
        return $this->belongsTo('App\Http\Models\Contacts', 'customer_id');
    }

    public function getSalesPeople()
    {
        return $this->belongsTo('App\Http\Models\Employees', 'salesperson_id');
    }

}
