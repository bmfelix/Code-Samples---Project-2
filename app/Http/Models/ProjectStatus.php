<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectStatus extends Model
{
    //
    protected $table = 'project_status';

    public function statuses()
    {
        return $this->belongsTo('App\Http\Models\Statuses', 'status_id');
    }

    public function projects()
    {
        return $this->hasMany('App\Http\Models\Projects','project_id');
    }

    public function employees()
    {
        return $this->hasMany('App\Http\Models\Employees','id');
    }
}
