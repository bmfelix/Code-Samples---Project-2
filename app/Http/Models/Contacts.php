<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Contacts extends Model
{
    //
    protected $table = 'contacts';

    public function quotes()
    {
        return $this->hasMany('App\Http\Models\Quotes');
    }

    public function projects()
    {
        return $this->hasMany('App\Http\Models\Projects');
    }
}
