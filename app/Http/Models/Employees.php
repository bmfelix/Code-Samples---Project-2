<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Employees extends Model
{
    //
    protected $table = 'users';

    public function quotes()
    {
        return $this->hasMany('App\Http\Models\Quotes');
    }

    public function projectstatus()
    {
        return $this->hasMany('App\Http\Models\ProjectStatus');
    }

    public function getRole()
    {
        return $this->belongsTo('App\Http\Models\Roles', 'role_id');
    }
}
