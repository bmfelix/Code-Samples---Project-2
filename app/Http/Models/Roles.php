<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Roles extends Model
{
    //
    protected $table = 'roles';

    public function employees()
    {
        return $this->hasOne('App\Http\Models\Employees');
    }
}
