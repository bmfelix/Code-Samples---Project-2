<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Statuses extends Model
{
    //
    protected $table = 'statuses';

    public function projectstatus()
    {
        return $this->hasMany('App\Http\Models\ProjectStatus');
    }
}
