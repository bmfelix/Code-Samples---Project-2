<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class NoteLocations extends Model
{
    //
    protected $table = 'note_locations';


    public function projectrevisions()
    {
        return $this->hasMany('App\Http\Models\ProjectRevisions');
    }
}
