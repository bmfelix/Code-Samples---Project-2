<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Form_types extends Model
{
    //
    protected $table = 'form_types';
    
    public function quotes()
    {
        return $this->hasMany('App\Http\Models\Quotes');
    }
}
