<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectRevisions extends Model
{
    //
    protected $table = 'project_revisions';

    public function revtypes()
    {
        return $this->belongsTo('App\Http\Models\RevTypes', 'revision_type_id');
    }

    public function notelocations()
    {
        return $this->belongsTo('App\Http\Models\NoteLocations', 'note_location_id');
    }

    public function employees()
    {
        return $this->belongsTo('App\Http\Models\Employees', 'user_id');
    }
}
