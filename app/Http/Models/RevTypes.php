<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class RevTypes extends Model
{
    //
    protected $table = 'revision_type';


    public function projectrevisions()
    {
        return $this->hasMany('App\Http\Models\ProjectRevisions');
    }
}
