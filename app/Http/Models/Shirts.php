<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Shirts extends Model
{
    //
    protected $table = 'shirts';
    
    public function shirtType()
    {
        return $this->belongsTo('App\Http\Models\Shirt_types');
    }

    public function shirtColor()
    {
        return $this->belongsTo('App\Http\Models\Colors');
    }
}
