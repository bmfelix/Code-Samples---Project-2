<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Payment_types extends Model
{
    //
    protected $table = 'payment_types';

    public function quotes()
    {
        return $this->hasMany('App\Http\Models\Quotes');
    }
}
