<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class DocTypes extends Model
{
    //
    protected $table = 'doctypes';

    public function projectfiles()
    {
        return $this->hasMany('App\Http\Models\ProjectFiles');
    }
}
