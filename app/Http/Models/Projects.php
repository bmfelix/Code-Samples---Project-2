<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Projects extends Model
{
    //
    protected $table = 'projects';

    public function production_status()
    {
        return $this->belongsTo('App\Http\Models\ProductionStatus', 'production_status_id');
    }

    public function contacts()
    {
        return $this->belongsTo('App\Http\Models\Contacts', 'company_id');
    }

    public function prodstatus()
    {
        return $this->belongsTo('App\Http\Models\ProductionStatus', 'production_status_id');
    }

    public function projstatus()
    {
        return $this->belongsTo('App\Http\Models\ProjectStatus', 'id');
    }
}
