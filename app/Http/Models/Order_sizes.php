<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Order_sizes extends Model
{
    //
    protected $table = 'order_sizes';

    public function preOrders()
    {
        return $this->belongsTo('App\Http\Models\Preorders');
    }
}
