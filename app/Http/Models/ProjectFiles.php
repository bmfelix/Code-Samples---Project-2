<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectFiles extends Model
{
    //
    protected $table = 'project_files';

    public function doctypes()
    {
        return $this->belongsTo('App\Http\Models\DocTypes', 'doc_type');
    }

    public function revdoctypes()
    {
        return $this->belongsTo('App\Http\Models\RevTypes', 'doc_type');
    }
}
