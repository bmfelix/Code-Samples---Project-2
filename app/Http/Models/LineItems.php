<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class LineItems extends Model
{
    //
    protected $table = 'lineitems';

    public function products()
    {
        return $this->belongsTo('App\Http\Models\Products', 'product_id');
    }

    public function garments()
    {
        return $this->belongsTo('App\Http\Models\Garments', 'garment');
    }

    public function shirttypes()
    {
        return $this->belongsTo('App\Http\Models\Shirt_types', 'garment_number');
    }

    public function colors()
    {
        return $this->belongsTo('App\Http\Models\Colors', 'garment_color_id');
    }
}
