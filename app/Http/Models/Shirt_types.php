<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Shirt_types extends Model
{
    //
    protected $table = 'shirt_type';
    
    public function shirts()
    {
        return $this->hasMany('App\Http\Models\Shirts');
    }
}
