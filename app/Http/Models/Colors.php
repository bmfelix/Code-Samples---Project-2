<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Colors extends Model
{
    //
    protected $table = 'shirt_colors';
    
    public function shirts()
    {
        return $this->hasMany('App\Http\Models\Shirts');
    }
}
