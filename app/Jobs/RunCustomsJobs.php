<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Http\Controllers\CustomsController;

class RunCustomsJobs implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $data;
    protected $type;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data, $type)
    {
        $this->data = $data;
        $this->type = $type;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

    //     switch($this->type){
	//         case "makeAvailable":
	//         	$runjob = (new CustomsController);
	// 			$runjob->makeAvailable($this->data);
	//         	break;
	//         case "syncInbound":
	//         	$runjob = (new CustomsController);
	// 			$runjob->syncInbound($this->data);
	//         	break;
	//         case "cancelOrder":
	//         	$runjob = (new CustomsController);
	// 			$runjob->cancelOrderNow($this->data);
	//         	break;
	//         case "fulfillOrder":
	//         	$runjob = (new CustomsController);
	// 			$runjob->updateShopifyByOrigin($this->data);
	//         	break;
    //     }
    // }
    }
}