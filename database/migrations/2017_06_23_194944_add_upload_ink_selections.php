<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUploadInkSelections extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->string('front_upload_ink_one');
            $table->string('front_upload_ink_two');
			$table->string('front_upload_ink_three');
			$table->string('back_upload_ink_one');
			$table->string('back_upload_ink_two');
			$table->string('back_upload_ink_three');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            //
        });
    }
}
