<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuotationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quotations', function (Blueprint $table) {
            $table->increments('order_id');
            $table->integer('salesperson_id');
            $table->string('customer_id');
            $table->string('purchase_order_number')->nullable();
            $table->integer('form_type_id');
            $table->integer('payment_type_id');
            $table->string('project_name')->nullable();
            $table->string('shipping_address')->nullable();
            $table->string('shipping_city')->nullable();
            $table->string('shipping_state')->nullable();
            $table->string('shipping_zip')->nullable();
            $table->tinyInteger('order_shipped');
            $table->string('shipping_method')->nullable();
            $table->dateTime('date_shipped')->nullable();
            $table->string('tracking_number')->nullable();
            $table->longText('comments')->nullable();
            $table->float('subtotal');
            $table->float('shipping_charged')->nullable();
            $table->float('discount')->nullable();
            $table->float('order_total');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quotations');
    }
}
