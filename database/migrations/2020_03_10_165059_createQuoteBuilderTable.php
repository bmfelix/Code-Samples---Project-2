<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuoteBuilderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quote_lines', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('project_id');
            $table->integer('request_id');
            $table->integer('product_id');
            $table->integer('garment_id');
            $table->integer('color_id');
            $table->string('size');
            $table->string('location');
            $table->integer('sleeve_id');
            $table->integer('ink_1_id')->nullable();
            $table->integer('ink_2_id')->nullable();
            $table->integer('ink_3_id')->nullable();
            $table->integer('ink_4_id')->nullable();
            $table->integer('quantity');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quote_lines');
    }
}
