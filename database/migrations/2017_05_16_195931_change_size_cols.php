<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeSizeCols extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn(['small_men', 'medium_men', 'large_men', 'xlarge_men','xxlarge_men','xxxlarge_men','xsmall_women','small_women','medium_women','large_women','xlarge_women','xxlarge_women']);
            $table->integer('xsmall');
            $table->integer('small');
            $table->integer('medium');
            $table->integer('large');
            $table->integer('xlarge');
            $table->integer('xxlarge');
            $table->integer('xxxlarge');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            //
        });
    }
}
