<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderSizeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_sizes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("order_id");
            $table->string("shirt_type");
            $table->string("shirt_color");
            $table->string("xsmall");
            $table->string("small");
            $table->string("medium");
            $table->string("large");
            $table->string("xlarge");
            $table->string("xxlarge");
            $table->string("xxxlarge");
            $table->string("xxxxlarge");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_sizes');
    }
}
