<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateOrderSizesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_sizes', function (Blueprint $table) {
            $table->tinyInteger('xsmall_override')->nullable();
            $table->tinyInteger('small_override')->nullable();
            $table->tinyInteger('medium_override')->nullable();
            $table->tinyInteger('large_override')->nullable();
            $table->tinyInteger('xlarge_override')->nullable();
            $table->tinyInteger('xxlarge_override')->nullable();
            $table->tinyInteger('xxxlarge_override')->nullable();
            $table->tinyInteger('xxxxlarge_override')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_sizes', function (Blueprint $table) {
            //
        });
    }
}
