<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUploadInkSelectionsMakeNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            //
            $table->string('front_upload_ink_one')->nullable()->change();
            $table->string('front_upload_ink_two')->nullable()->change();
			$table->string('front_upload_ink_three')->nullable()->change();
			$table->string('back_upload_ink_one')->nullable()->change();
			$table->string('back_upload_ink_two')->nullable()->change();
			$table->string('back_upload_ink_three')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            //
        });
    }
}
