<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeNullableSizes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->integer('xsmall')->nullable()->change();
            $table->integer('small')->nullable()->change();
            $table->integer('medium')->nullable()->change();
            $table->integer('large')->nullable()->change();
            $table->integer('xlarge')->nullable()->change();
            $table->integer('xxlarge')->nullable()->change();
            $table->integer('xxxlarge')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            //
        });
    }
}
