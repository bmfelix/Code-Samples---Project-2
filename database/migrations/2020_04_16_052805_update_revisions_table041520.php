<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateRevisionsTable041520 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('project_revisions', function (Blueprint $table) {
            $table->dropColumn('note_location_id');
            $table->dropColumn('revision_note');
            $table->longText('front_rev_note')->nullable();
            $table->longText('back_rev_note')->nullable();
            $table->longText('left_rev_note')->nullable();
            $table->longText('right_rev_note')->nullable();
            $table->longText('general_rev_note')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('project_revisions', function (Blueprint $table) {
            //
        });
    }
}
