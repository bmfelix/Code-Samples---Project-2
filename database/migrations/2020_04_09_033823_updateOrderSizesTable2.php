<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateOrderSizesTable2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_sizes', function (Blueprint $table) {
            $table->boolean('xsmall_override')->default(0)->change();
            $table->boolean('small_override')->default(0)->change();
            $table->boolean('medium_override')->default(0)->change();
            $table->boolean('large_override')->default(0)->change();
            $table->boolean('xlarge_override')->default(0)->change();
            $table->boolean('xxlarge_override')->default(0)->change();
            $table->boolean('xxxlarge_override')->default(0)->change();
            $table->boolean('xxxxlarge_override')->default(0)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_sizes', function (Blueprint $table) {
            //
        });
    }
}
