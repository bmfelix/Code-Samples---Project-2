<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateQuoteDatabaseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('quote_lines', function (Blueprint $table) {
            $table->dropColumn('location');
            $table->renameColumn('ink_1_id','ink_1_front_id');
            $table->renameColumn('ink_2_id','ink_2_front_id');
            $table->renameColumn('ink_3_id','ink_3_front_id');
            $table->renameColumn('ink_4_id','ink_4_front_id');
            $table->integer('ink_1_back_id')->nullable();
            $table->integer('ink_2_back_id')->nullable();
            $table->integer('ink_3_back_id')->nullable();
            $table->integer('ink_4_back_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('quote_lines', function (Blueprint $table) {
            //
        });
    }
}
