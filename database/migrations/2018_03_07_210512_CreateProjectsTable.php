<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->dateTime('graphics_review')->nullable();
            $table->dateTime('revision_date')->nullable();
            $table->dateTime('date_sent')->nullable();
            $table->integer('company_id');
            $table->integer('salesperson_id');
            $table->integer('head_designer_id')->nullable();
            $table->integer('designer_id')->nullable();
            $table->dateTime('estimated_start_date')->nullable();
            $table->dateTime('estimated_end_date')->nullable();
            $table->string('production_status_id')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
