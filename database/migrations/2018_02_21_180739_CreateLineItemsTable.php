<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLineItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lineitems', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_id');
            $table->string('product_id');
            $table->string('garment');
            $table->string('garment_number');
            $table->string('garment_color_id');
            $table->string('garment_size');
            $table->string('sleeve_type_id');
            $table->string('ink_color_1')->nullable();
            $table->string('ink_color_2')->nullable();
            $table->string('ink_color_3')->nullable();
            $table->string('ink_color_4')->nullable();
            $table->string('item_price');
            $table->string('quantity');
            $table->float('linetotal');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lineitems');
    }
}
