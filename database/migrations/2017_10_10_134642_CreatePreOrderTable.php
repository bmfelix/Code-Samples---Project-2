<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePreOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('preorders', function (Blueprint $table) {
            $table->increments('id');
            $table->string("name");
            $table->string("phone");
            $table->string("email");
            $table->string("address");
            $table->string("city");
            $table->string("state");
            $table->integer("zip");
            $table->longText("shirt_types");
            $table->longText("front");
            $table->longText("back");
            $table->longText("left_sleeve");
            $table->longText("right_sleeve");
            $table->string("creative_freedom");
            $table->longText("sleeve_notes");
            $table->longText("artwork_notes");
            $table->string('generated_url');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('preorders');
    }
}
